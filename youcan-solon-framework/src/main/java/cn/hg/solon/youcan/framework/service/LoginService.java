package cn.hg.solon.youcan.framework.service;

import cn.hg.solon.youcan.common.exception.ServiceException;
import cn.hg.solon.youcan.system.entity.User;

/**
 * @author 胡高
 */
public interface LoginService {

    User currentUser() throws ServiceException;

    void login(String account, String password, String captcha) throws ServiceException;

    void login(String account, String password, String captcha, boolean remember) throws ServiceException;
}
