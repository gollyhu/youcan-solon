package cn.hg.solon.youcan.framework.cache.interceptor;

import cn.hg.solon.youcan.framework.cache.CacheTagsExecutor;
import org.noear.solon.Solon;
import org.noear.solon.core.aspect.Interceptor;
import org.noear.solon.core.aspect.Invocation;
import org.noear.solon.data.annotation.CacheRemove;

public class CacheTagsRemoveInterceptor implements Interceptor {
    @Override
    public Object doIntercept(Invocation inv) throws Throwable {
        //支持动态开关缓存
        if (Solon.app().enableCaching()) {
            Object tmp = inv.invoke();

            CacheRemove anno = inv.getMethodAnnotation(CacheRemove.class);
            CacheTagsExecutor.global
                    .cacheRemove(anno, inv, tmp);

            return tmp;
        } else {
            return inv.invoke();
        }

    }
}
