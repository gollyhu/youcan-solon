package cn.hg.solon.youcan.framework.web.pear;

import cn.hg.solon.youcan.common.constant.AppConstants;
import cn.hg.solon.youcan.common.vo.BaseVo;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.collection.CollUtil;
import org.dromara.hutool.core.collection.ListUtil;
import org.dromara.hutool.core.util.ObjUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Layui TreeTable树表组件结果对象
 * <p>
 * https://layui.dev/docs/2.8/treeTable/
 * </p>
 * 
 * @author 胡高
 */
public class LayuiTreeTable<T extends Serializable> extends BaseVo {

    public final static int LAYUI_TREE_CODE_VALUE_SUCCESS = 0;

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -6223923414201740449L;

    private static String CHILDREN = "children";
    private static String NAME = "name";

    /**
     * 结果代码
     */
    protected int code = LAYUI_TREE_CODE_VALUE_SUCCESS;

    /**
     * 结果信息
     */
    protected String msg;

    /**
     * 数据总数
     */
    protected int count = 0;

    /**
     * 树对象数组
     */
    protected List<Map<String, Object>> data;

    /**
     * ID字段名
     */
    private String idField = "id";

    /**
     * 父ID字段名
     */
    private String parentIdField = "parentId";

    /**
     * 名称字段名
     */
    private String nameField = "name";

    public LayuiTreeTable(final Collection<T> beanList, final int totalRow, final Object initParentId) {
        this(beanList, CollUtil.isEmpty(beanList) ? 0 : beanList.size(), "id", "parentId", "name", initParentId);

        this.count = CollUtil.size(this.data);
    }

    public LayuiTreeTable(final Collection<T> beanList, final int totalRow, final String idField,
        final String parentIdField, final String nameField, final Object initParentId) {

        this.count = totalRow;

        this.idField = idField;
        this.parentIdField = parentIdField;
        this.nameField = nameField;

        // 转换项目列表为树状选择项列表
        List<Map<String, Object>> itemList = this.convertToItemMap(beanList, initParentId);

        // 构建树状对象
        this.data = this.buildTree(itemList, initParentId);
    }

    /**
     * 支持分页的构造方法
     */
    public LayuiTreeTable(int pageIndex, int pageSize, final Collection<T> beanList, final int totalRow,
        final String idField, final String parentIdField, final String nameField, final Object initParentId) {
        this(beanList, totalRow, idField, parentIdField, nameField, initParentId);
        this.count = CollUtil.size(this.data);
        this.data = ListUtil.page(this.data, pageIndex - 1, pageSize);
    }

    public LayuiTreeTable(final int code, final String errorMessage) {
        this.code = code;
        this.msg = errorMessage;
    }

    public LayuiTreeTable(final String errorMessage) {
        this(AppConstants.RETURN_CODE_VALUE_FAIL, errorMessage);
    }

    private List<Map<String, Object>> buildTree(final Collection<Map<String, Object>> mapList, final Object parentId) {
        List<Map<String, Object>> list = new ArrayList<>();

        for (Map<String, Object> item : mapList) {
            if (ObjUtil.equals(parentId, item.get("parentId"))) {
                list.add(item);
            }
        }

        for (Map<String, Object> item : list) {
            item.put(CHILDREN, this.buildTree(mapList, item.get(this.idField)));
        }

        return list;
    }

    private List<Map<String, Object>> convertToItemMap(final Collection<T> list, final Object initParentId) {
        List<Map<String, Object>> itemMap = new ArrayList<>();
        for (T item : list) {

            Map<String, Object> bean = null;
            bean = BeanUtil.beanToMap(item);

            /*
             * name字段
             */
            if (!bean.containsKey(this.nameField)) {
                bean.put(NAME, this.nameField);
            }

            itemMap.add(bean);
        }

        return itemMap;
    }

    public int getCode() {
        return this.code;
    }

    public int getCount() {
        return this.count;
    }

    public List<Map<String, Object>> getData() {
        return this.data;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setData(List<Map<String, Object>> data) {
        this.data = data;
    }

}
