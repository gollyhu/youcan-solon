package cn.hg.solon.youcan.framework.directive.common;

import org.noear.solon.annotation.Component;

/**
 * 公司参数指令：#COMPANY('...')
 * @author 胡高
 */
@Component("view:" + CompanyDirective.DIRECTIVE_NAME)
public class CompanyDirective extends BaseConfigDirective {

    public static final String DIRECTIVE_NAME = "COMPANY";

    private static final String TYPE = "COMPANY";

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.framework.directive.common.BaseConfigDirective#getDirectiveName()
     */
    @Override
    public String getDirectiveName() {
        return DIRECTIVE_NAME;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.framework.directive.common.BaseConfigDirective#getType()
     */
    @Override
    public String getType() {
        return TYPE;
    }

}
