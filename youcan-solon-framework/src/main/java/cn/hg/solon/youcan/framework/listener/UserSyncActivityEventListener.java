package cn.hg.solon.youcan.framework.listener;

import org.dromara.hutool.core.date.DateUtil;
import org.dromara.hutool.core.util.ObjUtil;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.event.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.hg.solon.youcan.system.service.UserOnlineService;

/**
 * @author 胡高
 */
@Component
public class UserSyncActivityEventListener implements EventListener<SaTokenInfo> {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private UserOnlineService userOnlineService;

    @Override
    public void onEvent(SaTokenInfo event) throws Throwable {
        if (ObjUtil.isNull(event)) {
            return;
        }

        this.log.debug("监听到在线事件：event={}", event);

        String token = event.getTokenValue();

        this.userOnlineService.syncActivityTime(token, DateUtil.now());
    }

}
