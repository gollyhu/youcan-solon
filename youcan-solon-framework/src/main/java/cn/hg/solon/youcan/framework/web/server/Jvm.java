package cn.hg.solon.youcan.framework.web.server;

import java.lang.management.ManagementFactory;
import java.util.Date;

import org.dromara.hutool.core.date.DateUnit;
import org.dromara.hutool.core.date.DateUtil;
import org.dromara.hutool.core.math.NumberUtil;

/**
 * JVM相关信息
 *
 * @author ruoyi
 */
public class Jvm
{
    /**
     * 当前JVM占用的内存总数(M)
     */
    private double total;

    /**
     * JVM最大可用内存总数(M)
     */
    private double max;

    /**
     * JVM空闲内存(M)
     */
    private double free;

    /**
     * JDK版本
     */
    private String version;

    /**
     * JDK路径
     */
    private String home;

    public double getFree()
    {
        return NumberUtil.div(this.free, (1024 * 1024), 2).doubleValue();
    }

    public String getHome()
    {
        return this.home;
    }

    /**
     * 运行参数
     */
    public String getInputArgs()
    {
        return ManagementFactory.getRuntimeMXBean().getInputArguments().toString();
    }

    public double getMax()
    {
        return NumberUtil.div(this.max, (1024 * 1024), 2).doubleValue();
    }

    /**
     * 获取JDK名称
     */
    public String getName()
    {
        return ManagementFactory.getRuntimeMXBean().getVmName();
    }

    /**
     * JDK运行时间
     */
    public String getRunTime()
    {
        Date startTime = DateUtil.date(ManagementFactory.getRuntimeMXBean().getStartTime());
        long poorTime = DateUtil.between(startTime, DateUtil.now(), DateUnit.SECOND);

        return DateUtil.format(DateUtil.date(poorTime), "HHH:mm:dd");
    }

    /**
     * JDK启动时间
     */
    public String getStartTime()
    {
        Date startTime = DateUtil.date(ManagementFactory.getRuntimeMXBean().getStartTime());
        return DateUtil.formatDateTime(startTime);
    }

    public double getTotal()
    {
        return NumberUtil.div(this.total, (1024 * 1024), 2).doubleValue();
    }

    public double getUsage()
    {
        return NumberUtil.mul(NumberUtil.div(this.total - this.free, this.total, 4), 100).doubleValue();
    }

    public double getUsed()
    {
        return NumberUtil.div(this.total - this.free, (1024 * 1024), 2).doubleValue();
    }

    public String getVersion()
    {
        return this.version;
    }

    public void setFree(double free)
    {
        this.free = free;
    }

    public void setHome(String home)
    {
        this.home = home;
    }

    public void setMax(double max)
    {
        this.max = max;
    }

    public void setTotal(double total)
    {
        this.total = total;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }
}
