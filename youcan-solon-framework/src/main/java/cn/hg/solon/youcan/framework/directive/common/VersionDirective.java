package cn.hg.solon.youcan.framework.directive.common;

import org.noear.solon.Solon;
import org.noear.solon.annotation.Component;

import com.jfinal.template.Env;
import com.jfinal.template.TemplateException;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

import cn.hg.solon.youcan.framework.directive.BaseDirective;

/**
 * 获取Youcan版本指令：#VERSION()
 * 
 * @author 胡高
 */
@Component("view:" + VersionDirective.DIRECTIVE_NAME)
public class VersionDirective extends BaseDirective {

    public static final String DIRECTIVE_NAME = "VERSION";

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.framework.directive.BaseDirective#onRender(com.jfinal.template.Env, com.jfinal.template.stat.Scope, com.jfinal.template.io.Writer)
     */
    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        try {
            writer.write(Solon.cfg().get("solon.youcan.version"));
        } catch (Exception e) {
            throw new TemplateException(e.getMessage(), this.location, e);
        }
    }

}
