package cn.hg.solon.youcan.framework.web.pear;

import java.io.Serializable;
import java.util.Collection;

import org.dromara.hutool.core.collection.CollUtil;
import org.dromara.hutool.core.collection.ListUtil;
import org.dromara.hutool.core.util.ObjUtil;

import cn.hg.solon.youcan.common.constant.AppConstants;
import cn.hg.solon.youcan.common.vo.BaseVo;

/**
 * Layui表格组件分页结果对象
 * <p>
 * https://layui.dev/docs/2.8/table/
 * </p>
 * 
 * @author 胡高
 */
public class LayuiPage<T extends Serializable> extends BaseVo {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 448012997404049085L;

    public final static int LAYUI_PAGE_CODE_VALUE_SUCCESS = 0;

    /*
     * 结果代码
     */
    protected int code = AppConstants.RETURN_CODE_VALUE_FAIL;

    /*
     * 结果信息
     */
    protected String msg = AppConstants.RETURN_MESSAGE_VALUE_FAILED;

    /*
     * 数据总数
     */
    protected long count = 0;

    /*
     * 数据集合
     */
    protected Collection<T> data = ListUtil.empty();

    public LayuiPage(Collection<T> list) {
        this(list, CollUtil.size(list));
    }

    public LayuiPage(Collection<T> records, long totalRow) {
        if (ObjUtil.isNotNull(records)) {
            this.count = totalRow;
            this.data = records;
        }
        this.code = LAYUI_PAGE_CODE_VALUE_SUCCESS;
        this.msg = AppConstants.RETURN_MESSAGE_VALUE_SUCCESS;
    }

    public LayuiPage (int code, String errorMessage) {
        this.code = code;
        this.msg = errorMessage;
    }

    public LayuiPage(String errorMessage) {
        this.code = AppConstants.RETURN_CODE_VALUE_FAIL;
        this.msg = errorMessage;
    }

    /**
     * @return the code
     */
    public int getCode() {
        return this.code;
    }

    /**
     * @return the count
     */
    public long getCount() {
        return this.count;
    }

    /**
     * @return the data
     */
    public Collection<T> getData() {
        return this.data;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return this.msg;
    }

    /**
     * @param code the code to set
     */
    public LayuiPage<T> setCode(int code) {
        this.code = code;
        return this;
    }

    /**
     * @param count the count to set
     */
    public LayuiPage<T> setCount(long count) {
        this.count = count;
        return this;
    }

    /**
     * @param data the data to set
     */
    public LayuiPage<T> setData(Collection<T> data) {
        this.data = data;
        return this;
    }

    /**
     * @param msg the msg to set
     */
    public LayuiPage<T> setMsg(String msg) {
        this.msg = msg;
        return this;
    }

}
