package cn.hg.solon.youcan.framework.listener;

import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.event.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.hg.solon.youcan.common.util.AddressUtil;
import cn.hg.solon.youcan.system.entity.UserOnline;
import cn.hg.solon.youcan.system.service.UserOnlineService;

/**
 * 用户在线事件监听器
 * 
 * @author 胡高
 */
@Component
public class UserOnlineEventListener implements EventListener<UserOnline> {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private UserOnlineService userOnlineService;

    @Override
    public void onEvent(UserOnline online) throws Throwable {
        this.log.debug("监听到用户在线事件：log={}", online);

        online.setLocation(AddressUtil.getRealAddressByIP(online.getIp()));

        this.userOnlineService.update(online);
    }

}
