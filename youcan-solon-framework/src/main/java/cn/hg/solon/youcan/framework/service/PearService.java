package cn.hg.solon.youcan.framework.service;

import java.util.List;

import cn.hg.solon.youcan.framework.web.pear.PearConfig;
import cn.hg.solon.youcan.framework.web.pear.PearMenuItem;
import cn.hg.solon.youcan.system.entity.User;

public interface PearService {

    List<PearMenuItem> buildMenu(User bean, Integer[] rootIds);

    PearConfig getConfig();

}