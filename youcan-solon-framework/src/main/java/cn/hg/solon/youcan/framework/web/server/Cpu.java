package cn.hg.solon.youcan.framework.web.server;

import org.dromara.hutool.core.math.NumberUtil;

/**
 * CPU相关信息
 *
 * @author ruoyi
 */
public class Cpu
{
    /**
     * 核心数
     */
    private int cpuNum;

    /**
     * CPU总的使用率
     */
    private double total;

    /**
     * CPU系统使用率
     */
    private double sys;

    /**
     * CPU用户使用率
     */
    private double used;

    /**
     * CPU当前等待率
     */
    private double wait;

    /**
     * CPU当前空闲率
     */
    private double free;

    public int getCpuNum()
    {
        return this.cpuNum;
    }

    public double getFree()
    {
        return NumberUtil.round(NumberUtil.mul(this.free / this.total, 100), 2).doubleValue();
    }

    public double getSys()
    {
        return NumberUtil.round(NumberUtil.mul(this.sys / this.total, 100), 2).doubleValue();
    }

    public double getTotal()
    {
        return NumberUtil.round(NumberUtil.mul(this.total, 100), 2).doubleValue();
    }

    public double getUsed()
    {
        return NumberUtil.round(NumberUtil.mul(this.used / this.total, 100), 2).doubleValue();
    }

    public double getWait()
    {
        return NumberUtil.round(NumberUtil.mul(this.wait / this.total, 100), 2).doubleValue();
    }

    public void setCpuNum(int cpuNum)
    {
        this.cpuNum = cpuNum;
    }

    public void setFree(double free)
    {
        this.free = free;
    }

    public void setSys(double sys)
    {
        this.sys = sys;
    }

    public void setTotal(double total)
    {
        this.total = total;
    }

    public void setUsed(double used)
    {
        this.used = used;
    }

    public void setWait(double wait)
    {
        this.wait = wait;
    }
}
