package cn.hg.solon.youcan.framework.directive.system;

import cn.hg.solon.youcan.common.constant.CacheConstants;
import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.framework.cache.RedisCacheTagsService;
import cn.hg.solon.youcan.framework.directive.BaseDirective;
import cn.hg.solon.youcan.system.service.DictService;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import org.dromara.hutool.core.map.MapUtil;
import org.dromara.hutool.core.text.StrUtil;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * 字典类型指令：#DICTYPES('...')
 *
 * @author 胡高
 */
@Component("view:" + DictTypeDirective.DIRECTIVE_NAME)
public class DictTypeDirective extends BaseDirective {

    public static final String DIRECTIVE_NAME = "DICTYPES";

    @Inject
    private DictService dictService;

    @Inject
    private RedisCacheTagsService cacheService;

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        @SuppressWarnings({"rawtypes"})
        List list = this.cacheService.getOrStoreTag(DIRECTIVE_NAME + ":all", List.class,
                CacheConstants.CACHE_SECONDS_ONE_DAY, new Supplier<List>() {
                    @Override
                    public List get() {
                        /*
                         * 查询字典类型列表
                         */
                        return DictTypeDirective.this.dictService.listBy(
                                MapUtil.of("status", getParaToString("status", scope))
                        );
                    }

                }, CacheConstants.SYS_DICT_TAG);

        scope.set(this.getPara("var", scope, DIRECTIVE_NAME.toLowerCase()), list);
    }
}
