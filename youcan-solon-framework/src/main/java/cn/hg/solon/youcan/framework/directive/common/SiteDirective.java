package cn.hg.solon.youcan.framework.directive.common;

import org.noear.solon.annotation.Component;

/**
 * 站点参数指令：#SITE('...')
 * 
 * @author 胡高
 */
@Component("view:" + SiteDirective.DIRECTIVE_NAME)
public class SiteDirective extends BaseConfigDirective {

    public static final String DIRECTIVE_NAME = "SITE";

    private static final String TYPE = "SITE";

    @Override
    public String getDirectiveName() {
        return DIRECTIVE_NAME;
    }

    @Override
    public String getType() {
        return TYPE;
    }
}
