package cn.hg.solon.youcan.framework.provider;

import java.util.ArrayList;
import java.util.List;

import org.dromara.hutool.core.array.ArrayUtil;
import org.dromara.hutool.core.collection.CollUtil;
import org.dromara.hutool.core.text.CharSequenceUtil;
import org.dromara.hutool.core.text.StrUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.handle.MethodType;

import cn.hg.solon.youcan.common.util.ContextPathUtil;
import cn.hg.solon.youcan.framework.web.pear.PearConfig;
import cn.hg.solon.youcan.framework.web.pear.PearConfig.Color;
import cn.hg.solon.youcan.framework.web.pear.PearMenuItem;
import cn.hg.solon.youcan.framework.service.PearService;
import cn.hg.solon.youcan.system.entity.Config;
import cn.hg.solon.youcan.system.entity.Permission;
import cn.hg.solon.youcan.system.entity.User;
import cn.hg.solon.youcan.system.service.ConfigService;
import cn.hg.solon.youcan.system.service.PermissionService;

/**
 * @author 胡高
 */
@Component
public class PearProvider implements PearService {

    private static String VIEW_BASE = ContextPathUtil.getContextPath() + "/admin/";

    @Inject
    private ConfigService configService;

    @Inject
    private PermissionService permissionService;

    private List<PearMenuItem> buildChildrenMenuJsonObject(PearMenuItem parent, List<? extends Permission> menuList) {
        String parentId = parent.getId();

        List<PearMenuItem> jsonList = new ArrayList<>();

        for (Permission menu : menuList) {
            if (!CharSequenceUtil.equals("MENU", menu.getType()) || !CharSequenceUtil.equals(parentId, StrUtil.toString(menu.getParentId()))) {
                continue;
            }

            /*
             * 示例 { "id": 20, "title":
             * "<i class='layui-icon layui-icon-app'></i>&nbsp;&nbsp;系统用户", "icon":
             * "layui-icon layui-icon-util", "type": 0, "href": "", "children": [ }
             */
            PearMenuItem item = new PearMenuItem();
            item.setId(StrUtil.toString(menu.getId()));
            item.setTitle(menu.getName());
            item.setHref(ContextPathUtil.prefixContextPath(menu.getUrl()));
            item.setIcon("layui-icon " + menu.getIcon());
            List<PearMenuItem> children = this.buildChildrenMenuJsonObject(item, menuList);
            item.setChildren(children);
            item.setOpenType(menu.getTarget());
            item.setType(CollUtil.isEmpty(children) ? 1 : 0);

            jsonList.add(item);
        }
        return jsonList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * cn.hg.solon.youcan.web.pear.PearService#buildMenu(cn.hg.solon.youcan.system.
     * entity.SysUser, java.lang.Integer[])
     */
    @Override
    public List<PearMenuItem> buildMenu(User user, Integer[] rootIds) {

        List<? extends Permission> menuList = this.permissionService.listByUser(user);
        List<PearMenuItem> list = new ArrayList<>();

        for (Permission menu : menuList) {
            if (ArrayUtil.contains(rootIds, menu.getParentId())) {
                PearMenuItem item = new PearMenuItem();
                item.setId(StrUtil.toString(menu.getId()));
                item.setTitle(menu.getName());
                item.setHref(ContextPathUtil.prefixContextPath(menu.getUrl()));
                if (StrValidator.isNotBlank(menu.getTarget())) {
                    item.setOpenType(menu.getTarget());
                }
                item.setIcon("layui-icon " + menu.getIcon());
                List<PearMenuItem> children = this.buildChildrenMenuJsonObject(item, menuList);
                if (CollUtil.isNotEmpty(children)) {
                    item.setType(0);
                    item.setChildren(children);
                } else {
                    item.setType(1);
                    item.setChildren(null);
                }
                item.setOpenType(menu.getTarget());

                list.add(item);
            }
        }
        return list;
    }

    /*
     * (non-Javadoc)
     * 
     * @see cn.hg.solon.youcan.web.pear.PearService#getConfig()
     */
    @Override
    public PearConfig getConfig() {
        PearConfig config = new PearConfig();

        /*
         * logo
         */
        // 网站图标
        Config icon = this.configService.getByTypeAndCode("SITE", "icon");
        config.getLogo().setImage(icon.getValue());
        // 网站名称
        Config fullname = this.configService.getByTypeAndCode("SITE", "fullname");
        config.getLogo().setTitle(fullname.getValue());

        /*
         * menu
         */
        // 菜单数据来源
        config.getMenu().setData(VIEW_BASE + "menu?ids=1");
        // 菜单接口的请求方式 GET / POST
        config.getMenu().setMethod(MethodType.GET.name);
        // 是否同时只打开一个菜单目录
        config.getMenu().setAccordion(true);
        // 侧边默认折叠状态
        config.getMenu().setCollaspe(false);
        // 是否开启多系统菜单模式
        config.getMenu().setControl(false);
        // 默认选中的菜单项
        config.getMenu().setSelect("0");
        // 是否开启异步菜单，false 时 data 属性设置为静态数据，true 时为后端接口
        config.getMenu().setAsync(true);

        /*
         * tab
         */
        // 是否开启多选项卡
        config.getTab().setEnable(true);
        // 保持视图状态
        config.getTab().setKeepState(true);
        config.getTab().setMax(10);
        // 开启选项卡记忆
        config.getTab().setSession(false);
        // 浏览器刷新时是否预加载非激活标签页
        config.getTab().setPreload(false);
        // 保持视图状态
        config.getTab().setKeepState(true);

        /*
         * 首页
         */
        // 页面地址
        config.getTab().getIndex().setHref(VIEW_BASE + "home");
        // 标识 ID , 建议与菜单项中的 ID 一致
        config.getTab().getIndex().setId("2");
        // 标题
        config.getTab().getIndex().setTitle("首页");

        /*
         * theme
         */
        // 默认主题色，对应 colors 配置中的 ID 标识
        config.getTheme().setDefaultColor("2");
        // 默认的顶部主题 dark-theme 黑 / light-theme 白
        config.getTheme().setDefaultHeader("light-theme");
        // 默认的菜单主题 dark-theme 黑 / light-theme 白
        config.getTheme().setDefaultMenu("dark-theme");
        // 是否允许用户切换主题，false 时关闭自定义主题面板
        config.getTheme().setAllowCustom(true);
        // 通栏配置
        config.getTheme().setBanner(false);

        /*
         * colors
         */
        Color color1 = new Color();
        color1.setId("1");
        color1.setColor("#2d8cf0");
        color1.setSecond("#ecf5ff");
        config.getColors().add(color1);

        Color color2 = new Color();
        color2.setId("2");
        color2.setColor("#36b368");
        color2.setSecond("#f0f9eb");
        config.getColors().add(color2);

        Color color3 = new Color();
        color3.setId("3");
        color3.setColor("#f6ad55");
        color3.setSecond("#fdf6ec");
        config.getColors().add(color3);

        Color color4 = new Color();
        color4.setId("4");
        color4.setColor("#f56c6c");
        color4.setSecond("#fef0f0");
        config.getColors().add(color4);

        Color color5 = new Color();
        color5.setId("5");
        color5.setColor("#3963bc");
        color5.setSecond("#ecf5ff");
        config.getColors().add(color5);

        /*
         * other
         */
        // 主页动画时长
        //config.getOther().setKeepLoad(100);
        // 布局顶部主题
        //config.getOther().setAutoHead(true);
        // 页脚
        config.getOther().setFooter(true);

        /*
         * header
         */
        // 站内消息，通过 false 设置关闭
        config.getHeader().setMessage(false);

        return config;
    }
}
