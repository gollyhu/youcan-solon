package cn.hg.solon.youcan.framework.web.qo;

import java.util.Iterator;
import java.util.Map;

import cn.hg.solon.youcan.common.vo.BaseVo;
import cn.hg.solon.youcan.common.vo.IQueryObject;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.text.StrUtil;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.json.JSONUtil;

/**
 * 基本查询对象
 *
 * @author 胡高
 */
public class QueryObject extends BaseVo implements IQueryObject {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1352772495431388859L;

    private String word;


    public String getWord() {
        return this.word;
    }

    @Override
    public Map<String, Object> toMap() {
        return BeanUtil.beanToMap(this, false, true);
    }

    @Override
    public String toString() {
        Map<String, Object> map = this.toMap();

        Iterator<Map.Entry<String, Object>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            if (StrUtil.isBlankIfStr(entry.getValue()) || ObjUtil.isNull(entry.getValue())) {
                iterator.remove();
            }
        }
        return JSONUtil.toJsonStr(map);
    }
}
