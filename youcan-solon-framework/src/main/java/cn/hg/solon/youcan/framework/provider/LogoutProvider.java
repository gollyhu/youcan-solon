package cn.hg.solon.youcan.framework.provider;

import java.util.List;

import org.dromara.hutool.core.collection.CollUtil;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.hg.solon.youcan.common.exception.ServiceException;
import cn.hg.solon.youcan.framework.service.LogoutService;
import cn.hg.solon.youcan.system.service.UserOnlineService;

/**
 * @author 胡高
 */
@Component
public class LogoutProvider implements LogoutService {

    @Inject
    private UserOnlineService userOnlineService;

    @Override
    public boolean kickout(List<String> tokens) throws ServiceException {
        if (CollUtil.isEmpty(tokens)) {
            return false;
        }

        for (String item : tokens) {
            StpUtil.kickoutByTokenValue(item);
            this.userOnlineService.offline(item);
        }

        return true;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.framework.service.LogoutService#logout(java.lang.String)
     */
    @Override
    public boolean logout() throws ServiceException {
        if (StpUtil.isLogin()) {
            SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
            String token = tokenInfo.getTokenValue();

            StpUtil.logoutByTokenValue(token);

            this.userOnlineService.offline(token);

            return true;
        }

        return false;
    }

}
