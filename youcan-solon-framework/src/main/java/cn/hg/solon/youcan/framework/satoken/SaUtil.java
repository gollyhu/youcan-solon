package cn.hg.solon.youcan.framework.satoken;

import org.dromara.hutool.core.util.ObjUtil;

import cn.dev33.satoken.stp.StpUtil;
import cn.hg.solon.youcan.common.constant.CacheConstants;
import cn.hg.solon.youcan.system.entity.Dept;
import cn.hg.solon.youcan.system.entity.User;

/**
 * @author 胡高
 */
public class SaUtil {

    public static Dept getCurrentDept() {
        return (Dept) StpUtil.getTokenSession().get(CacheConstants.SYS_DEPT_TAG);
    }

    public static User getCurrentUser() {
        if (!StpUtil.isLogin()) {
            return null;
        }

        Integer id = StpUtil.getLoginIdAsInt();
        if (ObjUtil.isNull(id)) {
            return null;
        }

        return (User) StpUtil.getTokenSession().get(CacheConstants.SYS_USER_TAG);
    }

    public static void setCurrentUser(User user, Dept dept) {
        StpUtil.getTokenSession().set(CacheConstants.SYS_USER_TAG, user);
        StpUtil.getTokenSession().set(CacheConstants.SYS_DEPT_TAG, dept);
    }

}
