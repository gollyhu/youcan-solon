package cn.hg.solon.youcan.framework.cache;

import org.dromara.hutool.core.collection.CollUtil;
import org.dromara.hutool.core.text.StrUtil;
import org.noear.redisx.plus.RedisList;
import org.noear.solon.Utils;
import org.noear.solon.cache.jedis.RedisCacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.function.Supplier;

/**
 * @author 胡高
 * @date 2023/06/15
 */
public class RedisCacheTagsProvider extends RedisCacheService implements RedisCacheTagsService {

    static final Logger log = LoggerFactory.getLogger(RedisCacheTagsProvider.class);

    private static String REDIS_CACHE_TAGS_KEY = "CACHE-TAGS";

    public RedisCacheTagsProvider(Properties prop) {
        super(prop);
    }

    protected String newKey(String key, String tag, boolean enableMd5key) {
        if (enableMd5key) {
            return _cacheKeyHead + ":" + tag + ":" + Utils.md5(key);
        } else {
            return _cacheKeyHead + ":" + tag + ":" + key;
        }
    }

    @Override
    public Object get(String key, String tag) {
        String newKey = newKey(key, tag, this._enableMd5key);
        String val = client.openAndGet((ru) -> ru.key(newKey).get());

        if (val == null) {
            return null;
        }

        try {
            return _serializer.deserialize(val, Object.class);
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public Object getMd5(String md5Key, String tag) {
        String newKey = newKey(md5Key, tag, false);

        String val = client.openAndGet((ru) -> ru.key(newKey).get());

        if (val == null) {
            return null;
        }

        try {
            return _serializer.deserialize(val, Object.class);
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public List<String> getKeysAll(String tag) {
        tag = _cacheKeyHead + ":" + tag + ":*";
        int tagLan = StrUtil.length(tag) - 1;

        Set<String> keys = this.client.getBucket().keys(tag);
        List<String> keyList = new ArrayList<>();

        keys.forEach(item -> {
            keyList.add(StrUtil.subSuf(item, tagLan));
        });

        return keyList;
    }

    @Override
    public List<String> getTagsAll() {
        return this.client().getList(REDIS_CACHE_TAGS_KEY).getAll();
    }

    @Override
    public void remove(String key, String tag) {
        String newKey = newKey(key, tag, this._enableMd5key);

        client.open((ru) -> {
            ru.key(newKey).delete();
        });
    }

    @Override
    public void removeMd5(String md5Key, String tag) {
        String newKey = newKey(md5Key, tag, false);

        client.open((ru) -> {
            ru.key(newKey).delete();
        });
    }

    @Override
    public <T> T getOrStoreTag(String key, Class<T> clz, int seconds, Supplier<T> supplier, String... tags) {
        Object obj = null;
        if (CollUtil.size(tags) > 0) {
            obj = this.get(tags[0] + ":" + key, clz);
        }
        if (obj == null) {
            obj = supplier.get();
            for (String tag : tags) {
                this.storeTag(key, obj, seconds, tag);
            }
        }
        return (T) obj;
    }


    @Override
    public long ttl(String key, String tag) {
        return this.client().getBucket().ttl(newKey(key, tag, this._enableMd5key));
    }

    @Override
    public long ttlMd5(String key, String tag) {
        return this.client().getBucket().ttl(newKey(key, tag, false));
    }

    @Override
    public long ttl(String key) {
        return this.client().getBucket().ttl(newKey(key));
    }

    @Override
    public void removeTag(String... tags) {
        RedisList list = this.client().getList(REDIS_CACHE_TAGS_KEY);
        for (String tag : tags) {
            this.client.getBucket().removeByPattern(_cacheKeyHead + ":" + tag + ":*");
            list.remove(tag);
        }
    }

    @Override
    public void storeTag(String key, Object obj, int seconds, String... tags) {
        RedisList list = this.client().getList(REDIS_CACHE_TAGS_KEY);
        for (String tag : tags) {
            this.store(key, tag, obj, seconds);

            if (!list.getAll().contains(tag)) {
                list.add(tag);
            }
        }
    }

    protected void store(String key, String tag, Object obj, int seconds) {
        if (obj == null) {
            return;
        }

        String newKey = newKey(key, tag, this._enableMd5key);

        try {
            String val = _serializer.serialize(obj);

            if (seconds > 0) {
                client.open((ru) -> ru.key(newKey).expire(seconds).set(val));
            } else {
                client.open((ru) -> ru.key(newKey).expire(_defaultSeconds).set(val));
            }
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
        }
    }
}
