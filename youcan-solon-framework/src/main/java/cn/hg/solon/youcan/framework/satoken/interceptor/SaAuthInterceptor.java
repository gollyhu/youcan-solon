package cn.hg.solon.youcan.framework.satoken.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.solon.integration.SaTokenInterceptor;
import cn.dev33.satoken.stp.StpUtil;
import cn.hg.solon.youcan.common.util.ContextPathUtil;

/**
 * sa-token认证拦截器
 * 
 * @author 胡高
 */
public class SaAuthInterceptor extends SaTokenInterceptor {


    /*
     * 登录地址
     */
    private static String loginUrl = "/admin/login";
    private static String loginUrlCtx = ContextPathUtil.getContextPath() + loginUrl;

    /*
     * 认证URL模式
     */
    private static String authPattern = "/admin/**";
    private static String authPatternCtx = ContextPathUtil.getContextPath() + authPattern;
    private static String adminPattern = "/admin";
    private static String adminPatternCtx = ContextPathUtil.getContextPath() + "/admin";

    /*
     * 日志
     */
    Logger log = LoggerFactory.getLogger(this.getClass().getName());

    public SaAuthInterceptor() {
        // Admin认证URL
        this.addInclude(authPattern, authPatternCtx, adminPattern, adminPatternCtx)
        // 不认证URL
        .addExclude(loginUrl, loginUrlCtx)
        .setAuth(req -> {
            SaRouter.match(authPattern, StpUtil::checkLogin);
            SaRouter.match(authPatternCtx, StpUtil::checkLogin);
        })
        // 前置函数：在每次认证函数之前执行
        .setBeforeAuth(req -> {
            // ---------- 设置一些安全响应头 ----------
            SaHolder.getResponse()
            // 服务器名称
            .setServer("sa-server")
            // 是否可以在iframe显示视图： DENY=不可以 | SAMEORIGIN=同域下可以 | ALLOW-FROM uri=指定域名下可以
            .setHeader("X-Frame-Options", "SAMEORIGIN")
            // 是否启用浏览器默认XSS防护： 0=禁用 | 1=启用 | 1; mode=block 启用, 并在检查到XSS攻击时，停止渲染页面
            .setHeader("X-XSS-Protection", "1; mode=block")
            // 禁用浏览器内容嗅探
            .setHeader("X-Content-Type-Options", "nosniff");
        });
    }

}
