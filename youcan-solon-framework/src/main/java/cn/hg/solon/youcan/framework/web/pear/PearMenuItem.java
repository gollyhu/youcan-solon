package cn.hg.solon.youcan.framework.web.pear;

import java.util.List;

import cn.hg.solon.youcan.common.vo.BaseVo;

/**
 * @author 胡高
 */
public class PearMenuItem extends BaseVo {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3160832034016889452L;

    private String id;
    
    private String title;
    
    private String icon;
    
    private int type;
    
    private String href;
    
    private String openType;
    
    private List<PearMenuItem> children;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getOpenType() {
		return openType;
	}

	public void setOpenType(String openType) {
		this.openType = openType;
	}

	public List<PearMenuItem> getChildren() {
		return children;
	}

	public void setChildren(List<PearMenuItem> children) {
		this.children = children;
	}
    
}
