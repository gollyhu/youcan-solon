package cn.hg.solon.youcan.framework.cache.interceptor;

import cn.hg.solon.youcan.framework.cache.CacheTagsExecutor;
import org.noear.solon.Solon;
import org.noear.solon.core.aspect.Interceptor;
import org.noear.solon.core.aspect.Invocation;
import org.noear.solon.data.annotation.CachePut;

public class CacheTagsPutInterceptor implements Interceptor {
    @Override
    public Object doIntercept(Invocation inv) throws Throwable {
        //支持动态开关缓存
        if (Solon.app().enableCaching()) {
            Object tmp = inv.invoke();

            CachePut anno = inv.getMethodAnnotation(CachePut.class);
            CacheTagsExecutor.global
                    .cachePut(anno, inv, tmp);

            return tmp;
        } else {
            return inv.invoke();
        }
    }
}
