package cn.hg.solon.youcan.framework.directive.common;


import cn.hg.solon.youcan.framework.directive.BaseDirective;
import com.jfinal.template.Env;
import com.jfinal.template.TemplateException;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Component;

/**
 * 调试模式指令：#DEBUG() ... #end
 * 
 * @author 胡高
 */
@Component("view:" + DebugDirective.DIRECTIVE_NAME)
public class DebugDirective extends BaseDirective {

    public static final String DIRECTIVE_NAME = "DEBUG";

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        if(Solon.cfg().isDebugMode()) {
            // 如果是调试模式，则输出内容
            this.stat.exec(env, scope, writer);
        }
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
