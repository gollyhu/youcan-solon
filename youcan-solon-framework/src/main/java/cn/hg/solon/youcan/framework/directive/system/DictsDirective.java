package cn.hg.solon.youcan.framework.directive.system;

import cn.hg.solon.youcan.common.constant.CacheConstants;
import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.framework.cache.RedisCacheTagsService;
import cn.hg.solon.youcan.framework.directive.BaseDirective;
import cn.hg.solon.youcan.system.service.DictItemService;
import com.jfinal.template.Env;
import com.jfinal.template.TemplateException;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import org.dromara.hutool.core.text.StrValidator;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import java.util.List;
import java.util.function.Supplier;

/**
 * 字典指令：#DICTS('...')
 * 
 * @author 胡高
 */
@Component("view:" + DictsDirective.DIRECTIVE_NAME)
public class DictsDirective extends BaseDirective {

    public static final String DIRECTIVE_NAME = "DICTS";

    @Inject
    private DictItemService dictItemService;

    @Inject
    private RedisCacheTagsService cacheService;

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        String type = this.getParaToString(0, scope);
        if (StrValidator.isBlank(type)) {
            throw new TemplateException("The parameter of #" + DIRECTIVE_NAME + " directive can not be blank",
                this.location);
        }

        /*
         * 查询DICT_ITEM
         */
        @SuppressWarnings({"rawtypes"})
        List list = this.cacheService.getOrStoreTag(DIRECTIVE_NAME + ":" + type, List.class,
            CacheConstants.CACHE_SECONDS_ONE_DAY, new Supplier<List>() {
                @Override
                public List get() {
                    return DictsDirective.this.dictItemService.listByType(type, BeanStatus.ON.name());
                }

            } , CacheConstants.SYS_DICT_TAG);

        scope.set(this.getPara("var", scope, DIRECTIVE_NAME.toLowerCase()), list);
    }

}
