package cn.hg.solon.youcan.framework.web.pear;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.reflect.FieldUtil;

import cn.hg.solon.youcan.common.vo.BaseVo;
import cn.hg.solon.youcan.framework.web.pear.LayuiTransferData.LayuiTransferItem;

/**
 * Layui Transfer穿梭框组件结果对象
 * <p>
 * https://layui.dev/docs/2.8/transfer/
 * </p>
 *
 * @author 胡高
 */
public class LayuiTransferData<T extends Serializable> extends BaseVo implements Iterable<LayuiTransferItem<T>> {

    /**
     * 树状选择项VO
     *
     * @author 胡高
     * @date 2021/12/20
     */
    public static class LayuiTransferItem<T> extends BaseVo {

        /**
         * serialVersionUID
         */
        private static final long serialVersionUID = -5604288608883878250L;

        /**
         * 标题
         */
        private String title;

        /**
         * 值
         */
        private String value;

        public LayuiTransferItem(T bean, String titleField, String valueField) {
            this.title = ConvertUtil.toStr(FieldUtil.getFieldValue(bean, titleField));
            this.value = ConvertUtil.toStr(FieldUtil.getFieldValue(bean, valueField));
        }

        /**
         * @return the id
         */
        public String getTitle() {
            return this.title;
        }

        /**
         * @return the name
         */
        public String getValue() {
            return this.value;
        }

    }

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3539879401089013189L;

    /**
     * 树对象数组
     */
    private List<LayuiTransferItem<T>> data;

    /**
     * 创建Transfor数据对象
     *
     * @param titleField
     *            标题字段名
     * @param valueField
     *            值字段名
     */
    public LayuiTransferData(List<T> beanList, String titleField, String valueField) {

        // 转换项目列表为树状选择项列表
        this.data = this.convertToItemList(beanList, titleField, valueField);

    }

    private List<LayuiTransferItem<T>> convertToItemList(List<T> list, String titleField, String valueField) {
        this.data = new ArrayList<>();
        for (T bean : list) {
            this.data.add(new LayuiTransferItem<>(bean, titleField, valueField));
        }

        return this.data;
    }

    @Override
    public Iterator<LayuiTransferItem<T>> iterator() {
        return this.data.iterator();
    }
}
