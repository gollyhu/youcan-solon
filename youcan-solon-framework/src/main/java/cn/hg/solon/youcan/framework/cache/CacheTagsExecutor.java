package cn.hg.solon.youcan.framework.cache;

import org.noear.solon.Solon;
import org.noear.solon.Utils;
import org.noear.solon.core.aspect.Invocation;
import org.noear.solon.core.util.SupplierEx;
import org.noear.solon.data.annotation.Cache;
import org.noear.solon.data.annotation.CachePut;
import org.noear.solon.data.annotation.CacheRemove;
import org.noear.solon.data.cache.CacheLib;
import org.noear.solon.data.cache.CacheService;
import org.noear.solon.data.util.InvKeys;

public class CacheTagsExecutor {
    public static final CacheTagsExecutor global = new CacheTagsExecutor();

    private static RedisCacheTagsService _cache;

    private RedisCacheTagsService getCache() {
        if (_cache == null) {
            _cache = Solon.context().getBean(RedisCacheTagsService.class);
        }

        return _cache;
    }

    public Object cache(Cache anno, Invocation inv, SupplierEx executor) throws Throwable {
        if (anno == null) {
            return executor.get();
        }

        //0.构建缓存key（如果有注解的key，优先用）
        String key = anno.key();
        if (Utils.isEmpty(key)) {
            //没有注解key，生成一个key
            key = InvKeys.buildByInv(inv);
        } else {
            //格式化key
            key = InvKeys.buildByTmlAndInv(key, inv);
        }


        Object result = null;
        CacheService cs = CacheLib.cacheServiceGet(anno.service());

        String keyLock = key + ":lock";

        synchronized (keyLock.intern()) {
            if (Utils.isNotEmpty(anno.tags())) {
                String annoTags = InvKeys.buildByTmlAndInv(anno.tags(), inv, null);
                String[] tags = annoTags.split(",");

                //1.从缓存获取
                //
                result = getCache().get(key, tags[0]);
                if (result == null) {
                    //2.执行调用，并返回
                    //
                    result = executor.get();

                    if (result != null) {
                        //3.不为null，则进行缓存
                        //
                        for (String tag : tags) {
                            getCache().storeTag(key, result, anno.seconds(), tag);
                        }
                    }
                }
            } else {
                //1.从缓存获取
                //
                result = cs.get(key, inv.method().getReturnType());
                if (result == null) {
                    //2.执行调用，并返回
                    //
                    result = executor.get();

                    if (result != null) {
                        //3.不为null，则进行缓存
                        //
                        cs.store(key, result, anno.seconds());
                    }
                }
            }

            return result;
        }
    }

    public void cachePut(CachePut anno, Invocation inv, Object rstValue) {
        if (anno == null) {
            return;
        }

        CacheService cs = CacheLib.cacheServiceGet(anno.service());
        String key = InvKeys.buildByTmlAndInv(anno.key(), inv, rstValue);

        //按 tags 更新缓存
        if (Utils.isNotEmpty(anno.tags())) {
            String tags = InvKeys.buildByTmlAndInv(anno.tags(), inv, rstValue);

            getCache().storeTag(key, rstValue, anno.seconds(), tags.split(","));
        } else if (Utils.isNotEmpty(anno.key())) {
            //按 key 更新缓存
            cs.store(key, rstValue, anno.seconds());
        }
    }

    public void cacheRemove(CacheRemove anno, Invocation inv, Object rstValue) {
        if (anno == null) {
            return;
        }

        CacheService cs = CacheLib.cacheServiceGet(anno.service());

        String keys = InvKeys.buildByTmlAndInv(anno.keys(), inv, rstValue);

        //按 tags 清除缓存
        if (Utils.isNotEmpty(anno.tags())) {
            String tags = InvKeys.buildByTmlAndInv(anno.tags(), inv, rstValue);
            getCache().removeTag(tags.split(","));
        } else if (Utils.isNotEmpty(anno.keys())) {
            //按 key 清除缓存
            for (String key : keys.split(",")) {
                cs.remove(key);
            }
        }
    }
}
