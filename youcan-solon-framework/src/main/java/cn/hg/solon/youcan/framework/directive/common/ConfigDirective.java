package cn.hg.solon.youcan.framework.directive.common;

import org.noear.solon.annotation.Component;

/**
 * 系统参数指令：#CONFIG('...')
 * 
 * @author 胡高
 */
@Component("view:" + ConfigDirective.DIRECTIVE_NAME)
public class ConfigDirective extends BaseConfigDirective {

    public static final String DIRECTIVE_NAME = "CONFIG";

    private static final String TYPE = "CONFIG";

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.framework.directive.common.BaseConfigDirective#getDirectiveName()
     */
    @Override
    public String getDirectiveName() {
        return DIRECTIVE_NAME;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.framework.directive.common.BaseConfigDirective#getType()
     */
    @Override
    public String getType() {
        return TYPE;
    }

}
