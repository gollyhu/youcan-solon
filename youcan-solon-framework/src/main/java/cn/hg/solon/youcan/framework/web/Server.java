package cn.hg.solon.youcan.framework.web;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.dromara.hutool.core.math.NumberUtil;
import org.dromara.hutool.core.thread.ThreadUtil;

import cn.hg.solon.youcan.framework.web.server.Cpu;
import cn.hg.solon.youcan.framework.web.server.Jvm;
import cn.hg.solon.youcan.framework.web.server.Mem;
import cn.hg.solon.youcan.framework.web.server.Sys;
import cn.hg.solon.youcan.framework.web.server.SysFile;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.CentralProcessor.TickType;
import oshi.hardware.GlobalMemory;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.software.os.FileSystem;
import oshi.software.os.OSFileStore;
import oshi.software.os.OperatingSystem;

/**
 * 服务器相关信息
 *
 * @author ruoyi
 */
public class Server
{

    private static final int OSHI_WAIT_SECOND = 1000;

    /**
     * 获取IP地址
     *
     * @return 本地IP地址
     */
    public static String getHostIp()
    {
        try
        {
            return InetAddress.getLocalHost().getHostAddress();
        }
        catch (UnknownHostException e)
        {
        }
        return "127.0.0.1";
    }

    /**
     * 获取主机名
     *
     * @return 本地主机名
     */
    public static String getHostName()
    {
        try
        {
            return InetAddress.getLocalHost().getHostName();
        }
        catch (UnknownHostException e)
        {
        }
        return "未知";
    }

    /**
     * CPU相关信息
     */
    private Cpu cpu = new Cpu();

    /**
     * 內存相关信息
     */
    private Mem mem = new Mem();

    /**
     * JVM相关信息
     */
    private Jvm jvm = new Jvm();

    /**
     * 服务器相关信息
     */
    private Sys sys = new Sys();

    /**
     * 磁盘相关信息
     */
    private List<SysFile> sysFiles = new LinkedList<>();

    /**
     * 字节转换
     *
     * @param size 字节大小
     * @return 转换后值
     */
    public String convertFileSize(long size)
    {
        long kb = 1024;
        long mb = kb * 1024;
        long gb = mb * 1024;
        if (size >= gb)
        {
            return String.format("%.1f GB", (float) size / gb);
        }
        else if (size >= mb)
        {
            float f = (float) size / mb;
            return String.format(f > 100 ? "%.0f MB" : "%.1f MB", f);
        }
        else if (size >= kb)
        {
            float f = (float) size / kb;
            return String.format(f > 100 ? "%.0f KB" : "%.1f KB", f);
        }
        else
        {
            return String.format("%d B", size);
        }
    }

    public void copyTo() throws Exception
    {
        SystemInfo si = new SystemInfo();
        HardwareAbstractionLayer hal = si.getHardware();

        this.setCpuInfo(hal.getProcessor());

        this.setMemInfo(hal.getMemory());

        this.setSysInfo();

        this.setJvmInfo();

        this.setSysFiles(si.getOperatingSystem());
    }

    public Cpu getCpu()
    {
        return this.cpu;
    }

    public Jvm getJvm()
    {
        return this.jvm;
    }

    public Mem getMem()
    {
        return this.mem;
    }

    public Sys getSys()
    {
        return this.sys;
    }

    public List<SysFile> getSysFiles()
    {
        return this.sysFiles;
    }

    public void setCpu(Cpu cpu)
    {
        this.cpu = cpu;
    }

    /**
     * 设置CPU信息
     */
    private void setCpuInfo(CentralProcessor processor)
    {
        // CPU信息
        long[] prevTicks = processor.getSystemCpuLoadTicks();
        ThreadUtil.safeSleep(OSHI_WAIT_SECOND);
        long[] ticks = processor.getSystemCpuLoadTicks();
        long nice = ticks[TickType.NICE.getIndex()] - prevTicks[TickType.NICE.getIndex()];
        long irq = ticks[TickType.IRQ.getIndex()] - prevTicks[TickType.IRQ.getIndex()];
        long softirq = ticks[TickType.SOFTIRQ.getIndex()] - prevTicks[TickType.SOFTIRQ.getIndex()];
        long steal = ticks[TickType.STEAL.getIndex()] - prevTicks[TickType.STEAL.getIndex()];
        long cSys = ticks[TickType.SYSTEM.getIndex()] - prevTicks[TickType.SYSTEM.getIndex()];
        long user = ticks[TickType.USER.getIndex()] - prevTicks[TickType.USER.getIndex()];
        long iowait = ticks[TickType.IOWAIT.getIndex()] - prevTicks[TickType.IOWAIT.getIndex()];
        long idle = ticks[TickType.IDLE.getIndex()] - prevTicks[TickType.IDLE.getIndex()];
        long totalCpu = user + nice + cSys + idle + iowait + irq + softirq + steal;
        this.cpu.setCpuNum(processor.getLogicalProcessorCount());
        this.cpu.setTotal(totalCpu);
        this.cpu.setSys(cSys);
        this.cpu.setUsed(user);
        this.cpu.setWait(iowait);
        this.cpu.setFree(idle);
    }

    public void setJvm(Jvm jvm)
    {
        this.jvm = jvm;
    }

    /**
     * 设置Java虚拟机
     */
    private void setJvmInfo() throws UnknownHostException
    {
        Properties props = System.getProperties();
        this.jvm.setTotal(Runtime.getRuntime().totalMemory());
        this.jvm.setMax(Runtime.getRuntime().maxMemory());
        this.jvm.setFree(Runtime.getRuntime().freeMemory());
        this.jvm.setVersion(props.getProperty("java.version"));
        this.jvm.setHome(props.getProperty("java.home"));
    }

    public void setMem(Mem mem)
    {
        this.mem = mem;
    }

    /**
     * 设置内存信息
     */
    private void setMemInfo(GlobalMemory memory)
    {
        this.mem.setTotal(memory.getTotal());
        this.mem.setUsed(memory.getTotal() - memory.getAvailable());
        this.mem.setFree(memory.getAvailable());
    }

    public void setSys(Sys sys)
    {
        this.sys = sys;
    }

    public void setSysFiles(List<SysFile> sysFiles)
    {
        this.sysFiles = sysFiles;
    }

    /**
     * 设置磁盘信息
     */
    private void setSysFiles(OperatingSystem os)
    {
        FileSystem fileSystem = os.getFileSystem();
        List<OSFileStore> fsArray = fileSystem.getFileStores();
        for (OSFileStore fs : fsArray)
        {
            long free = fs.getUsableSpace();
            long total = fs.getTotalSpace();
            long used = total - free;
            SysFile sysFile = new SysFile();
            sysFile.setDirName(fs.getMount());
            sysFile.setSysTypeName(fs.getType());
            sysFile.setTypeName(fs.getName());
            sysFile.setTotal(this.convertFileSize(total));
            sysFile.setFree(this.convertFileSize(free));
            sysFile.setUsed(this.convertFileSize(used));
            sysFile.setUsage(NumberUtil.mul(NumberUtil.div(used, total, 4), 100).doubleValue());
            this.sysFiles.add(sysFile);
        }
    }

    /**
     * 设置服务器信息
     */
    private void setSysInfo()
    {
        Properties props = System.getProperties();
        this.sys.setComputerName(getHostName());
        this.sys.setComputerIp(getHostIp());
        this.sys.setOsName(props.getProperty("os.name"));
        this.sys.setOsArch(props.getProperty("os.arch"));
        this.sys.setUserDir(props.getProperty("user.dir"));
    }

}
