package cn.hg.solon.youcan.framework.web.admin.filter;

import java.util.Date;

import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.date.DateField;
import org.dromara.hutool.core.date.DateUtil;
import org.dromara.hutool.core.math.NumberUtil;
import org.dromara.hutool.core.util.ObjUtil;
import org.noear.solon.core.event.EventBus;
import org.noear.solon.core.handle.Context;

import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.hg.solon.youcan.common.constant.OnlineConstants;

/**
 * @author 胡高
 */
public class UserOnlineSyncHandler {

    public static void handle(Context ctx) throws Throwable{
        // 如果未登录，则跳过
        if (!StpUtil.isLogin()) {
            return;
        }

        SaSession session = StpUtil.getTokenSession();
        SaTokenInfo token = StpUtil.getTokenInfo();

        // 刷新sa-token用户最后活动时间
        StpUtil.updateLastActiveToNow();

        /*
         * 更新在线表的最后活动时间
         */
        Date now = DateUtil.now();

        Date lastSyncTimestamp = (Date) session.get(OnlineConstants.LAST_SYNC_DB_TIMESTAMP);
        if (ObjUtil.isNull(lastSyncTimestamp) || (now.getTime() - lastSyncTimestamp.getTime()) > OnlineConstants.SYNC_PERIOD) {
            if (!NumberUtil.equals(-1L, token.getTokenActiveTimeout())) {
                session.set(OnlineConstants.EXPIRATION_DATETIME,
                    DateUtil.offset(now, DateField.SECOND, ConvertUtil.toInt(token.getTokenActiveTimeout())));
            }

            session.set(OnlineConstants.LAST_SYNC_DB_TIMESTAMP, now);
            EventBus.publishAsync(token);
        }
    }
}
