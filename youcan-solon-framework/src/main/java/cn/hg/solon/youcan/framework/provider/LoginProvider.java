package cn.hg.solon.youcan.framework.provider;

import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.hg.solon.youcan.common.constant.CacheConstants;
import cn.hg.solon.youcan.common.constant.OnlineConstants;
import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.common.enums.BusinessStatus;
import cn.hg.solon.youcan.common.enums.OnlineStatus;
import cn.hg.solon.youcan.common.exception.ServiceException;
import cn.hg.solon.youcan.common.util.PasswordUtil;
import cn.hg.solon.youcan.framework.satoken.SaUtil;
import cn.hg.solon.youcan.framework.service.LoginService;
import cn.hg.solon.youcan.system.entity.Dept;
import cn.hg.solon.youcan.system.entity.User;
import cn.hg.solon.youcan.system.entity.UserLogin;
import cn.hg.solon.youcan.system.entity.UserOnline;
import cn.hg.solon.youcan.system.service.DeptService;
import cn.hg.solon.youcan.system.service.UserOnlineService;
import cn.hg.solon.youcan.system.service.UserService;
import com.anji.captcha.model.common.ResponseModel;
import com.anji.captcha.model.vo.CaptchaVO;
import com.anji.captcha.service.CaptchaService;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.date.DateField;
import org.dromara.hutool.core.date.DateUtil;
import org.dromara.hutool.core.math.NumberUtil;
import org.dromara.hutool.core.text.CharSequenceUtil;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.http.useragent.UserAgent;
import org.dromara.hutool.http.useragent.UserAgentParser;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.event.EventBus;
import org.noear.solon.core.handle.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * @author 胡高
 */
@Component
public class LoginProvider implements LoginService {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private UserService userService;

    @Inject
    private DeptService deptService;

    @Inject
    private UserOnlineService userOnlineService;

    @Inject
    private CaptchaService captchaService;

    private void browserInfo(UserLogin login, Context ctx) {
        if (ObjUtil.isNull(ctx)) {
            return;
        }
        UserAgent agentInfo = UserAgentParser.parse(ctx.header("User-Agent"));

        login.setBrowser(agentInfo.getBrowser().getName());
        login.setOs(agentInfo.getOs().getName());
    }

    private void browserInfo(UserOnline online) {
        Context ctx = Context.current();
        if (ObjUtil.isNull(ctx)) {
            return;
        }
        UserAgent agentInfo = UserAgentParser.parse(ctx.header("User-Agent"));

        online.setBrowser(agentInfo.getBrowser().getName());
        online.setOs(agentInfo.getOs().getName());
    }

    @Override
    public User currentUser() throws ServiceException {
        if (!StpUtil.isLogin()) {
            return null;
        }

        Integer id = StpUtil.getLoginIdAsInt();
        if (ObjUtil.isNull(id)) {
            return null;
        }

        return (User)StpUtil.getTokenSession().get(CacheConstants.SYS_USER_TAG);
    }

    @Override
    public void login(String account, String password, String captcha) throws ServiceException {
        this.login(account, password, captcha, false);
    }

    @Override
    public void login(String account, String password, String captcha, boolean remember) throws ServiceException {
        CaptchaVO captchaVO = new CaptchaVO();
        captchaVO.setCaptchaVerification(captcha);

        ResponseModel response = this.captchaService.verification(captchaVO);

        if (!response.isSuccess()) {
            //登录日志
            this.loginLog(account, null, null, null, "验证码错误！");
            throw new ServiceException("验证码错误！");
        }

        User user = this.userService.getByAccount(account);

        if (ObjUtil.isNull(user)) {
            //登录日志
            this.loginLog(account, user, null, BusinessStatus.FAIL, "账号或密码错误！");
            throw new ServiceException("账号或密码错误！");
        }

        if (CharSequenceUtil.equals(user.getStatus(), BeanStatus.OFF.name())) {
            //登录日志
            this.loginLog(account, user, null, BusinessStatus.FAIL, "账号已经被禁用！");
            throw new ServiceException("账号已经被禁用！");
        }

        if (!PasswordUtil.validatePassword(user.getSalt(), password, user.getPassword())) {
            //登录日志
            this.loginLog(account, user, null, BusinessStatus.FAIL, "账号或密码错误！");
            throw new ServiceException("账号或密码错误！");
        }

        if (StpUtil.isLogin()) {
            StpUtil.logout(StpUtil.getLoginId());
        }

        /*
         * SaToken登录
         */
        StpUtil.login(user.getId(), remember);

        Dept dept = this.deptService.get(user.getDeptId());

        SaUtil.setCurrentUser(user, dept);

        //登录日志
        this.loginLog(account, user, dept, BusinessStatus.SUCCESS, "登录成功");

        /*
         * 在线处理
         */
        this.userOnline(user, dept);
    }

    private void loginLog(String account, User user, Dept dept, BusinessStatus status, String msg) {
        UserLogin login = new UserLogin();
        login.setAccount(account);
        Context ctx = Context.current();
        if (ObjUtil.isNotNull(ctx)) {
            login.setIp(ctx.realIp());
            login.setLoginDatetime(DateUtil.now());
            login.setMsg(msg);
            login.setStatus(status.name());
            this.browserInfo(login, ctx);
        }

        if (ObjUtil.isNotNull(user)) {
            login.setNickname(user.getNickname());
        }

        EventBus.publishAsync(login);

    }


    private void userOnline(User user, Dept dept) {
        /*
         * 创建Session
         */
        SaSession session = StpUtil.getTokenSession();
        SaTokenInfo token = StpUtil.getTokenInfo();
        Date now = DateUtil.now();

        if (NumberUtil.equals(-1L, token.getTokenActiveTimeout())) {
            session.set(OnlineConstants.EXPIRATION_DATETIME, OnlineConstants.MAX_DATE);
        } else {
            session.set(OnlineConstants.EXPIRATION_DATETIME,
                DateUtil.offset(now, DateField.SECOND, ConvertUtil.toInt(token.getTokenActiveTimeout())));
        }

        UserOnline online = this.userOnlineService.get(session.getId());

        if (ObjUtil.isNull(online)) {
            online = new UserOnline();
            online.setDeptName(dept.getName());
            online.setExpireSecond(ConvertUtil.toInt(token.getTokenActiveTimeout()));
            online.setId(token.getTokenValue());
            online.setDevice(StpUtil.getLoginDevice());
            online.setActivityDatetime(DateUtil.date(session.getCreateTime()));
            online.setNickname(user.getNickname());
            online.setStartDatetime(now);
            online.setStatus(OnlineStatus.ON.name());
            online.setUserId(ObjUtil.isNull(user) ? null : user.getId());
            if (ObjUtil.isNotNull(Context.current())) {
                online.setIp(Context.current().realIp());
            }
            this.browserInfo(online);

            this.userOnlineService.insert(online);

            EventBus.publishAsync(online);
        } else {
            online.setExpireSecond(ConvertUtil.toInt(session.getTimeout()));
            online.setActivityDatetime(DateUtil.now());
            online.setStatus(OnlineStatus.ON.name());

            this.userOnlineService.update(online);
        }
    }

}
