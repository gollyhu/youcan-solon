package cn.hg.solon.youcan.framework.service;

import java.util.List;

/**
 * @author 胡高
 */
public interface CacheService {

    /**
     * 清空缓存
     */
    void clear();

    /**
     * 清除指定缓存标签和指定缓存键的缓存
     * 
     * @param cacheTag 缓存标签
     * @param cacheKey 缓存键
     */
    void remove(String cacheTag, String cacheKey);

    /**
     * 清除与缓存标签相关的所有缓存
     * 
     * @param cacheTags 缓存标签表
     */
    void clear(String... cacheTags);

    /**
     * 获取指定缓存标签下所有的缓存键
     * 
     * @param cacheTag 缓存标签
     * @return 缓存键列表
     */
    List<?> listKeys(String cacheTag);

    /**
     * 获取所有缓存标签
     * 
     * @return 缓存标签
     */
    List<?> listTags();

    /**
     * 获取指定缓存标签、缓存键的缓存值
     * 
     * @param cacheTag 缓存标签
     * @param cacheKey 缓存键
     * @return 缓存值
     */
    Object getValue(String cacheTag, String cacheKey);

    /**
     * 获取指定缓存标签、缓存键的缓存时间
     *
     * @param cacheTag 缓存标签
     * @param cacheKey 缓存键
     * @return 缓存时间（秒）
     */
    long ttl(String cacheTag, String cacheKey);

    /**
     * 获取指定缓存键的缓存时间
     *
     * @param cacheKey 缓存键
     * @return 缓存时间（秒）
     */
    long ttl(String cacheKey);

}
