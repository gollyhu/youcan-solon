package cn.hg.solon.youcan.framework.web.server;

import org.dromara.hutool.core.math.NumberUtil;

/**
 * 內存相关信息
 *
 * @author ruoyi
 */
public class Mem
{
    /**
     * 内存总量
     */
    private double total;

    /**
     * 已用内存
     */
    private double used;

    /**
     * 剩余内存
     */
    private double free;

    public double getFree()
    {
        return NumberUtil.div(this.free, (1024 * 1024 * 1024), 2).doubleValue();
    }

    public double getTotal()
    {
        return NumberUtil.div(this.total, (1024 * 1024 * 1024), 2).doubleValue();
    }

    public double getUsage()
    {
        return NumberUtil.mul(NumberUtil.div(this.used, this.total, 4), 100).doubleValue();
    }

    public double getUsed()
    {
        return NumberUtil.div(this.used, (1024 * 1024 * 1024), 2).doubleValue();
    }

    public void setFree(long free)
    {
        this.free = free;
    }

    public void setTotal(long total)
    {
        this.total = total;
    }

    public void setUsed(long used)
    {
        this.used = used;
    }
}
