package cn.hg.solon.youcan.framework.directive.common;

import org.noear.solon.annotation.Component;

/**
 * 应用参数指令：#SITE('...')
 * 
 * @author 胡高
 */
@Component("view:" + AppDirective.DIRECTIVE_NAME)
public class AppDirective extends BaseConfigDirective {

    public static final String DIRECTIVE_NAME = "APP";

    private static final String TYPE = "APP";

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.framework.directive.common.BaseConfigDirective#getDirectiveName()
     */
    @Override
    public String getDirectiveName() {
        return DIRECTIVE_NAME;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.framework.directive.common.BaseConfigDirective#getType()
     */
    @Override
    public String getType() {
        return TYPE;
    }

}
