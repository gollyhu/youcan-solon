package cn.hg.solon.youcan.framework.cache.interceptor;

import cn.hg.solon.youcan.framework.cache.CacheTagsExecutor;
import org.noear.solon.Solon;
import org.noear.solon.core.aspect.Interceptor;
import org.noear.solon.core.aspect.Invocation;
import org.noear.solon.data.annotation.Cache;

public class CacheTagsInterceptor implements Interceptor {
    @Override
    public Object doIntercept(Invocation inv) throws Throwable {
        //支持动态开关缓存
        if (Solon.app().enableCaching()) {
            Cache anno = inv.getMethodAnnotation(Cache.class);

            return CacheTagsExecutor.global
                    .cache(anno, inv, () -> inv.invoke());
        } else {
            return inv.invoke();
        }

    }
}
