package cn.hg.solon.youcan.framework.listener;

import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.event.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.hg.solon.youcan.common.util.AddressUtil;
import cn.hg.solon.youcan.system.entity.OperateLog;
import cn.hg.solon.youcan.system.service.OperateLogService;

/**
 * @author 胡高
 */
@Component
public class OperateLogEventListener implements EventListener<OperateLog> {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private OperateLogService operateLogService;

    @Override
    public void onEvent(OperateLog operateLog) throws Throwable {
        this.log.debug("监听到操作日志：log={}", operateLog);

        /*
         * 获取IP所在的地址
         */
        operateLog.setLocation(AddressUtil.getRealAddressByIP(operateLog.getIp()));

        this.operateLogService.insert(operateLog);
    }

}
