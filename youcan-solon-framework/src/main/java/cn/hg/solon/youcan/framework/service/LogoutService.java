package cn.hg.solon.youcan.framework.service;

import java.util.List;

import cn.hg.solon.youcan.common.exception.ServiceException;

/**
 * @author 胡高
 */
public interface LogoutService {

    boolean kickout(List<String> tokens) throws ServiceException;

    boolean logout() throws ServiceException;
}
