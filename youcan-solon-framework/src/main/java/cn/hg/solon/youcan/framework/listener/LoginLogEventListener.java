package cn.hg.solon.youcan.framework.listener;

import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.event.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.hg.solon.youcan.common.util.AddressUtil;
import cn.hg.solon.youcan.system.entity.UserLogin;
import cn.hg.solon.youcan.system.service.UserLoginService;

/**
 * @author 胡高
 */
@Component
public class LoginLogEventListener implements EventListener<UserLogin> {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private UserLoginService userLoginService;

    @Override
    public void onEvent(UserLogin login) throws Throwable {
        this.log.debug("监听到登录日志：log={}", login);

        login.setLocation(AddressUtil.getRealAddressByIP(login.getIp()));

        this.userLoginService.insert(login);
    }

}
