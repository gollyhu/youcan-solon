package cn.hg.solon.youcan.framework.provider;

import cn.hg.solon.youcan.framework.cache.RedisCacheTagsService;
import cn.hg.solon.youcan.framework.service.CacheService;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import java.util.List;

/**
 * @author 胡高
 */
@Component
public class CacheProvider implements CacheService {

    @Inject
    private RedisCacheTagsService cacheTagsService;

    @Override
    public void clear() {
        List<?> cacheTags = this.listTags();
        for (Object cacheTag : cacheTags) {
            if (cacheTag instanceof String) {
                this.clear((String)cacheTag);
            }
        }
    }

    @Override
    public void remove(String cacheTag, String cacheKey) {
        this.cacheTagsService.removeMd5(cacheKey, cacheTag);
    }

    @Override
    public void clear(String... cacheTags) {
        this.cacheTagsService.removeTag(cacheTags);
    }

    @Override
    public List<?> listKeys(String cacheTag) {
        return this.cacheTagsService.getKeysAll(cacheTag);
    }

    @Override
    public List<?> listTags() {
        return this.cacheTagsService.getTagsAll();
    }

    @Override
    public Object getValue(String cacheTag, String cacheKey) {
        return this.cacheTagsService.getMd5(cacheKey, cacheTag);
    }

    @Override
    public long ttl(String key, String tag) {
        return this.cacheTagsService.ttlMd5(key, tag);
    }

    @Override
    public long ttl(String key) {
        return this.cacheTagsService.ttl(key);
    }

}
