package cn.hg.solon.youcan.framework.web.pear;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dromara.hutool.core.collection.CollUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.reflect.FieldUtil;
import org.dromara.hutool.core.util.ObjUtil;

import cn.hg.solon.youcan.common.vo.BaseVo;
import cn.hg.solon.youcan.framework.web.pear.LayuiTreeSelect.LayuiTreeSelectItem;

/**
 * Layui TreeSelect控件结果对象
 * <p>https://gitee.com/wujiawei0926/treeselect</p>
 *
 * @author 胡高
 */
public class LayuiTreeSelect<T extends Serializable> extends BaseVo implements Iterable<LayuiTreeSelectItem<T>> {

    /**
     * 树状选择项VO
     *
     * @author 胡高
     * @date 2021/12/20
     */
    public static class LayuiTreeSelectItem<T> extends BaseVo {

        /**
         * serialVersionUID
         */
        private static final long serialVersionUID = 3131301585276339948L;

        /**
         * ID
         */
        private Object id;

        /**
         * 名称
         */
        private String name;

        /**
         * 父ID
         */
        private Object parentId;

        /**
         * 是否展开
         */
        private boolean open = false;


        /**
         * 子项列表
         */
        private List<LayuiTreeSelectItem<T>> children = new ArrayList<>();

        public LayuiTreeSelectItem(T bean, String idField, String parentIdField, String nameField) {
            this.id = FieldUtil.getFieldValue(bean, idField);
            this.parentId = FieldUtil.getFieldValue(bean, parentIdField);
            this.name = ConvertUtil.toStr(FieldUtil.getFieldValue(bean, nameField));
        }

        /**
         * @return the children
         */
        public List<LayuiTreeSelectItem<T>> getChildren() {
            return this.children;
        }

        /**
         * @return the id
         */
        public Object getId() {
            return this.id;
        }

        /**
         * @return the name
         */
        public String getName() {
            return this.name;
        }

        public boolean getOpen() {
            return this.open;
        }

        /**
         * @return the parentId
         */
        public Object getParentId() {
            return this.parentId;
        }

        /**
         * @param children
         *            the children to set
         */
        protected void setChildren(List<LayuiTreeSelectItem<T>> children) {
            this.children = children;
            this.open = CollUtil.isNotEmpty(this.children);
        }

    }

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3562904133537565003L;

    /**
     * 树对象数组
     */
    private List<LayuiTreeSelectItem<T>> data;

    /**
     * 通过ID字段名、上级ID字段名、name字段名、初始顶层ID值创建树对象
     *
     * @param beanList bean对象列表
     * @param idField ID字段名
     * @param parentIdField 上级字段名
     * @param nameField name字段名
     * @param parentIdValue 初始顶层ID值
     */
    public LayuiTreeSelect(List<T> beanList, String idField, String parentIdField, String nameField,
        Object parentIdValue) {

        // 转换项目列表为树状选择项列表
        List<LayuiTreeSelectItem<T>> itemList =
            this.convertToItemList(beanList, idField, parentIdField, nameField, parentIdValue);

        // 构建树状对象
        this.data = this.buildTree(itemList, parentIdValue);
    }

    public void add(int index, LayuiTreeSelectItem<T> element) {
        this.data.add(index, element);
    }

    private List<LayuiTreeSelectItem<T>> buildTree(final List<LayuiTreeSelectItem<T>> beanList, Object parentId) {
        List<LayuiTreeSelectItem<T>> data = new ArrayList<>();

        for (LayuiTreeSelectItem<T> item : beanList) {

            if (ObjUtil.equals(parentId, item.getParentId())) {
                data.add(item);
            }

        }

        for (LayuiTreeSelectItem<T> org : data) {
            org.setChildren(this.buildTree(beanList, org.getId()));
        }

        return data;
    }

    private List<LayuiTreeSelectItem<T>> convertToItemList(List<T> list, String idName, String parentIdName,
        String nameField, Object initParentId) {
        List<LayuiTreeSelectItem<T>> itemList = new ArrayList<>();
        for (T bean : list) {
            itemList.add(new LayuiTreeSelectItem<>(bean, idName, parentIdName, nameField));
        }

        return itemList;
    }

    @Override
    public Iterator<LayuiTreeSelectItem<T>> iterator() {
        return this.data.iterator();
    }
}
