package cn.hg.solon.youcan.framework.directive.common;


import org.noear.solon.annotation.Component;

import com.jfinal.template.Env;
import com.jfinal.template.TemplateException;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

import cn.hg.solon.youcan.common.util.ContextPathUtil;
import cn.hg.solon.youcan.framework.directive.BaseDirective;

/**
 * 上下文路径指令：#CTX()
 * 
 * @author 胡高
 */
@Component("view:" + CtxDirective.DIRECTIVE_NAME)
public class CtxDirective extends BaseDirective {

    public static final String DIRECTIVE_NAME = "CTX";

    private String ctx = ContextPathUtil.getContextPath();

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        try {
            writer.write(this.ctx);
        } catch (Exception e) {
            throw new TemplateException(e.getMessage(), this.location, e);
        }
    }

}
