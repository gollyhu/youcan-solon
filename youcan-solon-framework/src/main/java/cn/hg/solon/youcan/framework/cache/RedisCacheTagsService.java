package cn.hg.solon.youcan.framework.cache;

import org.noear.solon.data.cache.CacheTagsService;

import java.util.List;

/**
 * @author 胡高
 */
public interface RedisCacheTagsService extends CacheTagsService {

    /**
     * 获取缓存
     *
     * @param key 缓存键
     * @param tag 缓存标签
     * @return 缓存值
     */
    Object get(String key, String tag);

    /**
     * 获取缓存（缓存键已经被MD5化）
     *
     * @param md5Key 已经通过MD5处理过的缓存键
     * @param tag    缓存标签
     * @return 缓存值
     */
    Object getMd5(String md5Key, String tag);

    /**
     * 通过缓存标签，获取缓存键列表
     *
     * @param tag 缓存标签
     * @return 缓存键列表
     */
    List<String> getKeysAll(String tag);

    /**
     * 获取缓存标签列表
     *
     * @return 缓存标签列表
     */
    List<String> getTagsAll();

    /**
     * 移除缓存
     *
     * @param key 缓存键
     * @param tag 缓存标签
     */
    void remove(String key, String tag);

    /**
     * 移除缓存（缓存键已经被MD5化）
     *
     * @param md5Key 已经通过MD5处理过的缓存键
     * @param tag 缓存标签
     */
    void removeMd5(String md5Key, String tag);

    /**
     * 获取缓存时间
     *
     * @param key 缓存键
     * @param tag 缓存标签
     * @return 缓存时间（秒）
     */
    long ttl(String key, String tag);

    /**
     * 获取缓存时间（缓存键已经被MD5化）
     *
     * @param md5Key 已经通过MD5处理过的缓存键
     * @param tag 缓存标签
     * @return 缓存时间（秒）
     */
    long ttlMd5(String md5Key, String tag);

    /**
     * 获取缓存时间
     *
     * @param key 缓存键
     * @return 缓存时间（秒）
     */
    long ttl(String key);

}
