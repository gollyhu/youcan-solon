package cn.hg.solon.youcan.framework.config;

import javax.sql.DataSource;

import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import com.zaxxer.hikari.HikariDataSource;

/**
 * 配置数据源
 */
@Configuration
public class DatasourceConfig {

    @Bean(value = "db1", typed = true) //不指定数据源名称，则采用默认数据源
    public DataSource db1(@Inject("${youcan.db1}") HikariDataSource dataSource) {
        return dataSource;
    }
}
