package cn.hg.solon.youcan.framework.config;

import cn.hg.solon.youcan.framework.cache.RedisCacheTagsProvider;
import cn.hg.solon.youcan.framework.cache.RedisCacheTagsService;
import cn.hg.solon.youcan.framework.cache.interceptor.CacheTagsInterceptor;
import cn.hg.solon.youcan.framework.cache.interceptor.CacheTagsPutInterceptor;
import cn.hg.solon.youcan.framework.cache.interceptor.CacheTagsRemoveInterceptor;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;
import org.noear.solon.data.annotation.Cache;
import org.noear.solon.data.annotation.CachePut;
import org.noear.solon.data.annotation.CacheRemove;

/**
 * @author 胡高
 */
@Configuration
public class CacheConfig {

    @Bean
    public RedisCacheTagsService redisCacheTagsService(@Inject("${youcan.cache}") RedisCacheTagsProvider cache) {
        cache.enableMd5key(!Solon.cfg().isDebugMode()); // 调试模式下，缓存Key不采用MD5加密
        return cache;
    }
}
