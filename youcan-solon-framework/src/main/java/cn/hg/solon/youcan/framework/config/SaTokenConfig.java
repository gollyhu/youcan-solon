/**
 *
 */
package cn.hg.solon.youcan.framework.config;

import java.util.function.Supplier;

import org.dromara.hutool.core.convert.ConvertUtil;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import cn.dev33.satoken.dao.SaTokenDao;
import cn.dev33.satoken.solon.dao.SaTokenDaoOfRedis;
import cn.dev33.satoken.solon.integration.SaTokenInterceptor;
import cn.dev33.satoken.stp.StpUtil;
import cn.hg.solon.youcan.common.constant.WebConstants;
import cn.hg.solon.youcan.framework.satoken.interceptor.SaAuthInterceptor;

/**
 * SaToken认证异常处理类
 *
 * @author 胡高
 *
 */
@Configuration
public class SaTokenConfig {

    /**
     * SaTokenDao 的 redis 适配
     */
    @Bean
    public SaTokenDao saTokenDaoInit(@Inject("${sa-token.dao.redis}") SaTokenDaoOfRedis saTokenDao) {
        return saTokenDao;
    }


    /**
     * sa-token认证拦截器
     */
    @Bean
    public SaTokenInterceptor saTokenInterceptor() {
        return new SaAuthInterceptor();
    }

    /**
     * 当前用户ID获取器
     * 
     * @return 当前用户ID
     */
    @Bean(WebConstants.USER_ID_GETTER)
    public Supplier<Integer> userIdGetter() {
        return () -> {
            return ConvertUtil.toInt(StpUtil.getLoginId());
        };
    }
}

