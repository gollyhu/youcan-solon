package cn.hg.solon.youcan.framework.web.pear;

import java.util.ArrayList;
import java.util.List;

import cn.hg.solon.youcan.common.vo.BaseVo;

/**
 * @author 胡高
 * @date 2023/06/16
 */
public class PearConfig extends BaseVo {

    /**
     * 主题色配置
     */
    public static class Color extends BaseVo {
        /**
         * 
         */
        private static final long serialVersionUID = -1929973786672792285L;
        /**
         * 配置ID
         */
        private String id;
        /**
         * 主色
         */
        private String color;
        /**
         * 次色
         */
        private String second;

        public String getColor() {
            return this.color;
        }

        public String getId() {
            return this.id;
        }

        public String getSecond() {
            return this.second;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setSecond(String second) {
            this.second = second;
        }
    }

    /**
     * 头部配置
     */
    public static class Header extends BaseVo {
        /**
         * 
         */
        private static final long serialVersionUID = 7314674523761588025L;
        /**
         * 站内消息，通过 false 设置关闭"/pear/admin/data/message.json"
         */
        private Object message = Boolean.FALSE;

        public Object getMessage() {
            return this.message;
        }

        public void setMessage(Object message) {
            this.message = message;
        }
    }

    /**
     * 网站配置
     */
    public static class Logo extends BaseVo {
        /**
         * 
         */
        private static final long serialVersionUID = 2278668338475272384L;
        /**
         * 网站名称
         */
        private String title;
        /**
         * 网站图标
         */
        private String image;

        public String getImage() {
            return this.image;
        }

        public String getTitle() {
            return this.title;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }

    /**
     * 菜单配置
     */
    public static class Menu extends BaseVo {
        /**
         * 
         */
        private static final long serialVersionUID = -3187540438102824775L;
        /**
         * 菜单数据来源
         */
        private String data;
        /**
         * 菜单接口的请求方式 GET / POST
         */
        private String method = "GET";
        /**
         * 是否同时只打开一个菜单目录
         */
        private Boolean accordion = Boolean.FALSE;
        /**
         * 侧边默认折叠状态
         */
        private Boolean collaspe = Boolean.FALSE;
        /**
         * 是否开启多系统菜单模式
         */
        private Boolean control = Boolean.FALSE;
        /**
         * 顶部菜单宽度 PX
         */
        private Integer controlWidth = 500;
        /**
         * 默认选中的菜单项
         */
        private String select;
        /**
         * 是否开启异步菜单，false 时 data 属性设置为静态数据，true 时为后端接口
         */
        private Boolean async = Boolean.FALSE;

        public Boolean getAccordion() {
            return this.accordion;
        }

        public Boolean getAsync() {
            return this.async;
        }

        public Boolean getCollaspe() {
            return this.collaspe;
        }

        public Boolean getControl() {
            return this.control;
        }

        public Integer getControlWidth() {
            return this.controlWidth;
        }

        public String getData() {
            return this.data;
        }

        public String getMethod() {
            return this.method;
        }

        public String getSelect() {
            return this.select;
        }

        public void setAccordion(Boolean accordion) {
            this.accordion = accordion;
        }

        public void setAsync(Boolean async) {
            this.async = async;
        }

        public void setCollaspe(Boolean collaspe) {
            this.collaspe = collaspe;
        }

        public void setControl(Boolean control) {
            this.control = control;
        }

        public void setControlWidth(Integer controlWidth) {
            this.controlWidth = controlWidth;
        }

        public void setData(String data) {
            this.data = data;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public void setSelect(String select) {
            this.select = select;
        }

    }

    /**
     * 其他配置
     */
    public static class Other extends BaseVo {
        /**
         * 
         */
        private static final long serialVersionUID = -2983459395408115548L;
        /**
         * 主页动画时长
         */
        private Integer keepLoad = 100;
        /**
         * 布局顶部主题
         */
        private Boolean autoHead = Boolean.FALSE;
        /**
         * 页脚
         */
        private Boolean footer = Boolean.FALSE;

        public Boolean getAutoHead() {
            return this.autoHead;
        }

        public Boolean getFooter() {
            return this.footer;
        }

        public Integer getKeepLoad() {
            return this.keepLoad;
        }

        public void setAutoHead(Boolean autoHead) {
            this.autoHead = autoHead;
        }

        public void setFooter(Boolean footer) {
            this.footer = footer;
        }

        public void setKeepLoad(Integer keepLoad) {
            this.keepLoad = keepLoad;
        }
    }

    /**
     * 视图内容配置
     */
    public static class Tab extends BaseVo {
        /**
         * 首页
         */
        public static class Index extends BaseVo {
            /**
             * 
             */
            private static final long serialVersionUID = 7615255106366484019L;
            /**
             * 标识 ID , 建议与菜单项中的 ID 一致
             */
            private String id;
            /**
             * 页面地址
             */
            private String href;
            /**
             * 标题
             */
            private String title;

            public String getHref() {
                return this.href;
            }

            public String getId() {
                return this.id;
            }

            public String getTitle() {
                return this.title;
            }

            public void setHref(String href) {
                this.href = href;
            }

            public void setId(String id) {
                this.id = id;
            }

            public void setTitle(String title) {
                this.title = title;
            }

        }

        /**
         * 
         */
        private static final long serialVersionUID = 5602515128712522474L;

        /**
         * 是否开启多选项卡
         */
        private Boolean enable = Boolean.TRUE;
        /**
         * 保持视图状态
         */
        private Boolean keepState = Boolean.TRUE;
        /**
         * 开启选项卡记忆
         */
        private Boolean session = Boolean.FALSE;
        /**
         * 浏览器刷新时是否预加载非激活标签页
         */
        private Boolean preload = Boolean.TRUE;
        /**
         * 最大可打开的选项卡数量
         */
        private Integer max = 10;

        /**
         * 首页
         */
        private Index index = new Index();

        public Boolean getEnable() {
            return this.enable;
        }

        public Index getIndex() {
            return this.index;
        }

        public Boolean getKeepState() {
            return this.keepState;
        }

        public Integer getMax() {
            return this.max;
        }

        public Boolean getPreload() {
            return this.preload;
        }

        public Boolean getSession() {
            return this.session;
        }

        public void setEnable(Boolean enable) {
            this.enable = enable;
        }

        public void setIndex(Index index) {
            this.index = index;
        }

        public void setKeepState(Boolean keepState) {
            this.keepState = keepState;
        }

        public void setMax(Integer max) {
            this.max = max;
        }

        public void setPreload(Boolean preload) {
            this.preload = preload;
        }

        public void setSession(Boolean session) {
            this.session = session;
        }
    }

    /**
     * 主题配置
     */
    public static class Theme extends BaseVo {
        /**
         * 
         */
        private static final long serialVersionUID = -6808516067720739390L;
        /**
         * 默认主题色，对应 colors 配置中的 ID 标识
         */
        private String defaultColor = "2";
        /**
         * 默认的菜单主题 dark-theme 黑 / light-theme 白
         */
        private String defaultMenu = "dark-theme";
        /**
         * 默认的顶部主题 dark-theme 黑 / light-theme 白
         */
        private String defaultHeader = "light-theme";
        /**
         * 是否允许用户切换主题，false 时关闭自定义主题面板
         */
        private Boolean allowCustom = Boolean.TRUE;
        /**
         * 通栏配置
         */
        private Boolean banner = Boolean.FALSE;

        public Boolean getAllowCustom() {
            return this.allowCustom;
        }

        public Boolean getBanner() {
            return this.banner;
        }

        public String getDefaultColor() {
            return this.defaultColor;
        }

        public String getDefaultHeader() {
            return this.defaultHeader;
        }

        public String getDefaultMenu() {
            return this.defaultMenu;
        }

        public void setAllowCustom(Boolean allowCustom) {
            this.allowCustom = allowCustom;
        }

        public void setBanner(Boolean banner) {
            this.banner = banner;
        }

        public void setDefaultColor(String defaultColor) {
            this.defaultColor = defaultColor;
        }

        public void setDefaultHeader(String defaultHeader) {
            this.defaultHeader = defaultHeader;
        }

        public void setDefaultMenu(String defaultMenu) {
            this.defaultMenu = defaultMenu;
        }

    }

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 5801164126125613498L;

    private Logo logo = new Logo();

    private List<Color> colors = new ArrayList<PearConfig.Color>();
    private Header header = new Header();
    private Menu menu = new Menu();
    private Other other = new Other();
    private Tab tab = new Tab();
    private Theme theme = new Theme();

    public List<Color> getColors() {
        return this.colors;
    }

    public Header getHeader() {
        return this.header;
    }

    public Logo getLogo() {
        return this.logo;
    }

    public Menu getMenu() {
        return this.menu;
    }

    public Other getOther() {
        return this.other;
    }

    public Tab getTab() {
        return this.tab;
    }

    public Theme getTheme() {
        return this.theme;
    }

    public void setColors(List<Color> colors) {
        this.colors = colors;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public void setLogo(Logo logo) {
        this.logo = logo;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public void setOther(Other other) {
        this.other = other;
    }

    public void setTab(Tab tab) {
        this.tab = tab;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

}
