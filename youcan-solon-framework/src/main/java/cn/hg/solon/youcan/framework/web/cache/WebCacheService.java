package cn.hg.solon.youcan.framework.web.cache;

import cn.hg.solon.youcan.framework.service.CacheService;
import org.dromara.hutool.core.collection.CollUtil;
import org.dromara.hutool.core.collection.ListUtil;
import org.dromara.hutool.core.text.CharSequenceUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 缓存操作处理
 */
@Component
public class WebCacheService {

    @Inject
    private CacheService cacheService;

    /**
     * 清理所有缓存
     */
    public void clearAll() {
        this.cacheService.clear();
    }

    /**
     * 根据名称和键名删除缓存信息
     *
     * @param cacheTag  缓存标签
     * @param cacheKeys 多个键名
     */
    public void removeByKeys(String cacheTag, String... cacheKeys) {
        for (String key : cacheKeys) {
            this.cacheService.remove(cacheTag, key);
        }
    }

    /**
     * 根据缓存标签删除缓存
     *
     * @param cacheTags 多个缓存标签
     */
    public void removeByTags(String... cacheTags) {
        this.cacheService.clear(cacheTags);
    }

    /**
     * 根据缓存标签获取所有键名
     *
     * @param cacheTag 缓存标签
     * @return 键名列表
     */
    @SuppressWarnings("unchecked")
    public PageResult<String> getKeysPage(int pageNumber, int pageSize, String cacheTag, String word) {
        List<String> keysList = (List<String>) this.cacheService.listKeys(cacheTag);

        /*
         * 删除不包含word的成员
         */
        return getResultList(pageNumber, pageSize, word, keysList);
    }

    /**
     * 获取所有缓存标签
     *
     * @return 缓存列表
     */
    @SuppressWarnings("unchecked")
    public PageResult<String> getTagsPage(int pageNumber, int pageSize, String word) {
        List<String> namesList = (List<String>) this.cacheService.listTags();

        /*
         * 删除不包含word的成员
         */
        return getResultList(pageNumber, pageSize, word, namesList);
    }

    private PageResult<String> getResultList(int pageNumber, int pageSize, String word, List<String> list) {
        if (StrValidator.isNotBlank(word)) {
            list = list.stream().filter(item -> CharSequenceUtil.containsAnyIgnoreCase(item, word))
                    .collect(Collectors.toList());
        }

        List<String> pageList = ListUtil.page(list, pageNumber - 1, pageSize);

        PageResult<String> page = new PageResult<>();
        page.addAll(pageList);
        page.setPage(pageNumber);
        page.setPageSize(pageSize);
        page.setTotal(CollUtil.size(list));

        return page;
    }

    /**
     * 根据缓存标签和键名获取内容值
     *
     * @param cacheTag 缓存标签
     * @param cacheKey 键名
     * @return 键值
     */
    public Object getValue(final String cacheTag, final String cacheKey) {
        return this.cacheService.getValue(cacheTag, cacheKey);
    }

    public Long ttl(final String cacheTag, final String cacheKey) {
        return this.cacheService.ttl(cacheKey, cacheTag);
    }
}
