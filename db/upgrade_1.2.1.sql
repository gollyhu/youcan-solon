ALTER TABLE `sys_job`
    MODIFY COLUMN `is_concurrent` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否并发执行' AFTER `policy`,
    ADD COLUMN `is_log` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否记录调度日志' AFTER `remark`;

UPDATE `sys_job` SET `is_log` = b'0' WHERE `id` = 5;
