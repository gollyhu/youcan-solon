/*
 Navicat Premium Dump SQL

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726 (5.7.26)
 Source Host           : localhost:3306
 Source Schema         : youcan_solon

 Target Server Type    : MySQL
 Target Server Version : 50726 (5.7.26)
 File Encoding         : 65001

 Date: 04/09/2024 18:07:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数名称',
  `code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数代码',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数值',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'APP' COMMENT '参数类型（SITE 站点，COMPANY 公司，CONFIG 参数。。。类型也可以自己定义）',
  `is_sys` bit(1) NOT NULL DEFAULT b'0' COMMENT '系统内置（true 是，false 否），内置参数不允许删除',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）',
  `creator` int(11) NOT NULL DEFAULT 1 COMMENT '创建者',
  `created_datetime` datetime NOT NULL COMMENT '创建时间',
  `editor` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `edited_datetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `key`(`code`) USING BTREE,
  INDEX `type`(`type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '站点全称', 'fullname', 'Youcan-Solon', 'SITE', b'1', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '站点的全称');
INSERT INTO `sys_config` VALUES (2, '网站小图标', 'icon', '/pear/admin/images/logo.png', 'SITE', b'1', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '图标的URL，以 http、https 或 / 开头');
INSERT INTO `sys_config` VALUES (3, 'SEO关键字', 'seo_meta_keywords', 'solon,youcan,cms,golly,flex,layui,pear,sa-token', 'SITE', b'1', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '网页头部Meta属性keywords的值，方便SEO收录');
INSERT INTO `sys_config` VALUES (4, 'SEO描述', 'seo_meta_description', 'YoucanSolon', 'SITE', b'1', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '网页头部Meta属性description的值，方便SEO收录');
INSERT INTO `sys_config` VALUES (5, '站点标题', 'title', 'Youcan-Solon', 'SITE', b'1', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '站点的标题，通常用于显示Web页面的标签名');
INSERT INTO `sys_config` VALUES (7, '公司全称', 'name', 'Youcan-Solon', 'COMPANY', b'0', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '在工商注册的公司全名');
INSERT INTO `sys_config` VALUES (8, '公司简称', 'abbreviation', 'Youcan', 'COMPANY', b'0', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '公司简称');
INSERT INTO `sys_config` VALUES (9, '公司地址', 'address', '宇宙 银行系 太阳系 地球 中国 广东 深圳', 'COMPANY', b'0', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '公司详细地址');
INSERT INTO `sys_config` VALUES (10, '公司电话', 'phone', '0755-888888', 'COMPANY', b'0', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '公司对外业务电话号码');
INSERT INTO `sys_config` VALUES (11, '联系人员', 'contact', '糊搞', 'COMPANY', b'0', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '业务联系人员名称');
INSERT INTO `sys_config` VALUES (12, '联系电话', 'contact_phone', '13000000000', 'COMPANY', b'0', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '业务人员联系电话');
INSERT INTO `sys_config` VALUES (13, '应用主页', 'main_page', '/', 'APP', b'1', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '网站主页URL地址');
INSERT INTO `sys_config` VALUES (14, 'Pear Admin Config', 'admin_config', '{\r\n  \"logo\": {\r\n    \"title\": \"Youcan-Solon\",\r\n    \"image\": \"#CTX()/pear/admin/images/logo.png\"\r\n  },\r\n  \"colors\": [\r\n    {\r\n      \"id\": \"1\",\r\n      \"color\": \"#2d8cf0\",\r\n      \"second\": \"#ecf5ff\"\r\n    },\r\n    {\r\n      \"id\": \"2\",\r\n      \"color\": \"#36b368\",\r\n      \"second\": \"#f0f9eb\"\r\n    },\r\n    {\r\n      \"id\": \"3\",\r\n      \"color\": \"#f6ad55\",\r\n      \"second\": \"#fdf6ec\"\r\n    },\r\n    {\r\n      \"id\": \"4\",\r\n      \"color\": \"#f56c6c\",\r\n      \"second\": \"#fef0f0\"\r\n    },\r\n    {\r\n      \"id\": \"5\",\r\n      \"color\": \"#3963bc\",\r\n      \"second\": \"#ecf5ff\"\r\n    }\r\n  ],\r\n  \"header\": {\r\n    \"message\": false\r\n  },\r\n  \"menu\": {\r\n    \"data\": \"#CTX()/admin/menu?ids=1\",\r\n    \"method\": \"GET\",\r\n    \"accordion\": true,\r\n    \"collaspe\": false,\r\n    \"control\": false,\r\n    \"controlWidth\": 500,\r\n    \"select\": \"0\",\r\n    \"async\": true\r\n  },\r\n  \"other\": {\r\n    \"keepLoad\": 100,\r\n    \"autoHead\": false,\r\n    \"footer\": true\r\n  },\r\n  \"tab\": {\r\n    \"enable\": true,\r\n    \"keepState\": true,\r\n    \"session\": false,\r\n    \"preload\": false,\r\n    \"max\": 10,\r\n    \"index\": {\r\n      \"id\": \"2\",\r\n      \"href\": \"#CTX()/admin/home\",\r\n      \"title\": \"首页\"\r\n    }\r\n  },\r\n  \"theme\": {\r\n    \"defaultColor\": \"2\",\r\n    \"defaultMenu\": \"dark-theme\",\r\n    \"defaultHeader\": \"light-theme\",\r\n    \"allowCustom\": true,\r\n    \"banner\": false\r\n  }\r\n}', 'APP', b'1', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '管理平台Web应用的Config Json配置参数。\n此配置如果为空则加载系统内置的Config。');
INSERT INTO `sys_config` VALUES (15, '水印开关', 'show_watermark', 'true', 'APP', b'1', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '此参数用于控制水印的显示，参数值为\"true\"时显示。');
INSERT INTO `sys_config` VALUES (16, 'ICP备案号', 'icp', '粤ICP备00000000号', 'SITE', b'0', b'0', 1, '2024-04-03 14:47:23', NULL, NULL, 'ICP的备案号，网站可见位置显示此值用于备案核验');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '父部门ID',
  `ancestors` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '祖级ID列表',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门名',
  `sort` smallint(6) NULL DEFAULT NULL COMMENT '显示排序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'ON' COMMENT '状态（ON 正常, OFF 停用）',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）',
  `creator` int(11) NOT NULL DEFAULT 1 COMMENT '创建人',
  `created_datetime` datetime NOT NULL COMMENT '创建时间',
  `editor` int(11) NULL DEFAULT NULL COMMENT '编辑人',
  `edited_datetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, 0, '0', 'Youcan-Solon', 10, 'Youcan', '13000000000', '3722711@qq.com', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dept` VALUES (2, 1, '0,1', '深圳总公司', 10, 'Youcan', '13000000000', '3722711@qq.com', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dept` VALUES (3, 1, '0,1', '长沙分公司', 20, 'Youcan', '13000000000', '3722711@qq.com', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dept` VALUES (4, 2, '0,1,2', '研发部门', 10, 'Youcan', '13000000000', '3722711@qq.com', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dept` VALUES (5, 2, '0,1,2', '市场部门', 20, 'Youcan', '13000000000', '3722711@qq.com', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dept` VALUES (6, 2, '0,1,2', '测试部门', 30, 'Youcan', '13000000000', '3722711@qq.com', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dept` VALUES (7, 2, '0,1,2', '财务部门', 40, 'Youcan', '13000000000', '3722711@qq.com', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dept` VALUES (8, 2, '0,1,2', '运维部门', 50, 'Youcan', '13000000000', '3722711@qq.com', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dept` VALUES (9, 3, '0,1,3', '市场部门', 10, 'Youcan', '13000000000', '3722711@qq.com', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dept` VALUES (10, 3, '0,1,3', '财务部门', 20, 'Youcan', '13000000000', '3722711@qq.com', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典名称',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典类型',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'ON' COMMENT '状态（ON 正常, OFF 停用）',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）',
  `creator` int(11) NOT NULL DEFAULT 1 COMMENT '创建者',
  `created_datetime` datetime NOT NULL COMMENT '创建时间',
  `editor` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `edited_datetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, '用户性别', 'user_gender', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '用户性别列表');
INSERT INTO `sys_dict` VALUES (2, '权限类型', 'permission_type', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '权限类型列表');
INSERT INTO `sys_dict` VALUES (3, '系统开关', 'normal_disable', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '系统开关列表');
INSERT INTO `sys_dict` VALUES (4, '任务状态', 'job_status', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '任务状态列表');
INSERT INTO `sys_dict` VALUES (5, '任务分组', 'job_group', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '任务分组列表');
INSERT INTO `sys_dict` VALUES (6, '系统是否', 'yes_no', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '系统是否列表');
INSERT INTO `sys_dict` VALUES (7, '通知类型', 'notice_type', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '通知类型列表');
INSERT INTO `sys_dict` VALUES (8, '通知状态', 'notice_status', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '通知状态列表');
INSERT INTO `sys_dict` VALUES (9, '操作类型', 'operate_type', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '操作类型列表');
INSERT INTO `sys_dict` VALUES (10, '在线状态', 'user_online_status', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '登录状态列表');
INSERT INTO `sys_dict` VALUES (11, '记录状态', 'bean_status', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '记录状态列表');
INSERT INTO `sys_dict` VALUES (12, '页面样式', 'css_class', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '回显样式列表，尽量不要修改');
INSERT INTO `sys_dict` VALUES (13, '参数类型', 'config_type', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '系统参数的类型列表');
INSERT INTO `sys_dict` VALUES (14, '菜单打开方式', 'menu_target', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '菜单打开方式列表（_iframe 正常打开，_blank 新建窗口）');
INSERT INTO `sys_dict` VALUES (15, '菜单请求方法', 'menu_method', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '菜单请求方法列表：GET，POST');
INSERT INTO `sys_dict` VALUES (16, '角色数据范围', 'role_data_scope', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '角色数据范围列表');
INSERT INTO `sys_dict` VALUES (17, '任务调度状态', 'job_execution_status', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '任务调度状态列表');
INSERT INTO `sys_dict` VALUES (18, '操作状态', 'operate_status', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '操作状态字典列表');
INSERT INTO `sys_dict` VALUES (19, 'HTTP请求方法', 'request_method', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, 'HTTP请求方法列表');
INSERT INTO `sys_dict` VALUES (20, '用户登录状态', 'user_login_status', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '用户登录状态列表');

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `sort` smallint(6) NOT NULL DEFAULT 0 COMMENT '显示排序',
  `label` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典标签',
  `value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典键值',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典类型',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` bit(1) NOT NULL COMMENT '是否默认（0 否，1 是）',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'ON' COMMENT '状态（ON 正常, OFF 停用,）',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）',
  `creator` int(11) NOT NULL DEFAULT 1 COMMENT '创建人',
  `created_datetime` datetime NOT NULL COMMENT '创建时间',
  `editor` int(11) NULL DEFAULT NULL COMMENT '编辑人',
  `edited_datetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典项目表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
INSERT INTO `sys_dict_item` VALUES (1, 20, '男♂', 'M', 'user_gender', '性别男', NULL, 'layui-badge layui-bg-blue', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (2, 30, '女♀', 'F', 'user_gender', '性别女', NULL, 'layui-badge layui-bg-red', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (3, 10, '未知', 'U', 'user_gender', '性别未知', NULL, 'layui-badge layui-bg-gray', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (4, 10, '目录', 'NODE', 'permission_type', '目录', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (5, 20, '菜单', 'MENU', 'permission_type', '菜单', NULL, 'layui-badge layui-bg-blue', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (6, 1, '正常', 'ON', 'normal_disable', '正常状态', NULL, 'layui-badge layui-bg-blue', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (7, 2, '停用', 'OFF', 'normal_disable', '停用状态', NULL, 'layui-badge layui-bg-orange', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (8, 1, '启用', 'ON', 'job_status', '启用状态', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (9, 2, '停用', 'OFF', 'job_status', '停用状态', NULL, 'layui-badge layui-bg-orange', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (10, 1, '默认', 'DEFAULT', 'job_group', '默认分组', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (11, 2, '系统', 'SYSTEM', 'job_group', '系统分组', NULL, 'layui-badge layui-bg-black', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (12, 1, '是', '1', 'yes_no', '系统默认是', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (13, 2, '否', '0', 'yes_no', '系统默认否', NULL, 'layui-badge layui-bg-red', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (14, 1, '通知', 'NOTICE', 'notice_type', '通知', NULL, 'layui-badge layui-bg-blue', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (15, 2, '公告', 'ANNOUNCEMENT', 'notice_type', '公告', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (16, 1, '正常', 'ON', 'notice_status', '正常状态', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (17, 2, '关闭', 'OFF', 'notice_status', '关闭状态', NULL, 'layui-badge layui-bg-orange', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (18, 99, '其他', 'OTHER', 'operate_type', '其他操作', NULL, 'layui-badge layui-bg-gray', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (19, 1, '新增', 'CREATE', 'operate_type', '新增操作', NULL, 'layui-badge layui-bg-blue', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (20, 2, '修改', 'UPDATE', 'operate_type', '修改操作', NULL, 'layui-badge layui-bg-orange', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (21, 3, '删除', 'DELETE', 'operate_type', '删除操作', NULL, 'layui-badge layui-bg-red', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (22, 4, '授权', 'GRANT', 'operate_type', '授权操作', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (23, 5, '导出', 'EXPORT', 'operate_type', '导出操作', NULL, 'layui-badge layui-bg-cyan', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (24, 6, '导入', 'IMPORT', 'operate_type', '导入操作', NULL, 'layui-badge layui-bg-black', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (25, 7, '强退', 'FORCE', 'operate_type', '强退操作', NULL, 'layui-badge layui-bg-red', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (26, 8, '生成代码', 'GENERATE', 'operate_type', '生成操作', NULL, 'layui-badge layui-bg-orange', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (27, 9, '清空数据', 'CLEAR', 'operate_type', '清空操作', NULL, 'layui-badge layui-bg-red', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (28, 1, '在线', 'ON', 'user_online_status', '在线状态', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (29, 2, '离线', 'OFF', 'user_online_status', '离线状态', NULL, 'layui-badge layui-bg-red', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (30, 10, '启用', 'ON', 'bean_status', '启用状态', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (31, 20, '禁用', 'OFF', 'bean_status', '禁用状态', NULL, 'layui-badge layui-bg-gray', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (32, 30, '删除', 'DEL', 'bean_status', '软删除', NULL, 'layui-badge layui-bg-red', b'0', 'OFF', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (33, 10, '红色', 'layui-badge layui-bg-red', 'css_class', '危险', '', 'layui-badge layui-bg-red', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (34, 20, '橙色', 'layui-badge layui-bg-orange', 'css_class', '警告', '', 'layui-badge layui-bg-orange', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (35, 30, '绿色', 'layui-badge layui-bg-green', 'css_class', '成功', '', 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (36, 40, '藏青', 'layui-badge layui-bg-cyan', 'css_class', '次要', NULL, 'layui-badge layui-bg-cyan', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (37, 50, '蓝色', 'layui-badge layui-bg-blue', 'css_class', '主要', '', 'layui-badge layui-bg-blue', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (38, 60, '黑色', 'layui-badge layui-bg-black', 'css_class', '暗色', '', 'layui-badge layui-bg-black', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (39, 70, '灰色', 'layui-badge layui-bg-gray', 'css_class', '亮色', '', 'layui-badge layui-bg-gray', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (40, 10, '站点参数', 'SITE', 'config_type', '站点参数', NULL, 'layui-badge layui-bg-blue', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (41, 20, '公司参数', 'COMPANY', 'config_type', '公司参数', NULL, 'layui-badge layui-bg-cyan', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (42, 30, '应用参数', 'APP', 'config_type', '应用参数', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (43, 40, '设置参数', 'CONFIG', 'config_type', '设置参数', NULL, 'layui-badge layui-bg-orange', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (44, 30, '功能', 'FUNCTION', 'permission_type', '功能', NULL, 'layui-badge layui-bg-orange', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (45, 40, '字段', 'FIELD', 'permission_type', '字段', NULL, 'layui-badge layui-bg-black', b'0', 'OFF', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (46, 10, '页签', '_iframe', 'menu_target', '页签', NULL, 'layui-badge layui-bg-blue', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (47, 20, '新窗口', '_blank', 'menu_target', '新窗口', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (48, 10, 'GET请求', 'GET', 'menu_method', 'GET请求', NULL, 'layui-badge layui-bg-blue', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (49, 20, 'POST请求', 'POST', 'menu_method', 'POST请求', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (50, 10, '全部数据权限', 'ALL', 'role_data_scope', '全部数据权限', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (51, 20, '自定数据权限', 'CUSTOM', 'role_data_scope', '自定数据权限', NULL, 'layui-badge layui-bg-orange', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (52, 30, '本部门数据权限', 'DEPARTMENT', 'role_data_scope', '本部门数据权限', NULL, 'layui-badge layui-bg-blue', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (53, 40, '本部门及以下数据权限', 'BELOW', 'role_data_scope', '本部门及以下数据权限', NULL, 'layui-badge layui-bg-black', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (54, 10, '成功', 'SUCCESS', 'job_execution_status', '成功', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (55, 20, '失败', 'FAIL', 'job_execution_status', '失败', NULL, 'layui-badge layui-bg-red', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (56, 10, '成功', 'SUCCESS', 'operate_status', '成功', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (57, 20, '失败', 'FAIL', 'operate_status', '失败', NULL, 'layui-badge layui-bg-red', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (58, 10, 'GET', 'GET', 'request_method', '获取资源', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (59, 20, 'POST', 'POST', 'request_method', '新建资源', NULL, 'layui-badge layui-bg-orange', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (60, 30, 'PUT', 'PUT', 'request_method', '修改资源', NULL, 'layui-badge layui-bg-blue', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (61, 40, 'DELETE', 'DELETE', 'request_method', '删除资源', NULL, 'layui-badge layui-bg-red', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (62, 10, '成功', 'SUCCESS', 'user_login_status', '成功', NULL, 'layui-badge layui-bg-green', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (63, 20, '失败', 'FAIL', 'user_login_status', '失败', NULL, 'layui-badge layui-bg-red', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (64, 50, 'HEAD', 'HEAD', 'request_method', '相当于GET，但不返回内容', NULL, 'layui-badge layui-bg-cyan', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (65, 60, 'OPTIONS', 'OPTIONS', 'request_method', '获取服务器支持的HTTP请求方法', NULL, 'layui-badge layui-bg-cyan', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (66, 70, 'TRACE', 'TRACE', 'request_method', '回馈服务器收到的请求，用于远程诊断服务器', NULL, 'layui-badge layui-bg-cyan', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (67, 80, 'CONNECT', 'CONNECT', 'request_method', '用于代理进行传输', NULL, 'layui-badge layui-bg-cyan', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (68, 45, 'PATCH', 'PATCH', 'request_method', '修改资源，客户端只提供改变的局部属性', NULL, 'layui-badge layui-bg-cyan', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (69, 90, 'UNKNOWN', 'UNKNOWN', 'request_method', '未知方法', NULL, 'layui-badge layui-bg-gray', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES (70, 80, '紫色', 'layui-badge layui-bg-purple', 'css_class', '突显', '', 'layui-badge layui-bg-purple', b'0', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `group` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `target` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'cron执行表达式',
  `policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'ABORT' COMMENT '计划执行错误策略（IMMEDIATELY 立即执行，ONCE 执行一次，ABORT 放弃执行）',
  `is_concurrent` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否并发执行',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'OFF' COMMENT '任务状态（ON 启用, OFF 停用）',
  `creator` int(11) NOT NULL DEFAULT 1 COMMENT '创建者',
  `created_datetime` datetime NOT NULL COMMENT '创建时间',
  `editor` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `edited_datetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `is_log` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否记录调度日志',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `group`(`group`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '演示任务（无参）', 'DEFAULT', 'demoTask.noParams', '*/1 * * * *', 'ABORT', b'0', 'OFF', 1, '2023-08-08 00:00:00', NULL, NULL, NULL, b'1');
INSERT INTO `sys_job` VALUES (2, '演示任务（有参）', 'DEFAULT', 'demoTask.params(\'demo\')', '*/5 * * * *', 'ABORT', b'0', 'OFF', 1, '2023-08-08 00:00:00', NULL, NULL, NULL, b'1');
INSERT INTO `sys_job` VALUES (3, '演示任务（多参）', 'DEFAULT', 'demoTask.multipleParams(\'demo\', true, 2000L, 316.50D, 100)', '*/10 * * * *', 'ABORT', b'0', 'OFF', 1, '2023-08-08 00:00:00', NULL, NULL, NULL, b'1');
INSERT INTO `sys_job` VALUES (4, '演示任务（异常）', 'DEFAULT', 'cn.hg.solon.youcan.job.demo.DemoTask.exception()', '*/30 * * * *', 'ABORT', b'0', 'OFF', 1, '2023-08-08 00:00:00', NULL, NULL, NULL, b'1');
INSERT INTO `sys_job` VALUES (5, '用户在线状态任务', 'SYSTEM', 'usrOnlineTask.processOfflines', '*/1 * * * *', 'ABORT', b'0', 'ON', 1, '2023-08-08 00:00:00', NULL, NULL, '每分钟轮询用户在线表，将实际已经不在线的记录状态变更为下线', b'0');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `target` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务状态：SUCCESS 成功，FAIL 失败',
  `exception` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '异常信息',
  `cost` bigint(20) NULL DEFAULT 0 COMMENT '执行时间（ms）',
  `created_datetime` datetime NOT NULL COMMENT '创建时间',
  `started_datetime` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `ended_datetime` datetime NULL DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `group`(`group`) USING BTREE,
  INDEX `status`(`status`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `created_datetime`(`created_datetime`) USING BTREE,
  INDEX `started_datetime`(`started_datetime`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'NOTICE' COMMENT '公告类型：NOTICE 通知, ANNOUNCEMENT 公告',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '公告内容',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'ON' COMMENT '状态（ON 正常, OFF 停用）',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）',
  `creator` int(11) NOT NULL DEFAULT 1 COMMENT '创建者',
  `created_datetime` datetime NOT NULL COMMENT '创建时间',
  `editor` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `edited_datetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2023-08-31 新版本发布啦', 'ANNOUNCEMENT', '<p><strong>新版本内容：</strong></p>\n<ol>\n<li>更新通知公告功能；</li>\n<li>修正总分Bug；</li>\n</ol>', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, NULL);
INSERT INTO `sys_notice` VALUES (2, '维护通知：2023-09-01 系统凌晨维护', 'NOTICE', '<p>维护内容：</p>\n<ul>\n<li>1.0版本上线；</li>\n<li>数据库同步更新；</li>\n</ul>', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_operate_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_operate_log`;
CREATE TABLE `sys_operate_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模块标题',
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作类型：OTHER 其他操作，CREATE 新增操作，UPDATE 修改操作，DELETE 删除操作，GRANT 授权操作，EXPORT 导出操作，IMPORT 导入操作，EXIT 强退操作，GENERATE 生成操作，CLEAR 清空操作',
  `method` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方式',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '操作人ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作人员',
  `dept_Id` int(11) NULL DEFAULT NULL COMMENT '部门ID',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门名',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求URL',
  `ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作地点',
  `param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求参数',
  `result` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '返回参数',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作状态：SUCCESS 成功，FAIL 失败',
  `message` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '错误消息',
  `action_datetime` datetime NULL DEFAULT NULL COMMENT '操作时间',
  `cost` mediumint(9) NULL DEFAULT NULL COMMENT '消耗时间（ms)',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `title`(`title`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE,
  INDEX `action_datetime`(`action_datetime`) USING BTREE,
  INDEX `dept_Id`(`dept_Id`) USING BTREE,
  INDEX `request_method`(`request_method`) USING BTREE,
  INDEX `method`(`method`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_operate_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限名称',
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '父菜单ID',
  `ancestors` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '祖级ID列表',
  `sort` int(6) NULL DEFAULT 0 COMMENT '显示顺序',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '打开方式（_iframe 正常打开，_blank 新建窗口）',
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'NODE' COMMENT '类型（NODE 节点，MENU 菜单，FUNCTION 功能按钮）',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'ON' COMMENT '状态（ON 正常，OFF 停用）',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）',
  `method` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'GET' COMMENT '请求方法',
  `value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限值',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `css_class` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '页面Class',
  `creator` int(11) NOT NULL DEFAULT 1 COMMENT '创建人',
  `created_datetime` datetime NOT NULL COMMENT '创建时间',
  `editor` int(11) NULL DEFAULT NULL COMMENT '编辑人',
  `edited_datetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `sort`(`sort`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 174 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES (1, 'Youcan-Solon 管理端', 0, '0', 10, '#', NULL, 'NODE', 'ON', b'0', NULL, NULL, NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (2, '首页', 1, '0,1', 10, '/admin/home', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:system:home', 'layui-icon-home', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (3, '系统管理', 1, '0,1', 20, '#', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:system', 'layui-icon-set-sm', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (4, '系统监控', 1, '0,1', 30, '#', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:monitor', 'layui-icon-chart-screen', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (5, '系统工具', 1, '0,1', 40, '#', '_iframe', 'MENU', 'OFF', b'0', 'GET', 'menu:tool', 'layui-icon-util', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (6, 'Pear Admin 展示', 1, '0,1', 50, '/pear', '_blank', 'MENU', 'ON', b'0', 'GET', 'menu:show', 'layui-icon-tabs', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (7, '获取源码', 1, '0,1', 60, 'https://gitee.com/gollyhu/youcan-solon', '_blank', 'MENU', 'ON', b'0', 'GET', 'menu:sourcecode', 'layui-icon-survey', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (8, '系统用户', 3, '0,1,3', 10, '/admin/system/user', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:system:user', 'layui-icon-user', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (9, '系统角色', 3, '0,1,3', 20, '/admin/system/role', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:system:role', 'layui-icon-username', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (10, '系统权限', 3, '0,1,3', 30, '/admin/system/permission', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:system:permission', 'layui-icon-menu-fill', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (11, '行政部门', 3, '0,1,3', 40, '/admin/system/dept', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:system:department', 'layui-icon-group', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (12, '公司岗位', 3, '0,1,3', 50, '/admin/system/position', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:system:position', 'layui-icon-friends', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (13, '数据字典', 3, '0,1,3', 60, '/admin/system/dict', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:system:dict', 'layui-icon-read', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (14, '系统参数', 3, '0,1,3', 70, '/admin/system/config', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:system:config', 'layui-icon-set-fill', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (15, '通知公告', 3, '0,1,3', 80, '/admin/system/notice', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:system:notice', 'layui-icon-notice', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (16, '在线用户', 4, '0,1,4', 10, '/admin/monitor/online', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:monitor:online', 'layui-icon-user', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (17, '定时任务', 4, '0,1,4', 20, '/admin/monitor/job', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:monitor:job', 'layui-icon-log', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (18, '服务监控', 4, '0,1,4', 30, '/admin/monitor/server', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:monitor:server', 'layui-icon-chart', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (19, '缓存监控', 4, '0,1,4', 40, '/admin/monitor/cache', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:monitor:cache', 'layui-icon-chart', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (20, '操作日志', 4, '0,1,4', 50, '/admin/monitor/operate', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:monitor:operate', 'layui-icon-chat', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (21, '登录日志', 4, '0,1,4', 60, '/admin/monitor/login', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:monitor:login', 'layui-icon-engine', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (22, '表单创建', 5, '0,1,5', 10, '#', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:tool:build', 'layui-icon-form', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (23, '代码生成', 5, '0,1,5', 20, '#', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:tool:generate', 'layui-icon-fonts-code', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (24, '系统接口', 5, '0,1,5', 30, '#', '_iframe', 'MENU', 'ON', b'0', 'GET', 'menu:tool:api', 'layui-icon-code-circle', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (101, '查询用户', 8, '0,1,3,8', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:user:query', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (102, '新增用户', 8, '0,1,3,8', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:user:add', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (103, '修改用户', 8, '0,1,3,8', 30, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:user:edit', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (104, '删除用户', 8, '0,1,3,8', 40, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:user:del', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (105, '重置密码', 8, '0,1,3,8', 50, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:user:resetPassword', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (106, '分配角色', 8, '0,1,3,8', 100, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:user:assignRole', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (107, '查询角色', 9, '0,1,3,9', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:role:query', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (108, '新增角色', 9, '0,1,3,9', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:role:add', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (109, '修改角色', 9, '0,1,3,9', 30, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:role:edit', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (110, '删除角色', 9, '0,1,3,9', 40, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:role:del', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (111, '分配权限', 9, '0,1,3,9', 50, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:role:assignPermission', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (112, '导出角色', 9, '0,1,3,9', 100, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:role:export', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (113, '分配数据权限', 9, '0,1,3,9', 60, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:role:assignDataScope', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (115, '查询权限', 10, '0,1,3,10', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:permission:query', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (116, '新增权限', 10, '0,1,3,10', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:permission:add', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (117, '修改权限', 10, '0,1,3,10', 30, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:permission:edit', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (118, '删除权限', 10, '0,1,3,10', 40, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:permission:del', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (119, '查询部门', 11, '0,1,3,11', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:department:query', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (120, '新增部门', 11, '0,1,3,11', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:department:add', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (121, '修改部门', 11, '0,1,3,11', 30, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:department:edit', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (122, '删除部门', 11, '0,1,3,11', 40, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:department:del', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (123, '查询岗位', 12, '0,1,3,12', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:position:query', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (124, '新增岗位', 12, '0,1,3,12', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:position:add', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (125, '修改岗位', 12, '0,1,3,12', 30, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:position:edit', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (126, '删除岗位', 12, '0,1,3,12', 40, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:position:del', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (127, '导出岗位', 12, '0,1,3,12', 100, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:position:export', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (128, '查询字典', 13, '0,1,3,13', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:dict:query', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (129, '新增字典', 13, '0,1,3,13', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:dict:add', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (130, '修改字典', 13, '0,1,3,13', 30, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:dict:edit', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (131, '删除字典', 13, '0,1,3,13', 40, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:dict:del', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (133, '导出字典', 13, '0,1,3,13', 100, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:dict:export', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (134, '查询参数', 14, '0,1,3,14', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:config:query', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (135, '新增参数', 14, '0,1,3,14', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:config:add', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (136, '修改参数', 14, '0,1,3,14', 30, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:config:edit', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (137, '删除参数', 14, '0,1,3,14', 40, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:config:del', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (138, '导出参数', 14, '0,1,3,14', 100, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:config:export', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (139, '查询通知公告', 15, '0,1,3,15', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:notice:query', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (140, '新增通知公告', 15, '0,1,3,15', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:notice:add', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (141, '修改通知公告', 15, '0,1,3,15', 30, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:notice:edit', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (142, '删除通知公告', 15, '0,1,3,15', 40, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'system:notice:del', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (143, '查询任务', 16, '0,1,4,16', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:online:query', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (144, '批量强退', 16, '0,1,4,16', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:online:batchForceLogout', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (145, '单条强退', 16, '0,1,4,16', 30, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:online:forceLogout', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (146, '查询任务', 17, '0,1,4,17', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:job:query', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (147, '新增任务', 17, '0,1,4,17', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:job:add', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (148, '修改任务', 17, '0,1,4,17', 30, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:job:edit', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (149, '删除任务', 17, '0,1,4,17', 40, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:job:del', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (150, '状态修改', 17, '0,1,4,17', 50, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:job:changeStatus', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (151, '查看任务', 17, '0,1,4,17', 60, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:job:detail', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (152, '导出任务', 17, '0,1,4,17', 100, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:job:export', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (153, '查询生成', 23, '0,1,5,23', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'tool:generate:list', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (154, '修改生成', 23, '0,1,5,23', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'tool:generate:edit', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (155, '删除生成', 23, '0,1,5,23', 30, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'tool:generate:remove', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (156, '代码预览', 23, '0,1,5,23', 40, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'tool:generate:preview', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (157, '代码生成', 23, '0,1,5,23', 50, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'tool:generate:code', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (158, 'Druid监控', 4, '0,1,4', 35, '/monitor/druid', '_iframe', 'MENU', 'ON', b'1', 'GET', 'menu:monitor:druid', 'layui-icon-chart', NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (159, '查询缓存', 19, '0,1,4,19', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:cache:query', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (160, '删除缓存', 19, '0,1,4,19', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:cache:del', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (161, '清空缓存', 19, '0,1,4,19', 30, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:cache:clear', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (162, '查询操作日志', 20, '0,1,4,20', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:operate:query', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (163, '删除操作日志', 20, '0,1,4,20', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:operate:del', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (164, '清空操作日志', 20, '0,1,4,20', 30, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:operate:clear', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (165, '查询登录日志', 21, '0,1,4,21', 10, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:login:query', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (166, '清除登录日志', 21, '0,1,4,21', 20, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:login:clear', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (167, '删除登录日志', 21, '0,1,4,21', 15, '#', NULL, 'FUNCTION', 'ON', b'0', NULL, 'monitor:login:del', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (168, '访问主页', 1, '0,1', 70, '/', '_blank', 'MENU', 'ON', b'0', 'GET', 'menu:website', 'layui-icon-website', '', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_permission` VALUES (169, '调度日志', 17, '0,1,4,17', 5, '#', '', 'NODE', 'ON', b'0', '', 'menu:monitor:jobLog', '', '', 1, '2024-05-30 11:13:07', NULL, NULL);
INSERT INTO `sys_permission` VALUES (170, '查询调度日志', 169, '0,1,4,17,169', 10, '#', '', 'FUNCTION', 'ON', b'0', '', 'monitor:jobLog:query', '', '', 1, '2024-05-30 11:14:30', NULL, NULL);
INSERT INTO `sys_permission` VALUES (171, '查看调度日志', 169, '0,1,4,17,169', 20, '#', '', 'FUNCTION', 'ON', b'0', '', 'monitor:jobLog:query', '', '', 1, '2024-05-30 11:16:05', NULL, NULL);
INSERT INTO `sys_permission` VALUES (172, '删除调度日志', 169, '0,1,4,17,169', 30, '#', '', 'FUNCTION', 'ON', b'0', '', 'monitor:jobLog:del', '', '', 1, '2024-05-30 11:16:54', NULL, NULL);
INSERT INTO `sys_permission` VALUES (173, '清空调度日志', 169, '0,1,4,17,169', 40, '#', '', 'FUNCTION', 'ON', b'0', '', 'monitor:jobLog:clear', '', '', 1, '2024-05-30 11:17:47', NULL, NULL);

-- ----------------------------
-- Table structure for sys_position
-- ----------------------------
DROP TABLE IF EXISTS `sys_position`;
CREATE TABLE `sys_position`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `sort` smallint(6) NOT NULL DEFAULT 0 COMMENT '显示排序',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'ON' COMMENT '状态（ON 正常, OFF 停用）',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）',
  `creator` int(11) NOT NULL DEFAULT 1 COMMENT '创建人',
  `created_datetime` datetime NOT NULL COMMENT '创建时间',
  `editor` int(11) NULL DEFAULT NULL COMMENT '编辑人',
  `edited_datetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `code`(`code`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `sort`(`sort`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_position
-- ----------------------------
INSERT INTO `sys_position` VALUES (1, 'ceo', '董事长', 10, 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_position` VALUES (2, 'se', '项目经理', 20, 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_position` VALUES (3, 'hr', '人力资源', 30, 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);
INSERT INTO `sys_position` VALUES (4, 'user', '普通员工', 40, 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限字符串',
  `sort` smallint(6) NOT NULL DEFAULT 0,
  `data_scope` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'ALL' COMMENT '数据范围（ALL：全部数据权限 CUSTOM：自定数据权限 DEPARTMENT：本部门数据权限 BELOW：本部门及以下数据权限）',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'ON' COMMENT '帐号状态（ON 正常，OFF 停用）',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）',
  `creator` int(11) NOT NULL DEFAULT 1 COMMENT '创建者',
  `created_datetime` datetime NOT NULL COMMENT '创建时间',
  `editor` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `edited_datetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `key`(`code`) USING BTREE,
  INDEX `status`(`status`) USING BTREE,
  INDEX `data_scope`(`data_scope`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理角色', 'administrators', 10, 'ALL', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '拥有全部权限的角色');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'commons', 20, 'CUSTOM', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '通常采用的角色');
INSERT INTO `sys_role` VALUES (3, '访客角色', 'visitors', 30, 'DEPARTMENT', 'ON', b'0', 1, '2023-08-08 00:00:00', NULL, NULL, '访客角色');

-- ----------------------------
-- Table structure for sys_role_dept_mapping
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept_mapping`;
CREATE TABLE `sys_role_dept_mapping`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID，关联sys_role表的ID',
  `dept_id` int(11) NOT NULL COMMENT '部门ID，关联sys_dept表的ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色与权限关联表' ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of sys_role_dept_mapping
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_permission_mapping
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission_mapping`;
CREATE TABLE `sys_role_permission_mapping`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID，关联sys_role表的ID',
  `permission_id` int(11) NOT NULL COMMENT '权限ID，关联sys_permission表的ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 79 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色与权限关联表' ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of sys_role_permission_mapping
-- ----------------------------
INSERT INTO `sys_role_permission_mapping` VALUES (1, 3, 1);
INSERT INTO `sys_role_permission_mapping` VALUES (2, 3, 2);
INSERT INTO `sys_role_permission_mapping` VALUES (3, 3, 3);
INSERT INTO `sys_role_permission_mapping` VALUES (4, 3, 8);
INSERT INTO `sys_role_permission_mapping` VALUES (5, 3, 101);
INSERT INTO `sys_role_permission_mapping` VALUES (6, 3, 9);
INSERT INTO `sys_role_permission_mapping` VALUES (7, 3, 107);
INSERT INTO `sys_role_permission_mapping` VALUES (8, 3, 10);
INSERT INTO `sys_role_permission_mapping` VALUES (9, 3, 115);
INSERT INTO `sys_role_permission_mapping` VALUES (10, 3, 11);
INSERT INTO `sys_role_permission_mapping` VALUES (11, 3, 119);
INSERT INTO `sys_role_permission_mapping` VALUES (12, 3, 12);
INSERT INTO `sys_role_permission_mapping` VALUES (13, 3, 123);
INSERT INTO `sys_role_permission_mapping` VALUES (14, 3, 13);
INSERT INTO `sys_role_permission_mapping` VALUES (15, 3, 128);
INSERT INTO `sys_role_permission_mapping` VALUES (16, 3, 14);
INSERT INTO `sys_role_permission_mapping` VALUES (17, 3, 134);
INSERT INTO `sys_role_permission_mapping` VALUES (18, 3, 15);
INSERT INTO `sys_role_permission_mapping` VALUES (19, 3, 139);
INSERT INTO `sys_role_permission_mapping` VALUES (20, 3, 4);
INSERT INTO `sys_role_permission_mapping` VALUES (21, 3, 16);
INSERT INTO `sys_role_permission_mapping` VALUES (22, 3, 143);
INSERT INTO `sys_role_permission_mapping` VALUES (23, 3, 17);
INSERT INTO `sys_role_permission_mapping` VALUES (24, 3, 146);
INSERT INTO `sys_role_permission_mapping` VALUES (25, 3, 18);
INSERT INTO `sys_role_permission_mapping` VALUES (26, 3, 19);
INSERT INTO `sys_role_permission_mapping` VALUES (27, 3, 159);
INSERT INTO `sys_role_permission_mapping` VALUES (28, 3, 20);
INSERT INTO `sys_role_permission_mapping` VALUES (29, 3, 162);
INSERT INTO `sys_role_permission_mapping` VALUES (30, 3, 21);
INSERT INTO `sys_role_permission_mapping` VALUES (31, 3, 165);
INSERT INTO `sys_role_permission_mapping` VALUES (32, 3, 6);
INSERT INTO `sys_role_permission_mapping` VALUES (33, 3, 7);
INSERT INTO `sys_role_permission_mapping` VALUES (34, 2, 1);
INSERT INTO `sys_role_permission_mapping` VALUES (35, 2, 2);
INSERT INTO `sys_role_permission_mapping` VALUES (36, 2, 3);
INSERT INTO `sys_role_permission_mapping` VALUES (37, 2, 8);
INSERT INTO `sys_role_permission_mapping` VALUES (38, 2, 101);
INSERT INTO `sys_role_permission_mapping` VALUES (39, 2, 102);
INSERT INTO `sys_role_permission_mapping` VALUES (40, 2, 103);
INSERT INTO `sys_role_permission_mapping` VALUES (41, 2, 104);
INSERT INTO `sys_role_permission_mapping` VALUES (42, 2, 105);
INSERT INTO `sys_role_permission_mapping` VALUES (43, 2, 106);
INSERT INTO `sys_role_permission_mapping` VALUES (44, 2, 9);
INSERT INTO `sys_role_permission_mapping` VALUES (45, 2, 107);
INSERT INTO `sys_role_permission_mapping` VALUES (46, 2, 108);
INSERT INTO `sys_role_permission_mapping` VALUES (47, 2, 109);
INSERT INTO `sys_role_permission_mapping` VALUES (48, 2, 110);
INSERT INTO `sys_role_permission_mapping` VALUES (49, 2, 111);
INSERT INTO `sys_role_permission_mapping` VALUES (50, 2, 112);
INSERT INTO `sys_role_permission_mapping` VALUES (51, 2, 113);
INSERT INTO `sys_role_permission_mapping` VALUES (52, 2, 10);
INSERT INTO `sys_role_permission_mapping` VALUES (53, 2, 115);
INSERT INTO `sys_role_permission_mapping` VALUES (54, 2, 116);
INSERT INTO `sys_role_permission_mapping` VALUES (55, 2, 11);
INSERT INTO `sys_role_permission_mapping` VALUES (56, 2, 119);
INSERT INTO `sys_role_permission_mapping` VALUES (57, 2, 120);
INSERT INTO `sys_role_permission_mapping` VALUES (58, 2, 121);
INSERT INTO `sys_role_permission_mapping` VALUES (59, 2, 122);
INSERT INTO `sys_role_permission_mapping` VALUES (60, 2, 12);
INSERT INTO `sys_role_permission_mapping` VALUES (61, 2, 123);
INSERT INTO `sys_role_permission_mapping` VALUES (62, 2, 124);
INSERT INTO `sys_role_permission_mapping` VALUES (63, 2, 125);
INSERT INTO `sys_role_permission_mapping` VALUES (64, 2, 126);
INSERT INTO `sys_role_permission_mapping` VALUES (65, 2, 127);
INSERT INTO `sys_role_permission_mapping` VALUES (66, 2, 13);
INSERT INTO `sys_role_permission_mapping` VALUES (67, 2, 128);
INSERT INTO `sys_role_permission_mapping` VALUES (68, 2, 14);
INSERT INTO `sys_role_permission_mapping` VALUES (69, 2, 134);
INSERT INTO `sys_role_permission_mapping` VALUES (70, 2, 135);
INSERT INTO `sys_role_permission_mapping` VALUES (71, 2, 136);
INSERT INTO `sys_role_permission_mapping` VALUES (72, 2, 15);
INSERT INTO `sys_role_permission_mapping` VALUES (73, 2, 139);
INSERT INTO `sys_role_permission_mapping` VALUES (74, 2, 140);
INSERT INTO `sys_role_permission_mapping` VALUES (75, 2, 141);
INSERT INTO `sys_role_permission_mapping` VALUES (76, 2, 142);
INSERT INTO `sys_role_permission_mapping` VALUES (77, 3, 168);
INSERT INTO `sys_role_permission_mapping` VALUES (78, 2, 168);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dept_id` int(11) NULL DEFAULT NULL COMMENT '部门ID',
  `account` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号',
  `nickname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'REGISTE' COMMENT '用户类型（SYSTEM 系统用户，REGISTE注册用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户邮箱',
  `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `gender` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'U' COMMENT '用户性别（M 男，F 女，U 未知）',
  `avatar` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像路径',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '加密盐',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'ON' COMMENT '帐号状态（ON 正常，OFF 停用）',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后登录IP',
  `login_datetime` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `creator` int(11) NOT NULL DEFAULT 1 COMMENT '创建者',
  `created_datetime` datetime NOT NULL COMMENT '创建时间',
  `editor` int(11) NULL DEFAULT NULL COMMENT '更新者',
  `edited_datetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `dept_id`(`dept_id`) USING BTREE,
  INDEX `account`(`account`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  INDEX `status`(`status`) USING BTREE,
  INDEX `created_datetime`(`created_datetime`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 1, 'admin', '超级管理员', 'SYSTEM', '3722711@qq.com', '13700000000', 'U', NULL, '4a966fbb953a7c14012f7a9935c212d7c9ac7076f05d15fd3229a4a51879ee81', 'f9c30f17fc1f4f8cb7ea', 'ON', b'0', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL, '拥有所有权限的管理员账号，不允许删除');
INSERT INTO `sys_user` VALUES (2, 1, 'youcan', 'Youcan', 'SYSTEM', '3722711@qq.com', '13700000000', 'U', NULL, '6c560c20e99dfd05dea1141ce65e359da32ddef1c8508d5eaed69303f90d0898', 'kne4txw5i1xwh3wc717q', 'ON', b'0', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL, '平台标准账号');
INSERT INTO `sys_user` VALUES (3, 1, 'guest', '访客', 'SYSTEM', '3722711@qq.com', '13700000000', 'U', NULL, 'cfdbb799c37b41be3937c6ab2211bd70d28b57f39970ddd5d568fc6f2c877974', 'norrsaz9fqd85q9gfu7v', 'ON', b'0', NULL, NULL, 1, '2023-08-08 00:00:00', NULL, NULL, '访客账号，只能查看而不能操作');

-- ----------------------------
-- Table structure for sys_user_login
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_login`;
CREATE TABLE `sys_user_login`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `account` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号',
  `nickname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录IP',
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '任务状态：SUCCESS 成功，FAIL 失败',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_datetime` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `account`(`account`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户登录记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_login
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户会话id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户ID',
  `nickname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户昵称',
  `dept_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `device` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录设备',
  `ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'ON：在线，OFF：离线',
  `start_datetime` datetime NULL DEFAULT NULL COMMENT 'session创建时间',
  `activity_datetime` datetime NULL DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_second` int(11) NULL DEFAULT 0 COMMENT '超时时间(秒)',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '在线用户记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_position_mapping
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_position_mapping`;
CREATE TABLE `sys_user_position_mapping`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID，关联sys_user表的ID',
  `position_id` int(11) NOT NULL COMMENT '岗位ID，关联sys_position表的ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of sys_user_position_mapping
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_role_mapping
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role_mapping`;
CREATE TABLE `sys_user_role_mapping`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID，关联sys_user表的ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID，关联sys_role表的ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与角色关联表' ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of sys_user_role_mapping
-- ----------------------------
INSERT INTO `sys_user_role_mapping` VALUES (1, 1, 1);
INSERT INTO `sys_user_role_mapping` VALUES (2, 2, 2);
INSERT INTO `sys_user_role_mapping` VALUES (3, 3, 3);

SET FOREIGN_KEY_CHECKS = 1;
