-- del字段更名为is_del，保持风格一致
ALTER TABLE `sys_config`
    CHANGE COLUMN `del` `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）' AFTER `is_sys`;
ALTER TABLE `sys_dept`
    CHANGE COLUMN `del` `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）' AFTER `status`;
ALTER TABLE `sys_dict`
    CHANGE COLUMN `del` `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）' AFTER `status`;
ALTER TABLE `sys_dict_item`
    CHANGE COLUMN `del` `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）' AFTER `status`;
ALTER TABLE `sys_job`
    CHANGE COLUMN `concurrent` `is_concurrent` bit(1) NULL DEFAULT b'0' COMMENT '是否并发执行' AFTER `policy`;
ALTER TABLE `sys_notice`
    CHANGE COLUMN `del` `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）' AFTER `status`;
ALTER TABLE `sys_permission`
    CHANGE COLUMN `del` `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）' AFTER `status`;
ALTER TABLE `sys_position`
    CHANGE COLUMN `del` `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）' AFTER `status`;
ALTER TABLE `sys_role`
    CHANGE COLUMN `del` `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）' AFTER `status`;
ALTER TABLE `sys_user`
    CHANGE COLUMN `del` `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '删除标志（true 是，false 否）' AFTER `status`;

ALTER TABLE `sys_job_log`
    MODIFY COLUMN `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务状态：SUCCESS 成功，FAIL 失败' AFTER `message`;
ALTER TABLE `sys_operate_log`
    MODIFY COLUMN `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作状态：SUCCESS 成功，FAIL 失败' AFTER `result`;
ALTER TABLE `sys_user_login`
    MODIFY COLUMN `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '任务状态：SUCCESS 成功，FAIL 失败' AFTER `os`;
ALTER TABLE `sys_notice`
    MODIFY COLUMN `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'NOTICE' COMMENT '公告类型：NOTICE 通知, ANNOUNCEMENT 公告' AFTER `title`;

-- 更新Pear Admin Config的配置值
UPDATE `sys_config` SET `value` = '{\r\n  \"logo\": {\r\n    \"title\": \"Youcan-Solon\",\r\n    \"image\": \"#CTX()/pear/admin/images/logo.png\"\r\n  },\r\n  \"colors\": [\r\n    {\r\n      \"id\": \"1\",\r\n      \"color\": \"#2d8cf0\",\r\n      \"second\": \"#ecf5ff\"\r\n    },\r\n    {\r\n      \"id\": \"2\",\r\n      \"color\": \"#36b368\",\r\n      \"second\": \"#f0f9eb\"\r\n    },\r\n    {\r\n      \"id\": \"3\",\r\n      \"color\": \"#f6ad55\",\r\n      \"second\": \"#fdf6ec\"\r\n    },\r\n    {\r\n      \"id\": \"4\",\r\n      \"color\": \"#f56c6c\",\r\n      \"second\": \"#fef0f0\"\r\n    },\r\n    {\r\n      \"id\": \"5\",\r\n      \"color\": \"#3963bc\",\r\n      \"second\": \"#ecf5ff\"\r\n    }\r\n  ],\r\n  \"header\": {\r\n    \"message\": false\r\n  },\r\n  \"menu\": {\r\n    \"data\": \"#CTX()/admin/menu?ids=1\",\r\n    \"method\": \"GET\",\r\n    \"accordion\": true,\r\n    \"collaspe\": false,\r\n    \"control\": false,\r\n    \"controlWidth\": 500,\r\n    \"select\": \"0\",\r\n    \"async\": true\r\n  },\r\n  \"other\": {\r\n    \"keepLoad\": 100,\r\n    \"autoHead\": false,\r\n    \"footer\": true\r\n  },\r\n  \"tab\": {\r\n    \"enable\": true,\r\n    \"keepState\": true,\r\n    \"session\": false,\r\n    \"preload\": false,\r\n    \"max\": 10,\r\n    \"index\": {\r\n      \"id\": \"2\",\r\n      \"href\": \"#CTX()/admin/home\",\r\n      \"title\": \"首页\"\r\n    }\r\n  },\r\n  \"theme\": {\r\n    \"defaultColor\": \"2\",\r\n    \"defaultMenu\": \"dark-theme\",\r\n    \"defaultHeader\": \"light-theme\",\r\n    \"allowCustom\": true,\r\n    \"banner\": false\r\n  }\r\n}' WHERE `id` = 14;

-- FAILED统一成FAIL，SUCCEED和SUCCESSED统一成SUCCESS
UPDATE `sys_dict_item` SET `value` = 'SUCCESS' WHERE `id` = 54;
UPDATE `sys_dict_item` SET `value` = 'SUCCESS' WHERE `id` = 56;
UPDATE `sys_dict_item` SET `value` = 'SUCCESS' WHERE `id` = 62;
UPDATE `sys_dict_item` SET `value` = 'FAIL' WHERE `id` = 55;
UPDATE `sys_dict_item` SET `value` = 'FAIL' WHERE `id` = 57;
UPDATE `sys_dict_item` SET `value` = 'FAIL' WHERE `id` = 63;

-- 插入ICP备案号配置
INSERT INTO `sys_config` (`id`, `name`, `code`, `value`, `type`, `is_sys`, `is_del`, `creator`, `created_datetime`, `editor`, `edited_datetime`, `remark`) VALUES (16, 'ICP备案号', 'icp', '粤ICP备00000000号', 'SITE', b'0', b'0', 1, '2024-04-03 14:47:23', NULL, NULL, 'ICP的备案号，网站可见位置显示此值用于备案核验');

