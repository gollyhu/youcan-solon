# 前端开发

​	前端框架采用 Pear Admin Layui，它基于流行的自适应框架，所以在看此文档之前请先查看相关文档：

​	Pear Admin Layui：http://www.pearadmin.com/doc/index.html

​	Layui 2.8.x：https://layui.dev/docs/2.8

​	JS 库参考了 Ruoyi，但复制的代码不多。youcan-solon 不会提供像 Ruoyi 那边完善的 JS 库，而是围绕以查询表格操作为主的库。



## 1. 表格类型

### 1.1 数据表格

![数据表格](./images/web-front/数据表格页面.png)

​	90% 以上 CRUD 页面基本都为单表单操作，youcan-solon 的 JS 库聚焦于单表操作。如上图，将页面中的各个部件视为一个整体进行封装。

​	注意，图中是数据表格示例完全体，最小的体量是数据表格，如下图。

![数据表格-简](./images/web-front/数据表格页面 - 最简.png)

### 1.2 树形表格

![树开形表格](./images/web-front/树形表格页面.png)

​	树形表格除开数据以树的方式承现之外，其它与数据表格没有区别，因为 Layui 的 TreeTable 就是继承自 Table。请参考：https://layui.dev/docs/2/treeTable/

​	表格类型定义在 youcan_ui.js 中：

> ```javascript
> const TABLE_TYPE = {
>     dataTable: 0,               //二维数据表格
>     treeTable: 1,               //树形表格视图
> }
> ```



## 2. 设计思路

​	youcan-solon 将查询面板和数据表格视为一个整体，JS 库围绕这个单一表格的最常用功能（增|删|改|查|详细）做了封装。

- 随时可采用自己的逻辑来替换默认封装逻辑；
- 采用约定方式开发，极大减少代码；
- 超出封装库的业务，开发人员自行扩展来支持；

​	其它形式的页面基本都为数据表格的组合，多个表格中间的连动关系可以通过事件等方式实现，极大缩减 JS 库的规模。

​	比如：主从表格查询，主表带查询面板，点击主表的某数据行时，刷新从表的数据。这种情况这可以在主表的事件中将被点击数据行当成参数用于从表刷新，多个表格连动。可参考“缓存监控”页面，此页面为主从表联动。



## 3. JS 库

​	位置：/youcan-solon-admin/src/main/resources/static/admin/js/youcan_ui.js

| 名称         | 组件        | 说明                 |
| ------------ | ----------- | -------------------- |
| 数据表格组件 | $.table     | 数据表格组件操作封装 |
| 查询面板组件 | $.queryForm | 查询面板组件操作封装 |
| 弹出层组件   | $.modal     | 弹出层组件操作封装   |
| 树组件       | $.tree      | 树组件操作封装       |
| 通用组件     | $.base      | 通用方法封装         |
| 操作组件     | $.operate   | 后台操作组件封装     |

​	

### 3.1 常量定义

> ```javascript
> /** 弹窗状态码 */
> const MODAL_STATUS = {
>  SUCCESS: "success",         //成功
>  FAIL: "error",              //失败
>  WARNING: "warning",         //警告
>  CANCEL: "cancel",           //取消
> };
> 
> /** 消息状态码 */
> const RESULT_CODE = {
>  SUCCESS: 200,               //成功
>  CANCEL: 204,                //客户端取消操作
>  FAIL: 500,                  //失败
>  BAD_REQUEST: 400,           //请求异常
>  NO_AUTH: 401,               //没有权限
>  REQUEST_FORBIDDEN: 403,     //请求被拒绝
>  WARNING: 409,               //业务异常
> };
> 
> /** 表格类型 */
> const TABLE_TYPE = {
>  dataTable: 0,               //二维数据表格
>  treeTable: 1,               //树形表格视图
> }
> ```



### 3.2 数据表格组件方法

| 方法                        | 参数 | 说明 |
| --------------------------- | ---- | ---- |
| $.table.buildRowToolbar()   |      |      |
| $.table.buildTableToolbar() |      |      |
| $.table.clearSelectedRows() |      |      |
| $.table.getPageData()       |      |      |
| $.table.init()              |      |      |
| $.table.search()            |      |      |
| $.table.refresh()           |      |      |



### 3.3 查询面板组件方法

| 方法                 | 参数 | 说明                                                         |
| -------------------- | ---- | ------------------------------------------------------------ |
| $.queryForm.getData  |      | 获取查询面板的值                                             |
| $.queryForm.setData  |      | 设置查询面板的值                                             |
| $.queryForm.reset()  |      | 调用查询面板上重置按钮事件                                   |
| $.queryForm.toggle() |      | 显示/隐藏查询面板，cls为面板DIV标签上定义的class名称，默认名称为"query-panel" |



### 3.4 弹出层组件方法

| 方法                   | 参数 | 说明 |
| ---------------------- | ---- | ---- |
| $.modal.alert()        |      |      |
| $.modal.alertError()   |      |      |
| $.modal.alertSuccess() |      |      |
| $.modal.alertWarning() |      |      |
| $.modal.close()        |      |      |
| $.modal.closeAll()     |      |      |
| $.modal.loading()      |      |      |
| $.modal.closeLoading() |      |      |
| $.modal.confirm()      |      |      |
| $.modal.open()         |      |      |
| $.modal.popupEditor()  |      |      |
| $.modal.msg()          |      |      |
| $.modal.msgFail()      |      |      |
| $.modal.msgSuccess()   |      |      |
| $.modal.msgWarning()   |      |      |
| $.modal.height()       |      |      |
| $.modal.width()        |      |      |



### 3.5 树组件方法

| 方法                   | 参数 | 说明 |
| ---------------------- | ---- | ---- |
| $.tree.getCheckedIds() |      |      |
| $.tree.selectChecked() |      |      |



### 3.6 通用组件方法

| 方法                | 参数 | 说明 |
| ------------------- | ---- | ---- |
| $.base.isEmpty()    |      |      |
| $.base.isFunction() |      |      |
| $.base.isMobile()   |      |      |
| $.base.isNotEmpty() |      |      |
| $.base.isNotNull    |      |      |
| $.base.isNull       |      |      |
| $.base.isString()   |      |      |
| $.base.trim()       |      |      |



### 3.7 后台操作组件方法

| 方法                      | 参数 | 说明 |
| ------------------------- | ---- | ---- |
| $.operate.query()         |      |      |
| $.operate.save()          |      |      |
| $.operate.successCallback |      |      |





## 4. 页面约定

​	Layui 的部分控件只定义 lay-filter 属性也可使用，但在 youcan-solon 中，请一定要定义 ID 属性使用，并且属性值与 lay-filter 保持一致。

> ```html
> <!-- 表单 -->
> <form id="main-form" class="layui-form layui-row layui-col-space16" lay-filter="main-form" onsubmit="return false;">
> 
> <!-- 提交按钮 -->
> <button id="submit-button" type="submit" class="layui-btn layui-btn-sm" lay-submit lay-filter="submit-button">
> ```



### 4.1 表格页面

​	

### 4.2 编辑页面

​	新增、编辑页面用的是同一个：![编辑页面](./images/web-front/编辑页面（模态）.png)

​	

​	弹出的编辑页面默认为模态对话框，如下方式定义页面的 url：

> ```javascript
> let actions = {
> 	addUrl: module.path + 'add',                        //新增记录 URL
> 	editUrl: module.path + 'edit',                      //修改记录 URL
> }
> ```



### 4.3 详情页面

![详情页面](./images/web-front/详情页面（模态）.png)



## 5. 页面初始化



### 5.1 模块参数

​	首先，为表格定义如下参数（很多参数可以不必定义，采用 JS 库的默认值即可）：

> ```javascript
> //定义表格模块基本参数
> let module = {
>     type: TABLE_TYPE.dataTable,             //表格类型：二维表格
>     idField: 'id',                          //表格中 id 的字段名（与数据库定义的要一致，如userId）
>     name: '系统参数',                        //表格模块的业务名称
>     path: '#CTX()/admin/system/config/',    //表格模块的操作路径，以 '/' 结尾
> };
> 
> //定义查询表单基本参数
> let queryForm = {
>     id: 'main-form',                        //查询面板中 form 表单的 id 属性
>     searchButtonId: 'main-form-search',     //查询面板中搜索按钮的 id 属性
>     resetButtonId: 'main-form-reset',       //查询面板中重置按钮的 id 属性
> };
> 
> //定义编辑对话框基本参数
> let editDialog = {
>     submitFormId: 'edit-form',              //编辑页面（模态）中 form 表单的 ID 属性
>     submitButtonId: 'submit-button',        //编辑页面（模态）中提交按钮的 ID 属性
>     width: '720px',                         //对话框宽度
>     height: '95%',                          //对话框高度
> };
> 
> //定义表格操作基本参数
> let actions = {
>     //#authPermissions('system:config:add')
>     addFlag: true,                                      //新增记录权限标志
>     addUrl: module.path + 'add',                        //新增记录 URL
>     addCallback: (index, layero, that, result) => {
>         console.log("新增页面回调:" + JSON.stringify(result));
>     },
>     //#end
>     //事件处理
>     handleEvent: (options, event, obj) => {
>         //#DEBUG()
>         console.log("事件处理器接收到事件：event=" + event + "，obj=" + obj);
>         //#end
>     },
> };
> ```

​	代码中的 #authPermissions 、 #DEBUG 、 #end 请参考 "6.1 模板指令"。



### 5.2 表格参数

​	为数据表格定义 options参数，此参数即 Layui table 的 options，只是将上一节的4个参数代入到 options 中（正面代码的最后4行）做为子参数：

> ```javascript
> //定义数据表格的 options，参考：https://layui.dev/docs/2/table/#options
> let options = {
>     id: 'main-table',                      //设定实例唯一索引
>     elem: '#main-table',                   //绑定原始 table 元素
>     title: module.name,                    //定义 table 的大标题（在文件导出等地方会用到）
>     url: module.path + 'query',            //发送异步请求的 URL
>     initSort: {'field': 'createdDatetime', 'type': 'desc'},         //初始排序状态
>     toolbar: $.table.buildTableToolbar(actions, toolbarButtons),    //开启表格头部工具栏
>     pagebar: '#main-pagebar',              //开启分页区域的自定义模板
>     cols: [[                               //表头属性集
>         {type: 'checkbox', align: 'center', width: 60},
>         {field: 'name', align: 'left', minWidth: 130, width: '10%', sort: true, title: '名称'},
>         。。。。。。
>     ]],
>     module: module,             //表格模块基本参数
>     queryForm: queryForm,       //查询表单基本参数
>     editDialog: editDialog,     //编辑对话框基本参数
>     actions: actions,           //表格操作基本参数
> };
> ```



### 5.3 初始化表格

​	options 做为 init() 方法的参数，即可初始化表格。初始化后，系统将为options添加运行时内容，所以最好将 init() 的返回值赋值给 options。

> ```javascript
> options = $.init(options);
> ```



## 6. 参考

### 6.1 模板指令

#### 6.1.1 字典指令

##### 6.1.1.1 字典子项列表

#DICTS()

<br>

##### 6.1.1.2 字典类型列表

#DICTYPES()



#### 6.1.2 参数指令

##### 6.1.2.1 站点参数

#SITE()



##### 6.1.2.2 公司参数

#COMPANY()



##### 6.1.2.3 应用参数

#APP()



##### 6.1.2.4 设置参数

#CONFIG()



##### 6.1.2.4 参数表

#CONFIGS()



#### 6.1.3 系统指令

##### 6.1.3.1 上下文路径指令

#CTX()



##### 6.1.3.2 调试模式指令

#DEBUG()



##### 6.1.3.3 版本指令

#VERSION()





