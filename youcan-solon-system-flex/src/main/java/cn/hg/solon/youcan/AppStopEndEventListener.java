package cn.hg.solon.youcan;

import org.noear.solon.annotation.Component;
import org.noear.solon.core.event.AppPrestopEndEvent;
import org.noear.solon.core.event.EventListener;

/**
 * @author 胡高
 */
@Component
public class AppStopEndEventListener implements EventListener<AppPrestopEndEvent> {

    @Override
    public void onEvent(AppPrestopEndEvent event) throws Throwable {
    }

}
