package cn.hg.solon.youcan.system.provider;

import cn.hg.solon.youcan.flex.util.QueryWrapperUtil;
import cn.hg.solon.youcan.system.entity.OperateLog;
import cn.hg.solon.youcan.system.entity.SysOperateLog;
import cn.hg.solon.youcan.system.mapper.SysOperateLogMapper;
import cn.hg.solon.youcan.system.service.OperateLogService;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static cn.hg.solon.youcan.system.entity.table.SysOperateLogTableDef.SYS_OPERATE_LOG;

/**
 * @author 胡高
 */
@Component
public class SysOperateLogProvider extends ServiceImpl<SysOperateLogMapper, SysOperateLog>
        implements OperateLogService {

    @Tran
    @Override
    public boolean clear() {
        return this.remove(SYS_OPERATE_LOG.ID.ne(0));
    }

    @Tran
    @Override
    public boolean delete(List<Integer> idList) {
        return this.getMapper().deleteBatchByIds(idList) > 0;
    }

    @Override
    public SysOperateLog get(Integer id) {
        return this.getMapper().selectOneById(id);
    }

    @Tran
    @Override
    public boolean insert(OperateLog bean) {
        SysOperateLog cloneBean = BeanUtil.copyProperties(bean, SysOperateLog.class);

        return this.getMapper().insert(cloneBean) > 0;
    }

    @Override
    public PageResult<SysOperateLog> pageBy(org.dromara.hutool.db.Page page, Map<String, Object> paraMap) {
        String type = (String) paraMap.get("type");
        String method = (String) paraMap.get("method");
        String status = (String) paraMap.get("status");
        String word = (String) paraMap.get("word");
        Date startDate = (Date) paraMap.get("startDate");
        Date endDate = (Date) paraMap.get("endDate");

        QueryWrapper query = QueryWrapper.create()
                .where(SYS_OPERATE_LOG.TYPE.eq(type).when(StrValidator.isNotBlank(type))
                        .and(SYS_OPERATE_LOG.REQUEST_METHOD.eq(method).when(StrValidator.isNotBlank(method)))
                        .and(SYS_OPERATE_LOG.STATUS.eq(status).when(StrValidator.isNotBlank(status)))
                        .and(SYS_OPERATE_LOG.ACTION_DATETIME.ge(startDate).when(ObjUtil.isNotNull(startDate)))
                        .and(SYS_OPERATE_LOG.ACTION_DATETIME.le(endDate).when(ObjUtil.isNotNull(endDate)))
                        .and(SYS_OPERATE_LOG.TITLE.like(word).when(StrValidator.isNotBlank(word))
                                .or(SYS_OPERATE_LOG.USER_NAME.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_OPERATE_LOG.DEPT_NAME.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_OPERATE_LOG.URL.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_OPERATE_LOG.IP.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_OPERATE_LOG.LOCATION.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_OPERATE_LOG.PARAM.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_OPERATE_LOG.RESULT.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_OPERATE_LOG.MESSAGE.like(word).when(StrValidator.isNotBlank(word)))
                        )
                );

        Page<SysOperateLog> pageList = this.getMapper().paginate(Page.of(page.getPageNumber(), page.getPageSize()),
                QueryWrapperUtil.applyOrderBy(query, page));

        PageResult<SysOperateLog> result = new PageResult<>();
        result.addAll(pageList.getRecords());
        result.setTotal(ConvertUtil.toInt(pageList.getTotalRow()));

        return result;
    }
}
