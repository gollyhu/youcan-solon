package cn.hg.solon.youcan.system.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table(value = "sys_user_role_mapping")
public class SysUserRoleMapping extends UserRoleMapping {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3721180620043085871L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Auto)
    private Integer id;

    /**
     * 用户ID，关联sys_user表的ID
     */
    private Integer userId;

    /**
     * 角色ID，关联sys_role表的ID
     */
    private Integer roleId;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public Integer getRoleId() {
        return this.roleId;
    }

    @Override
    public Integer getUserId() {
        return this.userId;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Override
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
