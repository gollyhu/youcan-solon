package cn.hg.solon.youcan;

import org.dromara.hutool.core.text.CharSequenceUtil;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Component;
import org.noear.solon.core.event.AppLoadEndEvent;
import org.noear.solon.core.event.EventListener;

import com.mybatisflex.core.audit.AuditManager;
import com.mybatisflex.core.audit.ConsoleMessageCollector;
import com.mybatisflex.core.audit.MessageCollector;

/**
 * @author 胡高
 */
@Component
public class AppLoadEndEventListener implements EventListener<AppLoadEndEvent> {

    @Override
    public void onEvent(AppLoadEndEvent event) throws Throwable {
        /*
         * 打开Flex审计日志，输出到控制台
         */
        if (CharSequenceUtil.equals(Solon.cfg().get("solon.env"), "dev")) {
            MessageCollector collector = new ConsoleMessageCollector();
            AuditManager.setMessageCollector(collector);
            AuditManager.setAuditEnable(true);
        }
    }
}