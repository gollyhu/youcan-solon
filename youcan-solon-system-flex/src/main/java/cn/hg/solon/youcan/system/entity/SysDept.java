package cn.hg.solon.youcan.system.entity;

import java.util.Date;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

import cn.hg.solon.youcan.flex.listener.EntityInsertListener;
import cn.hg.solon.youcan.flex.listener.EntityUpdateListener;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table(value = "sys_dept", onInsert = EntityInsertListener.class, onUpdate = EntityUpdateListener.class)
public class SysDept extends Dept {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3864679635515357548L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Auto)
    private Integer id;

    /**
     * 父部门ID
     */
    private Integer parentId;

    /**
     * 祖级ID列表
     */
    private String ancestors;

    /**
     * 部门名
     */
    private String name;

    /**
     * 显示排序
     */
    private Integer sort;

    /**
     * 负责人
     */
    private String leader;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 状态（ON 正常, OFF 停用）
     */
    private String status;

    /**
     * 删除标志（true 是，false 否）
     */
    @Column(value = "is_del", isLogicDelete = true)
    private Boolean isDel;

    /**
     * 创建人
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 编辑人
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    @Override
    public String getAncestors() {
        return this.ancestors;
    }

    @Override
    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    @Override
    public Integer getCreator() {
        return this.creator;
    }

    @Override
    public Boolean getIsDel() {
        return this.isDel;
    }

    @Override
    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    @Override
    public Integer getEditor() {
        return this.editor;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getLeader() {
        return this.leader;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Integer getParentId() {
        return this.parentId;
    }

    @Override
    public String getPhone() {
        return this.phone;
    }

    @Override
    public Integer getSort() {
        return this.sort;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setAncestors(String ancestors) {
        this.ancestors = ancestors;
    }

    @Override
    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    @Override
    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    @Override
    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    @Override
    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    @Override
    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setLeader(String leader) {
        this.leader = leader;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Override
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

}
