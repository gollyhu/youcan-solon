package cn.hg.solon.youcan.system.provider;

import cn.hg.solon.youcan.common.exception.ServiceException;
import cn.hg.solon.youcan.flex.util.QueryWrapperUtil;
import cn.hg.solon.youcan.system.entity.DictItem;
import cn.hg.solon.youcan.system.entity.SysDictItem;
import cn.hg.solon.youcan.system.mapper.SysDictItemMapper;
import cn.hg.solon.youcan.system.service.DictItemService;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.util.UpdateEntity;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.List;
import java.util.Map;

import static cn.hg.solon.youcan.system.entity.table.SysDictItemTableDef.SYS_DICT_ITEM;

/**
 * @author 胡高
 */
@Component
public class SysDictItemProvider extends ServiceImpl<SysDictItemMapper, SysDictItem> implements DictItemService {

    @Override
    public boolean checkUnique(DictItem bean) {
        // 查找已存在记录
        QueryWrapper query = QueryWrapper.create()
                .where(SYS_DICT_ITEM.TYPE.eq(bean.getType())
                        .and(SYS_DICT_ITEM.VALUE.eq(bean.getValue()))
                        .and(SYS_DICT_ITEM.ID.ne(bean.getId()))
                );

        return ObjUtil.isNull(this.getOne(query));
    }

    @Tran
    @Override
    public boolean delete(List<Integer> idList) {
        return this.getMapper().deleteBatchByIds(idList) > 0;
    }

    @Override
    public SysDictItem get(Integer id) {
        return this.getMapper().selectOneById(id);
    }

    @Override
    public SysDictItem getByTypeAndValue(String type, String value) {
        QueryWrapper query = QueryWrapper.create().where(SYS_DICT_ITEM.TYPE.eq(type));
        return this.getOne(query);
    }

    @Tran
    @Override
    public boolean insert(DictItem bean) {
        if (!this.checkUnique(bean)) {
            throw new ServiceException("字典键值已经存在，请更换其它值！");
        }

        SysDictItem cloneBean = BeanUtil.copyProperties(bean, SysDictItem.class);

        return this.getMapper().insert(cloneBean) > 0;
    }

    @Override
    public List<SysDictItem> listByType(String type, String status) {
        return this.getMapper().selectListByCondition(SYS_DICT_ITEM.TYPE.eq(type).and(SYS_DICT_ITEM.STATUS.eq(status)));
    }

    @Override
    public PageResult<SysDictItem> pageBy(org.dromara.hutool.db.Page page, Map<String, Object> paraMap) {
        String type = (String) paraMap.get("type");
        String status = (String) paraMap.get("status");
        String word = (String) paraMap.get("word");

        QueryWrapper query = QueryWrapper.create()
                .where(SYS_DICT_ITEM.TYPE.eq(type).when(StrValidator.isNotBlank(type))
                        .and(SYS_DICT_ITEM.STATUS.eq(status).when(StrValidator.isNotBlank(status)))
                        .and(SYS_DICT_ITEM.VALUE.like(word).when(StrValidator.isNotBlank(word))
                                .or(SYS_DICT_ITEM.LABEL.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_DICT_ITEM.CSS_CLASS.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_DICT_ITEM.LIST_CLASS.like(word).when(StrValidator.isNotBlank(word)))
                        )
                );

        Page<SysDictItem> pageList = this.getMapper().paginate(Page.of(page.getPageNumber(), page.getPageSize()),
                QueryWrapperUtil.applyOrderBy(query, page));

        PageResult<SysDictItem> result = new PageResult<>();
        result.addAll(pageList.getRecords());
        result.setTotal(ConvertUtil.toInt(pageList.getTotalRow()));

        return result;
    }

    @Tran
    @Override
    public boolean update(DictItem bean) {
        if (!this.checkUnique(bean)) {
            throw new ServiceException("字典键值已经存在，请更换其它值！");
        }

        SysDictItem cloneBean = BeanUtil.copyProperties(bean, SysDictItem.class);

        return this.getMapper().update(cloneBean) > 0;
    }

    @Tran
    @Override
    public boolean updateTypeName(String oldType, String newType) {
        DictItem bean = UpdateEntity.of(DictItem.class);
        bean.setType(newType);

        SysDictItem cloneBean = BeanUtil.copyProperties(bean, SysDictItem.class);

        return this.getMapper().updateByCondition(cloneBean, SYS_DICT_ITEM.TYPE.eq(oldType)) > 0;
    }

}
