package cn.hg.solon.youcan.system.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table(value = "sys_role_permission_mapping")
public class SysRolePermissionMapping extends RolePermissionMapping {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8028777195051886220L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Auto)
    private Integer id;

    /**
     * 角色ID，关联sys_role表的ID
     */
    private Integer roleId;

    /**
     * 权限ID，关联sys_permission表的ID
     */
    private Integer permissionId;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public Integer getPermissionId() {
        return this.permissionId;
    }

    @Override
    public Integer getRoleId() {
        return this.roleId;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    @Override
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

}
