package cn.hg.solon.youcan.system.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table(value = "sys_user_position_mapping")
public class SysUserPositionMapping extends UserPositionMapping {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -923873607882460661L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Auto)
    private Integer id;

    /**
     * 用户ID，关联sys_user表的ID
     */
    private Integer userId;

    /**
     * 岗位ID，关联sys_position表的ID
     */
    private Integer positionId;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public Integer getPositionId() {
        return this.positionId;
    }

    @Override
    public Integer getUserId() {
        return this.userId;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    @Override
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
