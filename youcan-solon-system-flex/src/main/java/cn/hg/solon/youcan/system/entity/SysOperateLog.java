package cn.hg.solon.youcan.system.entity;

import java.util.Date;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table(value = "sys_operate_log")
public class SysOperateLog extends OperateLog {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1915122474666861329L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Auto)
    private Integer id;

    /**
     * 模块标题
     */
    private String title;

    /**
     * 操作类型：OTHER 其他操作，CREATE 新增操作，UPDATE 修改操作，DELETE 删除操作，GRANT 授权操作，EXPORT 导出操作，IMPORT 导入操作，EXIT 强退操作，GENERATE 生成操作，CLEAR 清空操作
     */
    private String type;

    /**
     * 方法名称
     */
    private String method;

    /**
     * 请求方式
     */
    private String requestMethod;

    /**
     * 操作人ID
     */
    private Integer userId;

    /**
     * 操作人员
     */
    private String userName;

    /**
     * 部门ID
     */
    private Integer deptId;

    /**
     * 部门名
     */
    private String deptName;

    /**
     * 请求URL
     */
    private String url;

    /**
     * IP地址
     */
    private String ip;

    /**
     * 操作地点
     */
    private String location;

    /**
     * 请求参数
     */
    private String param;

    /**
     * 返回参数
     */
    private String result;

    /**
     * 操作状态：SUCCESS 成功，FAIL 失败
     */
    private String status;

    /**
     * 错误消息
     */
    private String message;

    /**
     * 操作时间
     */
    private Date actionDatetime;

    /**
     * 消耗时间（ms)
     */
    private Integer cost;

    @Override
    public Date getActionDatetime() {
        return this.actionDatetime;
    }

    @Override
    public Integer getCost() {
        return this.cost;
    }

    @Override
    public Integer getDeptId() {
        return this.deptId;
    }

    @Override
    public String getDeptName() {
        return this.deptName;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getIp() {
        return this.ip;
    }

    @Override
    public String getLocation() {
        return this.location;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getMethod() {
        return this.method;
    }

    @Override
    public String getParam() {
        return this.param;
    }

    @Override
    public String getRequestMethod() {
        return this.requestMethod;
    }

    @Override
    public String getResult() {
        return this.result;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public String getUrl() {
        return this.url;
    }

    @Override
    public Integer getUserId() {
        return this.userId;
    }

    @Override
    public String getUserName() {
        return this.userName;
    }

    @Override
    public void setActionDatetime(Date actionDatetime) {
        this.actionDatetime = actionDatetime;
    }

    @Override
    public void setCost(Integer cost) {
        this.cost = cost;
    }

    @Override
    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    @Override
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public void setParam(String param) {
        this.param = param;
    }

    @Override
    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    @Override
    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

}
