package cn.hg.solon.youcan.system.provider;

import cn.hg.solon.youcan.flex.util.QueryWrapperUtil;
import cn.hg.solon.youcan.system.entity.Notice;
import cn.hg.solon.youcan.system.entity.SysNotice;
import cn.hg.solon.youcan.system.mapper.SysNoticeMapper;
import cn.hg.solon.youcan.system.service.NoticeService;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.List;
import java.util.Map;

import static cn.hg.solon.youcan.system.entity.table.SysNoticeTableDef.SYS_NOTICE;

/**
 * @author 胡高
 */
@Component
public class SysNoticeProvider extends ServiceImpl<SysNoticeMapper, SysNotice> implements NoticeService {

    @Override
    public boolean delete(List<Integer> idList) {
        return this.getMapper().deleteBatchByIds(idList) > 0;
    }

    @Override
    public SysNotice get(Integer id) {
        return this.getMapper().selectOneById(id);
    }

    @Tran
    @Override
    public boolean insert(Notice bean) {
        SysNotice cloneBean = BeanUtil.copyProperties(bean, SysNotice.class);

        return this.getMapper().insert(cloneBean) > 0;
    }

    @Override
    public PageResult<SysNotice> pageBy(org.dromara.hutool.db.Page page, Map<String, Object> paraMap) {
        String word = (String) paraMap.get("word");
        String type = (String) paraMap.get("type");
        String status = (String) paraMap.get("status");

        QueryWrapper query = QueryWrapper.create()
                .where(SYS_NOTICE.TYPE.eq(type).when(StrValidator.isNotBlank(type))
                        .and(SYS_NOTICE.STATUS.eq(status).when(StrValidator.isNotBlank(status)))
                        .and(SYS_NOTICE.TITLE.like(word).when(StrValidator.isNotBlank(word))
                                .or(SYS_NOTICE.CONTENT.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_NOTICE.REMARK.like(word).when(StrValidator.isNotBlank(word)))
                        )
                );

        Page<SysNotice> pageList = this.getMapper().paginate(Page.of(page.getPageNumber(), page.getPageSize()),
                QueryWrapperUtil.applyOrderBy(query, page));

        PageResult<SysNotice> result = new PageResult<>();
        result.addAll(pageList.getRecords());
        result.setTotal(ConvertUtil.toInt(pageList.getTotalRow()));

        return result;
    }

    @Tran
    @Override
    public boolean update(Notice bean) {
        SysNotice cloneBean = BeanUtil.copyProperties(bean, SysNotice.class);

        return this.getMapper().update(cloneBean) > 0;
    }

}
