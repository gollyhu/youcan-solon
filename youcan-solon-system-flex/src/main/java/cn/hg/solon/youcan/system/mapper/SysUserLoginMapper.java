package cn.hg.solon.youcan.system.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.mybatisflex.core.BaseMapper;

import cn.hg.solon.youcan.system.entity.SysUserLogin;

/**
 *  映射层。
 *
 * @author 胡高
 * @since 2023-06-08
 */
@Mapper
public interface SysUserLoginMapper extends BaseMapper<SysUserLogin> {

}
