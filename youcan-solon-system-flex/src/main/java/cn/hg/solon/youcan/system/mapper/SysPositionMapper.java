package cn.hg.solon.youcan.system.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.mybatisflex.core.BaseMapper;

import cn.hg.solon.youcan.system.entity.SysPosition;

@Mapper
public interface SysPositionMapper extends BaseMapper<SysPosition> {

}
