package cn.hg.solon.youcan.system.entity;

import java.util.Date;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

import cn.hg.solon.youcan.flex.listener.EntityInsertListener;
import cn.hg.solon.youcan.flex.listener.EntityUpdateListener;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table(value = "sys_user", onInsert = EntityInsertListener.class, onUpdate = EntityUpdateListener.class)
public class SysUser extends User {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6848697527045710882L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Auto)
    private Integer id;

    /**
     * 部门ID
     */
    private Integer deptId;

    /**
     * 账号
     */
    private String account;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 用户类型（SYSTEM 系统用户，REGISTE注册用户）
     */
    private String type;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 用户性别（M 男，F 女，U 未知）
     */
    private String gender;

    /**
     * 头像路径
     */
    private String avatar;

    /**
     * 密码
     */
    private String password;

    /**
     * 加密盐
     */
    private String salt;

    /**
     * 帐号状态（ON 正常，OFF 停用）
     */
    private String status;

    /**
     * 删除标志（true 是，false 否）
     */
    @Column(value = "is_del", isLogicDelete = true)
    private Boolean isDel;

    /**
     * 最后登录IP
     */
    private String loginIp;

    /**
     * 最后登录时间
     */
    private Date loginDatetime;

    /**
     * 创建者
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 更新者
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    /**
     * 备注
     */
    private String remark;

    @Override
    public String getAccount() {
        return this.account;
    }

    @Override
    public String getAvatar() {
        return this.avatar;
    }

    @Override
    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    @Override
    public Integer getCreator() {
        return this.creator;
    }

    @Override
    public Boolean getIsDel() {
        return this.isDel;
    }

    @Override
    public Integer getDeptId() {
        return this.deptId;
    }

    @Override
    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    @Override
    public Integer getEditor() {
        return this.editor;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    @Override
    public String getGender() {
        return this.gender;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public Date getLoginDatetime() {
        return this.loginDatetime;
    }

    @Override
    public String getLoginIp() {
        return this.loginIp;
    }

    @Override
    public String getNickname() {
        return this.nickname;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getPhone() {
        return this.phone;
    }

    @Override
    public String getRemark() {
        return this.remark;
    }

    @Override
    public String getSalt() {
        return this.salt;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    @Override
    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    @Override
    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    @Override
    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    @Override
    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    @Override
    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setLoginDatetime(Date loginDatetime) {
        this.loginDatetime = loginDatetime;
    }

    @Override
    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    @Override
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

}
