package cn.hg.solon.youcan.system.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.mybatisflex.core.BaseMapper;

import cn.hg.solon.youcan.system.entity.SysOperateLog;

@Mapper
public interface SysOperateLogMapper extends BaseMapper<SysOperateLog> {

}
