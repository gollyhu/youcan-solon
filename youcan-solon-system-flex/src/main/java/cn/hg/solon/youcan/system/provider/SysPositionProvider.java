package cn.hg.solon.youcan.system.provider;

import cn.hg.solon.youcan.common.exception.ServiceException;
import cn.hg.solon.youcan.flex.util.QueryWrapperUtil;
import cn.hg.solon.youcan.system.entity.Position;
import cn.hg.solon.youcan.system.entity.SysPosition;
import cn.hg.solon.youcan.system.mapper.SysPositionMapper;
import cn.hg.solon.youcan.system.service.PositionService;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.List;
import java.util.Map;

import static cn.hg.solon.youcan.system.entity.table.SysPositionTableDef.SYS_POSITION;

/**
 * @author 胡高
 */
@Component
public class SysPositionProvider extends ServiceImpl<SysPositionMapper, SysPosition> implements PositionService {

    @Override
    public boolean checkUnique(Position bean) {
        // 查找已存在记录
        QueryWrapper query = QueryWrapper.create()
                .where(SYS_POSITION.CODE.eq(bean.getCode())
                        .and(SYS_POSITION.ID.ne(bean.getId()))
                );

        return ObjUtil.isNull(this.getOne(query));
    }

    @Tran
    @Override
    public boolean delete(List<Integer> idList) {
        return this.getMapper().deleteBatchByIds(idList) > 0;
    }

    @Override
    public SysPosition get(Integer id) {
        return this.getMapper().selectOneById(id);
    }

    @Tran
    @Override
    public boolean insert(Position bean) {
        if (!this.checkUnique(bean)) {
            throw new ServiceException("岗位编码已经存在，请更换其它值！");
        }

        SysPosition cloneBean = BeanUtil.copyProperties(bean, SysPosition.class);

        return this.getMapper().insert(cloneBean) > 0;
    }

    @Override
    @Tran
    public PageResult<SysPosition> pageBy(org.dromara.hutool.db.Page page, Map<String, Object> paraMap) {
        String status = (String) paraMap.get("status");
        String word = (String) paraMap.get("word");

        QueryWrapper query = QueryWrapper.create()
                .where(SYS_POSITION.STATUS.eq(status).when(StrValidator.isNotBlank(status))
                        .and(SYS_POSITION.NAME.like(word).when(StrValidator.isNotBlank(word))
                                .or(SYS_POSITION.CODE.like(word).when(StrValidator.isNotBlank(word)))
                        )
                );

        Page<SysPosition> pageList = this.getMapper().paginate(Page.of(page.getPageNumber(), page.getPageSize()),
                QueryWrapperUtil.applyOrderBy(query, page));

        PageResult<SysPosition> result = new PageResult<>();
        result.addAll(pageList.getRecords());
        result.setTotal(ConvertUtil.toInt(pageList.getTotalRow()));

        return result;
    }

    @Tran
    @Override
    public boolean update(Position bean) {
        if (!this.checkUnique(bean)) {
            throw new ServiceException("岗位编码已经存在，请更换其它值！");
        }

        SysPosition cloneBean = BeanUtil.copyProperties(bean, SysPosition.class);

        return this.getMapper().update(cloneBean) > 0;
    }

}
