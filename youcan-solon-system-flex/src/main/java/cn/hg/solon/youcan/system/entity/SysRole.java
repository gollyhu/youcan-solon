package cn.hg.solon.youcan.system.entity;

import java.util.Date;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

import cn.hg.solon.youcan.flex.listener.EntityInsertListener;
import cn.hg.solon.youcan.flex.listener.EntityUpdateListener;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table(value = "sys_role", onInsert = EntityInsertListener.class, onUpdate = EntityUpdateListener.class)
public class SysRole extends Role {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6159678246238844283L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Auto)
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 权限字符串
     */
    private String code;


    private Integer sort;

    /**
     * 数据范围（ALL：全部数据权限 CUSTOM：自定数据权限 DEPARTMENT：本部门数据权限 BELOW：本部门及以下数据权限）
     */
    private String dataScope;

    /**
     * 帐号状态（ON 正常，OFF 停用）
     */
    private String status;

    /**
     * 删除标志（true 是，false 否）
     */
    @Column(value = "is_del", isLogicDelete = true)
    private Boolean isDel;

    /**
     * 创建者
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 更新者
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    /**
     * 备注
     */
    private String remark;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    @Override
    public Integer getCreator() {
        return this.creator;
    }

    @Override
    public String getDataScope() {
        return this.dataScope;
    }

    @Override
    public Boolean getIsDel() {
        return this.isDel;
    }

    @Override
    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    @Override
    public Integer getEditor() {
        return this.editor;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRemark() {
        return this.remark;
    }

    @Override
    public Integer getSort() {
        return this.sort;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    @Override
    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    @Override
    public void setDataScope(String dataScope) {
        this.dataScope = dataScope;
    }

    @Override
    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    @Override
    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    @Override
    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

}
