package cn.hg.solon.youcan.system.provider;

import cn.hg.solon.youcan.flex.util.QueryWrapperUtil;
import cn.hg.solon.youcan.system.entity.SysUserLogin;
import cn.hg.solon.youcan.system.entity.UserLogin;
import cn.hg.solon.youcan.system.mapper.SysUserLoginMapper;
import cn.hg.solon.youcan.system.service.UserLoginService;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static cn.hg.solon.youcan.system.entity.table.SysUserLoginTableDef.SYS_USER_LOGIN;

/**
 * @author 胡高
 */
@Component
public class SysUserLoginProvider extends ServiceImpl<SysUserLoginMapper, SysUserLogin> implements UserLoginService {

    @Tran
    @Override
    public boolean clear() {
        return this.remove(SYS_USER_LOGIN.ID.ne(0));
    }

    @Tran
    @Override
    public boolean delete(List<Integer> idList) {
        return this.getMapper().deleteBatchByIds(idList) > 0;
    }

    @Override
    public SysUserLogin get(Integer id) {
        return this.getMapper().selectOneById(id);
    }

    @Tran
    @Override
    public boolean insert(UserLogin bean) {
        SysUserLogin cloneBean = BeanUtil.copyProperties(bean, SysUserLogin.class);

        return this.getMapper().insert(cloneBean) > 0;
    }

    @Override
    public PageResult<SysUserLogin> pageBy(org.dromara.hutool.db.Page page, Map<String, Object> paraMap) {
        String word = (String) paraMap.get("word");
        String status = (String) paraMap.get("status");
        Date startDate = (Date) paraMap.get("startDate");
        Date endDate = (Date) paraMap.get("endDate");

        QueryWrapper query = QueryWrapper.create()
                .where(SYS_USER_LOGIN.STATUS.eq(status).when(StrValidator.isNotBlank(status))
                        .and(SYS_USER_LOGIN.LOGIN_DATETIME.ge(startDate).when(ObjUtil.isNotNull(startDate)))
                        .and(SYS_USER_LOGIN.LOGIN_DATETIME.le(endDate).when(ObjUtil.isNotNull(endDate)))
                        .and(SYS_USER_LOGIN.ACCOUNT.like(word).when(StrValidator.isNotBlank(word))
                                .or(SYS_USER_LOGIN.BROWSER.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_USER_LOGIN.LOCATION.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_USER_LOGIN.MSG.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_USER_LOGIN.NICKNAME.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_USER_LOGIN.OS.like(word).when(StrValidator.isNotBlank(word)))
                        )
                );

        Page<SysUserLogin> pageList = this.getMapper().paginate(Page.of(page.getPageNumber(), page.getPageSize()),
                QueryWrapperUtil.applyOrderBy(query, page));

        PageResult<SysUserLogin> result = new PageResult<>();
        result.addAll(pageList.getRecords());
        result.setTotal(ConvertUtil.toInt(pageList.getTotalRow()));

        return result;
    }

}
