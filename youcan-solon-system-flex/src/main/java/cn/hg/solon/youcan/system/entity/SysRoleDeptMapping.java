package cn.hg.solon.youcan.system.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table(value = "sys_role_dept_mapping")
public class SysRoleDeptMapping extends RoleDeptMapping {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1960875350055066794L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Auto)
    private Integer id;

    /**
     * 角色ID，关联sys_role表的ID
     */
    private Integer roleId;

    /**
     * 部门ID，关联sys_dept表的ID
     */
    private Integer deptId;

    @Override
    public Integer getDeptId() {
        return this.deptId;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public Integer getRoleId() {
        return this.roleId;
    }

    @Override
    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

}
