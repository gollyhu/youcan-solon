package cn.hg.solon.youcan.system.provider;

import cn.hg.solon.youcan.common.constant.AppConstants;
import cn.hg.solon.youcan.common.exception.ServiceException;
import cn.hg.solon.youcan.flex.util.QueryWrapperUtil;
import cn.hg.solon.youcan.system.entity.Config;
import cn.hg.solon.youcan.system.entity.SysConfig;
import cn.hg.solon.youcan.system.mapper.SysConfigMapper;
import cn.hg.solon.youcan.system.service.ConfigService;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import static cn.hg.solon.youcan.system.entity.table.SysConfigTableDef.SYS_CONFIG;

/**
 * @author 胡高
 */
@Component
public class SysConfigProvider extends ServiceImpl<SysConfigMapper, SysConfig> implements ConfigService {

    @Override
    public boolean checkUnique(Config bean) {
        // 查找已存在记录
        QueryWrapper query = QueryWrapper.create()
                .where(SYS_CONFIG.TYPE.eq(bean.getType())
                        .and(SYS_CONFIG.CODE.eq(bean.getCode()))
                        .and(SYS_CONFIG.ID.ne(bean.getId()))
                );

        return ObjUtil.isNull(this.getOne(query));
    }

    @Tran
    @Override
    public boolean delete(List<Integer> idList) {
        for (Serializable id : idList) {
            Config item = this.getById(id);

            if (ObjUtil.isNotNull(item) && item.getIsSys()) {
                throw new ServiceException(AppConstants.RETURN_CODE_VALUE_FAIL, "[" + item.getName() + "] 为系统参数，不允许删除！");
            }
        }

        return this.getMapper().deleteBatchByIds(idList) > 0;
    }

    @Override
    public SysConfig get(Integer id) {
        return this.getMapper().selectOneById(id);
    }

    @Override
    public SysConfig getByTypeAndCode(String type, String code) {
        QueryWrapper query = QueryWrapper.create().where(SYS_CONFIG.TYPE.eq(type).and(SYS_CONFIG.CODE.eq(code)));
        return this.getOne(query);
    }

    @Tran
    @Override
    public boolean insert(Config bean) {
        if (!this.checkUnique(bean)) {
            throw new ServiceException("参数代码已经存在，请更换其它值！");
        }

        SysConfig cloneBean = BeanUtil.copyProperties(bean, SysConfig.class);

        return this.getMapper().insert(cloneBean) > 0;
    }

    @Override
    public List<SysConfig> listByType(String type) {
        return this.getMapper().selectListByCondition(SYS_CONFIG.TYPE.eq(type));
    }

    @Override
    public PageResult<SysConfig> pageBy(org.dromara.hutool.db.Page page, Map<String, Object> paraMap) {
        String type = (String) paraMap.get("type");
        String word = (String) paraMap.get("word");

        QueryWrapper query = QueryWrapper.create()
                .where(SYS_CONFIG.TYPE.eq(type).when(StrValidator.isNotBlank(type))
                        .and(SYS_CONFIG.NAME.like(word).when(StrValidator.isNotBlank(word))
                                .or(SYS_CONFIG.CODE.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_CONFIG.VALUE.like(word).when(StrValidator.isNotBlank(word)))
                        )
                );

        Page<SysConfig> pageList = this.getMapper().paginate(Page.of(page.getPageNumber(), page.getPageSize()),
                QueryWrapperUtil.applyOrderBy(query, page));

        PageResult<SysConfig> result = new PageResult<>();
        result.addAll(pageList.getRecords());
        result.setTotal(ConvertUtil.toInt(pageList.getTotalRow()));

        return result;
    }

    @Tran
    @Override
    public boolean update(Config bean) {
        if (!this.checkUnique(bean)) {
            throw new ServiceException("参数代码已经存在，请更换其它值！");
        }

        SysConfig cloneBean = BeanUtil.copyProperties(bean, SysConfig.class);

        return this.getMapper().update(cloneBean) > 0;
    }

}
