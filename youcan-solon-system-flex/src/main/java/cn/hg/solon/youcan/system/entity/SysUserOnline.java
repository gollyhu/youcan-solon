package cn.hg.solon.youcan.system.entity;

import java.util.Date;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table(value = "sys_user_online")
public class SysUserOnline extends UserOnline {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3234152087906310617L;

    /**
     * 用户会话id
     */
    @Id
    private String id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 登录设备
     */
    private String device;

    /**
     * 登录IP地址
     */
    private String ip;

    /**
     * 登录地点
     */
    private String location;

    /**
     * 浏览器类型
     */
    private String browser;

    /**
     * 操作系统
     */
    private String os;

    /**
     * ON：在线，OFF：离线
     */
    private String status;

    /**
     * session创建时间
     */
    private Date startDatetime;

    /**
     * session最后访问时间
     */
    private Date activityDatetime;

    /**
     * 超时时间(秒)
     */
    private Integer expireSecond;

    @Override
    public Date getActivityDatetime() {
        return this.activityDatetime;
    }

    @Override
    public String getBrowser() {
        return this.browser;
    }

    @Override
    public String getDeptName() {
        return this.deptName;
    }

    @Override
    public String getDevice() {
        return this.device;
    }

    @Override
    public Integer getExpireSecond() {
        return this.expireSecond;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getIp() {
        return this.ip;
    }

    @Override
    public String getLocation() {
        return this.location;
    }

    @Override
    public String getNickname() {
        return this.nickname;
    }

    @Override
    public String getOs() {
        return this.os;
    }

    @Override
    public Date getStartDatetime() {
        return this.startDatetime;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public Integer getUserId() {
        return this.userId;
    }

    @Override
    public void setActivityDatetime(Date activityDatetime) {
        this.activityDatetime = activityDatetime;
    }

    @Override
    public void setBrowser(String browser) {
        this.browser = browser;
    }

    @Override
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @Override
    public void setDevice(String device) {
        this.device = device;
    }

    @Override
    public void setExpireSecond(Integer expireSecond) {
        this.expireSecond = expireSecond;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void setOs(String os) {
        this.os = os;
    }

    @Override
    public void setStartDatetime(Date startDatetime) {
        this.startDatetime = startDatetime;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
