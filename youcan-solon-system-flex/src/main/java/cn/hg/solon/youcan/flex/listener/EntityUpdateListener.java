/**
 * 
 */
package cn.hg.solon.youcan.flex.listener;

import cn.hg.solon.youcan.common.constant.WebConstants;
import com.mybatisflex.annotation.UpdateListener;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.date.DateUtil;
import org.dromara.hutool.core.util.ObjUtil;
import org.noear.solon.Solon;

import java.util.function.Supplier;

/**
 * @author 胡高
 *
 */
public class EntityUpdateListener implements UpdateListener {

    @Override
    public void onUpdate(Object entity) {
        Supplier<Integer> getter = Solon.context().getBean(WebConstants.USER_ID_GETTER);
        if (ObjUtil.isNull(getter)) {
            return;
        }
        try {
            BeanUtil.setProperty(entity, "editor", getter.get());
            BeanUtil.setProperty(entity, "editedDatetime", DateUtil.now());
        } catch (Exception e) {
        }
    }

}
