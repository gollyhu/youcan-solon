package cn.hg.solon.youcan.system.entity;

import java.util.Date;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

import cn.hg.solon.youcan.flex.listener.EntityInsertListener;
import cn.hg.solon.youcan.flex.listener.EntityUpdateListener;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table(value = "sys_notice", onInsert = EntityInsertListener.class, onUpdate = EntityUpdateListener.class)
public class SysNotice extends Notice {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 379144863440528321L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Auto)
    private Integer id;

    /**
     * 公告标题
     */
    private String title;

    /**
     * 公告类型：NOTICE 通知，ANNOUNCEMENT：公告
     */
    private String type;

    /**
     * 公告内容
     */
    private String content;

    /**
     * 状态（ON 正常, OFF 停用）
     */
    private String status;

    /**
     * 删除标志（true 是，false 否）
     */
    @Column(value = "is_del", isLogicDelete = true)
    private Boolean isDel;

    /**
     * 创建者
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 更新者
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    /**
     * 备注
     */
    private String remark;

    @Override
    public String getContent() {
        return this.content;
    }

    @Override
    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    @Override
    public Integer getCreator() {
        return this.creator;
    }

    @Override
    public Boolean getIsDel() {
        return this.isDel;
    }

    @Override
    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    @Override
    public Integer getEditor() {
        return this.editor;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getRemark() {
        return this.remark;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    @Override
    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    @Override
    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    @Override
    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    @Override
    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

}
