package cn.hg.solon.youcan.job.entity;

import cn.hg.solon.youcan.common.enums.BusinessStatus;
import cn.hg.solon.youcan.job.entity.proxy.EqJobLogProxy;
import com.easy.query.core.annotation.Column;
import com.easy.query.core.annotation.EntityProxy;
import com.easy.query.core.annotation.Table;
import com.easy.query.core.proxy.ProxyEntityAvailable;

import java.util.Date;

@Table("sys_job_log")
@EntityProxy
public class EqJobLog extends JobLog implements ProxyEntityAvailable<EqJobLog, EqJobLogProxy> {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3890218080170032607L;

    /**
     * ID
     */
	@Column(primaryKey = true)
    private Integer id;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 任务组名
     */
    private String group;

    /**
     * 调用目标字符串
     */
    private String target;

    /**
     * 日志信息
     */
    private String message;

    /**
     * 任务状态：SUCCESS 成功，FAIL 失败
     */
    private String status = BusinessStatus.SUCCESS.name();

    /**
     * 异常信息
     */
    private String exception;

    /**
     * 执行时间（ms）
     */
    private Long cost;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 开始时间
     */
    private Date startedDatetime;

    /**
     * 结束时间
     */
    private Date endedDatetime;

    public Long getCost() {
        return this.cost;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public Date getEndedDatetime() {
        return this.endedDatetime;
    }

    public String getException() {
        return this.exception;
    }

    public String getGroup() {
        return this.group;
    }

    public Integer getId() {
        return this.id;
    }

    public String getMessage() {
        return this.message;
    }

    public String getName() {
        return this.name;
    }

    public Date getStartedDatetime() {
        return this.startedDatetime;
    }

    public String getStatus() {
        return this.status;
    }

    public String getTarget() {
        return this.target;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public void setEndedDatetime(Date endedDatetime) {
        this.endedDatetime = endedDatetime;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStartedDatetime(Date startedDatetime) {
        this.startedDatetime = startedDatetime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public Class<EqJobLogProxy> proxyTableClass() {
        return EqJobLogProxy.class;
    }
}
