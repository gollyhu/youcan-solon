package cn.hg.solon.youcan.job.entity;

import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.job.entity.proxy.EqJobProxy;
import com.easy.query.core.annotation.Column;
import com.easy.query.core.annotation.EntityProxy;
import com.easy.query.core.annotation.Table;
import com.easy.query.core.proxy.ProxyEntityAvailable;

import java.util.Date;

@Table("sys_job")
@EntityProxy
public class EqJob extends Job implements ProxyEntityAvailable<EqJob, EqJobProxy> {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4170416843641096555L;

    /**
     * ID
     */
	@Column(primaryKey = true)
    private Integer id;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 任务组名
     */
    private String group;

    /**
     * 调用目标字符串
     */
    private String target;

    /**
     * cron执行表达式
     */
    private String cron;

    /**
     * 计划执行错误策略（IMMEDIATELY 立即执行，ONCE 执行一次，ABORT 放弃执行）
     */
    private String policy = "ABORT";

    /**
     * 是否并发执行
     */
    private Boolean isConcurrent = Boolean.FALSE;

    /**
     * 是否记录调度日志
     */
    private Boolean isLog = Boolean.TRUE;

    /**
     * 任务状态（ON 启用, OFF 停用）
     */
    private String status = BeanStatus.OFF.name();

    /**
     * 创建者
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 更新者
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    /**
     * 备注
     */
    private String remark;

    public Boolean getIsConcurrent() {
        return this.isConcurrent;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public Integer getCreator() {
        return this.creator;
    }

    public String getCron() {
        return this.cron;
    }

    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    public Integer getEditor() {
        return this.editor;
    }

    public String getGroup() {
        return this.group;
    }

    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getPolicy() {
        return this.policy;
    }

    public String getRemark() {
        return this.remark;
    }

    public String getStatus() {
        return this.status;
    }

    public String getTarget() {
        return this.target;
    }

    public void setIsConcurrent(Boolean isConcurrent) {
        this.isConcurrent = isConcurrent;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public Class<EqJobProxy> proxyTableClass() {
        return EqJobProxy.class;
    }

    @Override
    public Boolean getIsLog() {
        return isLog;
    }

    @Override
    public void setIsLog(Boolean log) {
        isLog = log;
    }
}
