package cn.hg.solon.youcan.job.listener;

import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.event.AppLoadEndEvent;
import org.noear.solon.core.event.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.hg.solon.youcan.job.service.JobService;

/**
 * @author 胡高
 */
@Component
public class AppLoadEndEventListener implements EventListener<AppLoadEndEvent> {
    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    JobService jobService;

    @Override
    public void onEvent(AppLoadEndEvent event) throws Throwable {
        this.log.info("初始化定时任务开始。。。");
        this.jobService.init();
        this.log.info("初始化定时任务结束。。。");
    }
}