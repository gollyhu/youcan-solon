/**
 *
 */
package cn.hg.solon.youcan.job.provider;

import cn.hg.solon.youcan.easyquery.util.EntityQueryableUtil;
import cn.hg.solon.youcan.job.entity.EqJobLog;
import cn.hg.solon.youcan.job.entity.JobLog;
import cn.hg.solon.youcan.job.entity.proxy.EqJobLogProxy;
import cn.hg.solon.youcan.job.service.JobLogService;
import com.easy.query.api.proxy.client.EasyEntityQuery;
import com.easy.query.api.proxy.entity.select.EntityQueryable;
import com.easy.query.core.api.pagination.EasyPageResult;
import com.easy.query.solon.annotation.Db;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 调度日志服务实现
 *
 * @author 胡高
 */
@Component
public class EqJobLogProvider implements JobLogService {

    @Db("db1")
    private EasyEntityQuery easyEntityQuery;

    private EntityQueryable<EqJobLogProxy, EqJobLog> buildQuery(Page page, Map<String, Object> paraMap) {
        String word = (String) paraMap.get("word");
        String group = (String) paraMap.get("group");
        Date startDate = (Date) paraMap.get("startDate");
        Date endDate = (Date) paraMap.get("endDate");
        String status = (String) paraMap.get("status");

        EntityQueryable<EqJobLogProxy, EqJobLog> entityQueryable = this.easyEntityQuery
                // FROM sys_job_log AS t
                .queryable(EqJobLog.class)
                // WHERE t.`group` = ${group} AND t.`status` = ${status}
                //      AND ${startDate} <= t.`started_datetime` AND t.`started_datetime` <= ${endDate}
                .where(t -> {
                    t.group().eq(StrValidator.isNotBlank(group), group);
                    t.status().eq(StrValidator.isNotBlank(status), status);
                    t.startedDatetime().rangeClosed(ObjUtil.isNotNull(startDate), startDate, ObjUtil.isNotNull(endDate), endDate);
                    // AND (t.`name` LIKE '%${word}%' OR t.`target` LIKE '%${word}%' OR t.`exception` LIKE '%${word}%'
                    //      OR t.`message` LIKE '%${word}%')
                    t.or(StrValidator.isNotBlank(word), () -> {
                        t.name().like(word);
                        t.target().like(word);
                        t.exception().like(word);
                        t.message().like(word);
                    });
                });

        // ORDER BY ${sortField}
        return EntityQueryableUtil.applyOrderBy(entityQueryable, page);
    }

    @Override
    public boolean clear() {
        return this.easyEntityQuery.deletable(EqJobLog.class)
                .where(t -> t.id().ge(0))
                .allowDeleteStatement(true)
                .executeRows() > 0L;
    }

    @Override
    public boolean delete(List<Integer> idList) {
        return this.easyEntityQuery.deletable(EqJobLog.class)
                .whereByIds(idList)
                .allowDeleteStatement(true)
                .executeRows() > 0L;
    }

    @Override
    public JobLog get(Integer id) {
        return this.easyEntityQuery.queryable(EqJobLog.class).whereById(id).firstOrNull();
    }

    @Override
    public boolean insert(JobLog bean) {
        EqJobLog cloneBean = BeanUtil.copyProperties(bean, EqJobLog.class);

        return this.easyEntityQuery.insertable(cloneBean).executeRows() > 0L;
    }

    @Override
    public PageResult<? extends JobLog> pageBy(Page page, Map<String, Object> paraMap) {
        EasyPageResult<EqJobLog> pageList = this.buildQuery(page, paraMap).toPageResult(page.getPageNumber(), page.getPageSize());

        PageResult<EqJobLog> result = new PageResult<>();
        result.addAll(pageList.getData());
        result.setTotal(ConvertUtil.toInt(pageList.getTotal()));

        return result;
    }

}
