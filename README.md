# Youcan-Solon



## 简介
​	基于 Solon 框架，功能类似 Ruoyi 的 WEB 脚手架应用。
​	此应用摒弃 Spring 臃肿框架，尽量多采用国产开源软件，符合国人习性，为不大会 Web 编程的后端程序员提供一个相对简单易上手的 Web 脚手架应用，简单熟悉后就可按自己习惯任意改造使用。

<br>

## 项目说明
​	后端基于 Solon，前端采用 Pear Admin 的快速开发框架。

​	项目采用了多种经典开源软件（Solon + Sa-Token + MyBatis-Flex 或 easy-query + Pear Admin + Enjoy + Hutool），内置模块如：部门管理、角色用户、菜单及按钮授权、数据权限、系统参数、日志管理、通知公告等。可在线定时任务配置。

<br>

## 联系方式

​	QQ交流群：439091540

<br>

## 在线体验

- 演示地址：http://youcan.wiki

- guest / 123456

  admin 账号可向作者索取；

<br>

------

​	未来规划：

| 功能                     | 负责人 | 开发进度 | 测试进度 | 时间       |
| ------------------------ | ------ | -------- | -------- | ---------- |
| 增加对 easy-query 的支持 | 糊搞   | 已完成   | 已完成   | 2023-08-31 |
| 全新封装的 JS 库         | 糊搞   | 已完成   | 已完成   | 2024-10-21 |
| 代码生成模块             | 糊搞   | 计划中   |          |            |

​	**注意：框架中使用了未发布的 Hutool V6 版本，请谨慎用于生产环境。**

<br>


## 安装教程

- 数据库

  使用 MySql 5.7 以上版本，新建名为 youcan_solon 的数据库，将 youcan_solon.sql 导入到新数据库即可。

- 配置文件

  youcan-solon-admin 模块的 resources/app-*.yml 中，修改服务端口和数据库密码。

- Hutool

  采用了未来才发布的 Hutool 6（目前是 V6.0.0-M16 版本）；由于是**预览版本，在生产环境上使用之前请权衡**。

- easy-query

  在 V1.1.0 版本中增加实现，因为其稳定性高，今后将做为默认ORM。

- MyBatis-Flex

  在项目根文件夹下运行 mvn clean install，然后在 youcan-solon-system 模块和 youcan-solon-job 模块引入“/target/generated-sources/annotations”下生成好的代码。

- Redis

  框架采用了 Redis 做缓存，启动时需要进行连接，在配置文件中修改 Redis 地址就好。

- 应用入口

  入口在 youcan-solon-admin 模块的 cn.hg.solon.youcan.YoucanSolonApp.java 中，直接启动即可。

- Maven打包

  默认打 Fat 包指令：mvn clean package -Dmaven.test.skip=true

  打包完成后，找到"youcan-solon\youcan-solon-admin\target\youcan-solon-admin.jar"。

- 运行应用

  开发模式运行：java -jar youcan-solon-admin.jar

  正式模式运行：java -jar -Dsolon.env=prod youcan-solon-admin.jar

<br>

## 开发指南（补充中。。。）

1.  前端开发 [指南](./doc/前端开发指南.md) ;

<br>

## 主要特性

- 完全响应式布局（支持电脑、平板、手机等所有主流设备）；
- 支持按钮及数据权限，可自定义部门数据权限；
- 对常用 js 插件进行二次封装，使 js 代码变得简洁，更加易维护；
- Maven 多项目依赖，模块及插件分项目，尽量松耦合，方便模块升级、增减模块；
- 支持服务监控，数据监控，缓存监控功能；

<br>

## 项目结构

```text
youcan-solon
├── common            // 通用模块
│       └── constant                      // 通用常量
│       └── controller                    // 控制器基类
│       └── enums                         // 通用枚举
│       └── exception                     // 通用异常
│       └── util                          // 通用工具	
│       └── vo                            // VO 基类
│       └── web                           // Web 页面相关类
├── framework         // 核心框架模块
│       └── cache                         // 缓存实现
│       └── config                        // 系统配置
│       └── directive                     // 页面通知指令
│       └── listener                      // 系统业务监听器
│       └── provider                      // 系统服务提供者
│       └── satoken                       // Sa-Token 相关
│       └── service                       // 系统服务接口
│       └── task                          // 系统任务
│       └── web                           // 前端控制
├── generator         // 代码生成模块（规划中）
├── job               // 定时任务模块接口（可移除）
│       └── entity                         // 业务实体
│       └── service                        // 服务接口
├── job-flex          // 定时任务模块 Mybatis-flex 实现（可移除）
├── job-easyquery     // 定时任务模块 easy-query 实现（可移除）
├── system            // 系统业务模块接口
│       └── entity                         // 业务实体
│       └── service                        // 服务接口
├── system-flex       // 系统业务模块 Mybatis-flex 实现
├── system-easyquery  // 系统业务模块 easy-query 实现
├── admin             // Admin管理平台模块
```

<br>

## 内置功能

1.   **系统用户**：用户是系统操作者，该功能主要完成系统用户配置。
2.   **系统角色**：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
3.   **系统权限**：配置系统菜单，操作权限，按钮权限标识等。
4.   **行政部门**：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
5.   **公司岗位**：配置系统用户所属担任职务。
6.   **数据字典**：对系统中经常使用的一些较为固定的数据进行维护。
7.   **系统参数**：对系统动态配置常用参数。
8.   **通知公告**：系统通知公告信息发布维护。
9.   **在线用户**：当前系统中活跃用户状态监控。
10.   **定时任务**：在线（添加、修改、删除)任务调度包含执行结果日志。
11.   **服务监控**：监视当前系统 CPU、内存、磁盘、堆栈等相关信息。
12.   **缓存监控**：对系统的缓存查询，删除、清空等操作。
13.   **操作日志**：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
14.   **登录日志**：系统登录日志记录查询包含登录异常。

<br>

## 项目示例

- 系统用户

![image-20230627163622803](./doc/images/app-screenshot/sys-user.png)



- 系统角色

![image-20230627164032408](./doc/images/app-screenshot/sys-role.png)



- 系统权限

![image-20230627164123260](./doc/images/app-screenshot/sys-permission.png)



- 行政部门

![image-20230627164448167](./doc/images/app-screenshot/sys-dept.png)



- 公司岗位

![image-20230627164500971](./doc/images/app-screenshot/sys-position.png)



- 数据字典

![image-20230627164515451](./doc/images/app-screenshot/sys-dict.png)

![image-20230627164530613](./doc/images/app-screenshot/sys-dict-item.png)



- 系统参数

![image-20230627164545384](./doc/images/app-screenshot/sys-config.png)



- 通知公告

![image-20230627164554829](./doc/images/app-screenshot/sys-notice.png)



- 在线用户

![image-20230627164628830](./doc/images/app-screenshot/sys-user-online.png)



- 定时任务

![image-20230627164637010](./doc/images/app-screenshot/sys-job.png)

![image-20230627164848448](./doc/images/app-screenshot/sys-job-log.png)



- 服务监控

![image-20230627164649957](./doc/images/app-screenshot/sys-server.png)



- 缓存监控

![image-20230627164705589](./doc/images/app-screenshot/sys-cache.png)



- 操作日志

![image-20230627164806069](./doc/images/app-screenshot/sys-operate-log.png)



- 登录日志

![image-20230627164824996](./doc/images/app-screenshot/sys-user-login.png)



