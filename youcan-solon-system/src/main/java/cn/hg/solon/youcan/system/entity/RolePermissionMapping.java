package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;

/**
 * 角色权限映射
 *
 * @author 胡高
 */
public class RolePermissionMapping implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 4263602636964217535L;

    /**
     * ID
     */
    private Integer id;

    /**
     * 角色ID，关联sys_role表的ID
     */
    private Integer roleId;

    /**
     * 权限ID，关联sys_permission表的ID
     */
    private Integer permissionId;

    public Integer getId() {
        return this.id;
    }

    public Integer getPermissionId() {
        return this.permissionId;
    }

    public Integer getRoleId() {
        return this.roleId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

}
