package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;
import java.util.Date;

import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;

import cn.hg.solon.youcan.common.enums.BusinessStatus;
import cn.hg.solon.youcan.common.validate.UpdateLabel;

/**
 * 操作日志
 *
 * @author 胡高
 */
public class OperateLog implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1723959306772098914L;

    /**
     * ID
     */
    @NotNull(groups = UpdateLabel.class, message = "必须输入ID")
    private Integer id;

    /**
     * 模块标题
     */
    @NotEmpty(message = "必须输入模块标题")
    @Length(min = 2, max = 50, message = "模块标题必须 2 到 50 字符")
    private String title;

    /**
     * 操作类型：OTHER 其他操作，CREATE 新增操作，UPDATE 修改操作，DELETE 删除操作，GRANT 授权操作，EXPORT 导出操作，IMPORT 导入操作，EXIT 强退操作，GENERATE 生成操作，CLEAR 清空操作
     */
    @Length(min = 2, max = 10, message = "操作类型必须 2 到 10 字符")
    private String type;

    /**
     * 方法名称
     */
    @NotEmpty(message = "必须输入方法名称")
    @Length(min = 2, max = 250, message = "方法名称必须 2 到 250 字符")
    private String method;

    /**
     * 请求方式
     */
    @NotEmpty(message = "必须输入请求方式")
    @Length(min = 2, max = 10, message = "请求方式必须 2 到 10 字符")
    private String requestMethod;

    /**
     * 操作人ID
     */
    private Integer userId;

    /**
     * 操作人员
     */
    private String userName;

    /**
     * 部门ID
     */
    private Integer deptId;

    /**
     * 部门名
     */
    private String deptName;

    /**
     * 请求URL
     */
    private String url;

    /**
     * IP地址
     */
    private String ip;

    /**
     * 操作地点
     */
    private String location;

    /**
     * 请求参数
     */
    private String param;

    /**
     * 返回参数
     */
    private String result;

    /**
     * 操作状态：SUCCESS 成功，FAIL 失败
     */
    private String status = BusinessStatus.SUCCESS.name();

    /**
     * 错误消息
     */
    private String message;

    /**
     * 操作时间
     */
    private Date actionDatetime;

    /**
     * 消耗时间（ms)
     */
    private Integer cost;

    public Date getActionDatetime() {
        return this.actionDatetime;
    }

    public Integer getCost() {
        return this.cost;
    }

    public Integer getDeptId() {
        return this.deptId;
    }

    public String getDeptName() {
        return this.deptName;
    }

    public Integer getId() {
        return this.id;
    }

    public String getIp() {
        return this.ip;
    }

    public String getLocation() {
        return this.location;
    }

    public String getMessage() {
        return this.message;
    }

    public String getMethod() {
        return this.method;
    }

    public String getParam() {
        return this.param;
    }

    public String getRequestMethod() {
        return this.requestMethod;
    }

    public String getResult() {
        return this.result;
    }

    public String getStatus() {
        return this.status;
    }

    public String getTitle() {
        return this.title;
    }

    public String getType() {
        return this.type;
    }

    public String getUrl() {
        return this.url;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setActionDatetime(Date actionDatetime) {
        this.actionDatetime = actionDatetime;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
