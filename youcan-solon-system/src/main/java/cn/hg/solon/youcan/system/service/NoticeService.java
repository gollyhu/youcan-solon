package cn.hg.solon.youcan.system.service;

import cn.hg.solon.youcan.system.entity.Notice;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;

import java.util.List;
import java.util.Map;

/**
 * 通知公告服务
 * 
 * @author 胡高
 */
public interface NoticeService {

    /**
     * 通过ID删除对象
     * 
     * @param idList ID列表
     * @return true：成功，false：失败
     */
    boolean delete(List<Integer> idList);

    /**
     * 通过ID获取对象
     * 
     * @param id ID值
     * @return 对象
     */
    Notice get(Integer id);

    /**
     * 插入对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean insert(Notice bean);

    /**
     * 通过参数Map获取对象分页结果集
     *
     * @param page 分页对象
     * @param paraMap 参数Map
     * @return 分页数据结果集
     */
    PageResult<? extends Notice> pageBy(Page page, Map<String, Object> paraMap);

    /**
     * 更新对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean update(Notice bean);
}
