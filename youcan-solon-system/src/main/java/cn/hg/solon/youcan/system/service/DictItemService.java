package cn.hg.solon.youcan.system.service;

import cn.hg.solon.youcan.system.entity.DictItem;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;

import java.util.List;
import java.util.Map;

/**
 * 字典子项服务
 * 
 * @author 胡高
 */
public interface DictItemService {

    /**
     * 检测唯一性
     * 
     * @param bean 对象
     * @return true：唯一，false：不唯一
     */
    boolean checkUnique(DictItem bean);

    /**
     * 通过ID删除对象
     * 
     * @param idList ID列表
     * @return true：成功，false：失败
     */
    boolean delete(List<Integer> idList);

    /**
     * 通过ID获取对象
     * 
     * @param id ID值
     * @return 对象
     */
    DictItem get(Integer id);

    /**
     * 通过类型和值获取对象
     * 
     * @param type 字典类型
     * @param value 字典值
     * @return 对象
     */
    DictItem getByTypeAndValue(String type, String value);

    /**
     * 插入对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean insert(DictItem bean);

    /**
     * 通过类型、状态获取对象列表
     * 
     * @param type 类型值
     * @param status 状态
     * @return 对象列表
     */
    List<? extends DictItem> listByType(String type, String status);

    /**
     * 通过参数Map获取对象分页结果集
     *
     * @param page 分页对象
     * @param paraMap 参数Map
     * @return 分页数据结果集
     */
    PageResult<? extends DictItem> pageBy(Page page, Map<String, Object> paraMap);

    /**
     * 更新对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean update(DictItem bean);

    /**
     * 更新类型名
     * 
     * @param oldType 旧类型名
     * @param newType 新类型名
     * @return true：成功，false：失败
     */
    boolean updateTypeName(String oldType, String newType);
}
