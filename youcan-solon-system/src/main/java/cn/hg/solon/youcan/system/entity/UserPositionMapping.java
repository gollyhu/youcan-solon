package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;

/**
 * 用户岗位映射
 *
 * @author 胡高
 */
public class UserPositionMapping implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3708506616427387053L;

    /**
     * ID
     */
    private Integer id;

    /**
     * 用户ID，关联sys_user表的ID
     */
    private Integer userId;

    /**
     * 岗位ID，关联sys_position表的ID
     */
    private Integer positionId;

    public Integer getId() {
        return this.id;
    }

    public Integer getPositionId() {
        return this.positionId;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
