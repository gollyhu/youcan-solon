package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;
import java.util.Date;

import org.dromara.hutool.core.regex.RegexPool;
import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.Max;
import org.noear.solon.validation.annotation.Min;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;
import org.noear.solon.validation.annotation.Pattern;

import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.common.validate.UpdateLabel;

/**
 * 公司部门
 *
 * @author 胡高
 */
public class Dept implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8599497010892569076L;

    /**
     * ID
     */
    @NotNull(groups = UpdateLabel.class, message = "必须输入ID")
    private Integer id;

    /**
     * 父部门ID
     */
    private Integer parentId;

    /**
     * 祖级ID列表
     */
    private String ancestors;

    /**
     * 部门名
     */
    @NotEmpty(message = "必须输入部门名")
    @Length(min = 2, max = 20, message = "部门名必须 2 到 20 字符")
    private String name;

    /**
     * 显示排序
     */
    @NotNull
    @Min(value = 0, message = "排序值必须为 0 到 32767 之间")
    @Max(value = Short.MAX_VALUE, message = "排序值必须为 0 到 32767 之间")
    private Integer sort;

    /**
     * 负责人
     */
    @Length(min = 2, max = 20, message = "负责人必须 2 到 20 字符")
    private String leader;

    /**
     * 联系电话
     */
    @Pattern(value = RegexPool.MOBILE, message = "手机号码不合法")
    private String phone;

    /**
     * 邮箱
     */
    @Pattern(value = RegexPool.EMAIL, message = "电子邮箱不合法")
    private String email;

    /**
     * 状态（ON 正常, OFF 停用）
     */
    private String status = BeanStatus.ON.name();

    /**
     * 删除标志（true 是，false 否）
     */
    private Boolean isDel = Boolean.FALSE;

    /**
     * 创建人
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 编辑人
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    public String getAncestors() {
        return this.ancestors;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public Integer getCreator() {
        return this.creator;
    }

    public Boolean getIsDel() {
        return this.isDel;
    }

    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    public Integer getEditor() {
        return this.editor;
    }

    public String getEmail() {
        return this.email;
    }

    public Integer getId() {
        return this.id;
    }

    public String getLeader() {
        return this.leader;
    }

    public String getName() {
        return this.name;
    }

    public Integer getParentId() {
        return this.parentId;
    }

    public String getPhone() {
        return this.phone;
    }

    public Integer getSort() {
        return this.sort;
    }

    public String getStatus() {
        return this.status;
    }

    public void setAncestors(String ancestors) {
        this.ancestors = ancestors;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
