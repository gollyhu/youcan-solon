package cn.hg.solon.youcan.system.service;

import cn.hg.solon.youcan.system.entity.Dict;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;

import java.util.List;
import java.util.Map;

/**
 * 字典服务
 * 
 * @author 胡高
 */
public interface DictService {

    /**
     * 检测唯一性
     * 
     * @param bean 对象
     * @return true：唯一，false：不唯一
     */
    boolean checkUnique(Dict bean);

    /**
     * 通过ID删除对象
     * 
     * @param idList ID列表
     * @return true：成功，false：失败
     */
    boolean delete(List<Integer> idList);

    /**
     * 通过ID获取对象
     * 
     * @param id ID值
     * @return 对象
     */
    Dict get(Integer id);

    /**
     * 通过类型获取对象
     * 
     * @param type 类型值
     * @return 对象
     */
    Dict getByType(String type);

    /**
     * 插入对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean insert(Dict bean);

    /**
     * 通过状态获取对象列表
     * 
     * @param status 状态值
     * @return 对象列表
     */
    List<? extends Dict> listByStatus(String status);

    /**
     * 通过参数Map获取对象列表
     *
     * @param paraMap 参数Map
     * @return 对象列表
     */
    List<? extends Dict> listBy(Map<String, Object> paraMap);

    /**
     * 通过参数Map获取对象分页结果集
     *
     * @param page 分页对象
     * @param paraMap 参数Map
     * @return 分页数据结果集
     */
    PageResult<? extends Dict> pageBy(Page page, Map<String, Object> paraMap);

    /**
     * 更新对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean update(Dict bean);
}
