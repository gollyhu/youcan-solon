package cn.hg.solon.youcan.system.service;

import java.util.List;
import java.util.Map;

import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;

import cn.hg.solon.youcan.system.entity.User;

/**
 * 系统用户服务
 * 
 * @author 胡高
 */
public interface UserService {

    /**
     * 分配角色
     * 
     * @param user 用户对象
     * @param roleIdList 角色ID列表
     */
    void assignRole(User user, List<Integer> roleIdList);

    /**
     * 检测唯一性
     * 
     * @param bean 对象
     * @return true：唯一，false：不唯一
     */
    boolean checkUnique(User bean);

    /**
     * 通过ID删除对象
     * 
     * @param idList ID列表
     * @return true：成功，false：失败
     */
    boolean delete(List<Integer> idList);

    /**
     * 通过ID获取对象
     * 
     * @param userId ID值
     * @return 对象
     */
    User get(Integer userId);

    /**
     * 通过账号获取对象
     * 
     * @param account 账号值
     * @return 对象
     */
    User getByAccount(String account);

    /**
     * 插入对象
     * 
     * @param bean 对象
     * @param roleIdList 角色ID列表
     * @return true：成功，false：失败
     */
    boolean insert(User bean, List<Integer> roleIdList);

    /**
     * 通过参数Map获取对象列表
     * 
     * @param paraMap 参数Map
     * @return 对象列表
     */
    List<? extends User> listBy(Map<String, Object> paraMap);

    /**
     * 通过参数Map获取对象分页结果集
     *
     * @param page 分页对象
     * @param paraMap 参数Map
     * @return 分页数据结果集
     */
    PageResult<? extends User> pageBy(Page page, Map<String, Object> paraMap);

    /**
     * 重置密码
     * 
     * @param bean 用户对象
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     * @param editor 更新人
     */
    void resetPassword(User bean, String oldPassword, String newPassword, User editor);

    /**
     * 重置密码
     * 
     * @param bean 用户对象
     * @param password 密码
     * @param editor 更新人
     */
    void resetPassword(User bean, String password, User editor);

    /**
     * 更新对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean update(User bean);

}
