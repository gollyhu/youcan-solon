package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;
import java.util.Date;

import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;

import cn.hg.solon.youcan.common.validate.UpdateLabel;

/**
 * 参数配置
 * 
 * @author 胡高
 */
public class Config implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8599236735637174643L;

    /**
     * ID
     */
    @NotNull(groups = UpdateLabel.class, message = "必须输入ID")
    private Integer id;

    /**
     * 参数名称
     */
    @NotEmpty(message = "必须输入参数名称")
    @Length(min = 2, max = 20, message = "参数名称必须 2 到 20 字符")
    private String name;

    /**
     * 参数代码
     */
    @NotEmpty(message = "必须输入参数代码")
    @Length(min = 2, max = 30, message = "参数代码必须 2 到 30 字符")
    private String code;

    /**
     * 参数值
     */
    @NotEmpty(message = "必须输入参数值")
    private String value;

    /**
     * 参数类型（SITE 站点，COMPANY 公司，CONFIG 参数。。。类型也可以自己定义）
     */
    @NotEmpty(message = "必须输入参数类型")
    @Length(min = 2, max = 20, message = "参数类型必须 2 到 20 字符")
    private String type;

    /**
     * 系统内置（true 是，false 否），内置参数不允许删除
     */
    @NotNull
    private Boolean isSys = Boolean.FALSE;

    /**
     * 删除标志（true 是，false 否）
     */
    @NotNull
    private Boolean isDel = Boolean.FALSE;

    /**
     * 创建者
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 更新者
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    /**
     * 备注
     */
    @Length(min = 0, max = 500, message = "备注必须 500 字符以内")
    private String remark;

    public String getCode() {
        return this.code;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public Integer getCreator() {
        return this.creator;
    }

    public Boolean getIsDel() {
        return this.isDel;
    }

    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    public Integer getEditor() {
        return this.editor;
    }

    public Integer getId() {
        return this.id;
    }

    public Boolean getIsSys() {
        return this.isSys;
    }

    public String getName() {
        return this.name;
    }

    public String getRemark() {
        return this.remark;
    }

    public String getType() {
        return this.type;
    }

    public String getValue() {
        return this.value;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setIsSys(Boolean isSys) {
        this.isSys = isSys;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
