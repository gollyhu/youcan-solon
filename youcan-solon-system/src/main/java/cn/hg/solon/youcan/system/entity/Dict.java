package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;
import java.util.Date;

import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;

import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.common.validate.UpdateLabel;

/**
 * 字典
 *
 * @author 胡高
 */
public class Dict implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3512666092963675196L;

    /**
     * ID
     */
    @NotNull(groups = UpdateLabel.class, message = "必须输入ID")
    private Integer id;

    /**
     * 字典名称
     */
    @NotEmpty
    @Length(min = 2, max = 20, message = "字典名称必须 2 到 20 字符")
    private String name;

    /**
     * 字典类型
     */
    @NotEmpty(message = "必须输入字典类型")
    @Length(min = 2, max = 30, message = "字典类型必须 2 到 30 字符")
    private String type;

    /**
     * 状态（ON 正常, OFF 停用）
     */
    private String status = BeanStatus.ON.name();

    /**
     * 删除标志（true 是，false 否）
     */
    private Boolean isDel = Boolean.FALSE;

    /**
     * 创建者
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 更新者
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    /**
     * 备注
     */
    @Length(min = 0, max = 500, message = "备注必须 500 字符以内")
    private String remark;

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public Integer getCreator() {
        return this.creator;
    }

    public Boolean getIsDel() {
        return this.isDel;
    }

    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    public Integer getEditor() {
        return this.editor;
    }

    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getRemark() {
        return this.remark;
    }

    public String getStatus() {
        return this.status;
    }

    public String getType() {
        return this.type;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setType(String type) {
        this.type = type;
    }

}
