package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;
import java.util.Date;

import org.dromara.hutool.core.regex.RegexPool;
import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;
import org.noear.solon.validation.annotation.Pattern;

import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.common.validate.UpdateLabel;

/**
 * 系统用户
 *
 * @author 胡高
 */
public class User implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3195549434753942610L;

    /**
     * ID
     */
    @NotNull(groups = UpdateLabel.class, message = "必须输入ID")
    private Integer id;

    /**
     * 部门ID
     */
    @NotNull(message = "必须设定部门")
    private Integer deptId;

    /**
     * 账号
     */
    @NotEmpty(message = "必须输入登录账号")
    @Length(min = 2, max = 20, message = "登录账号必须 2 到 20 字符")
    private String account;

    /**
     * 昵称
     */
    @NotEmpty(message = "必须输入用户昵称")
    @Length(min = 2, max = 20, message = "用户昵称必须 2 到 20 字符")
    private String nickname;

    /**
     * 用户类型（SYSTEM 系统用户，REGISTE注册用户）
     */
    @NotEmpty(message = "必须输入用户类型")
    @Length(min = 2, max = 20, message = "用户类型长度必须 2 到 20 字符")
    private String type = "SYSTEM";

    /**
     * 用户邮箱
     */
    @Pattern(value = RegexPool.EMAIL, message = "电子邮箱不合法")
    private String email;

    /**
     * 手机号码
     */
    @Pattern(value = RegexPool.MOBILE, message = "手机号码不合法")
    private String phone;

    /**
     * 用户性别（M 男，F 女，U 未知）
     */
    @NotEmpty(message = "必须输入用户性别")
    @Length(min = 1, max = 1)
    private String gender = "M";

    /**
     * 头像路径
     */
    @Length(min = 0, max = 1024, message = "头像路径必须小于 1024 字符")
    private String avatar;

    /**
     * 密码
     */
    @Length(min = 6, max = 20, message = "密码长度必须 6 到 20 字符")
    private String password;

    /**
     * 加密盐
     */
    @Length(min = 6, max = 20, message = "加密盐长度不少于 6 字符")
    private String salt;

    /**
     * 帐号状态（ON 正常，OFF 停用）
     */
    @NotEmpty(message = "必须输入状态")
    private String status = BeanStatus.ON.name();

    /**
     * 删除标志（true 是，false 否）
     */
    @NotNull
    private Boolean isDel = Boolean.FALSE;

    /**
     * 最后登录IP
     */
    private String loginIp;

    /**
     * 最后登录时间
     */
    private Date loginDatetime;

    /**
     * 创建者
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 更新者
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    /**
     * 备注
     */
    @Length(min = 0, max = 500, message = "备注必须 500 字符以内")
    private String remark;

    public String getAccount() {
        return this.account;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public Integer getCreator() {
        return this.creator;
    }

    public Boolean getIsDel() {
        return this.isDel;
    }

    public Integer getDeptId() {
        return this.deptId;
    }

    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    public Integer getEditor() {
        return this.editor;
    }

    public String getEmail() {
        return this.email;
    }

    public String getGender() {
        return this.gender;
    }

    public Integer getId() {
        return this.id;
    }

    public Date getLoginDatetime() {
        return this.loginDatetime;
    }

    public String getLoginIp() {
        return this.loginIp;
    }

    public String getNickname() {
        return this.nickname;
    }

    public String getPassword() {
        return this.password;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getRemark() {
        return this.remark;
    }

    public String getSalt() {
        return this.salt;
    }

    public String getStatus() {
        return this.status;
    }

    public String getType() {
        return this.type;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLoginDatetime(Date loginDatetime) {
        this.loginDatetime = loginDatetime;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setType(String type) {
        this.type = type;
    }

}
