package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;
import java.util.Date;

import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.NotNull;

import cn.hg.solon.youcan.common.enums.OnlineStatus;
import cn.hg.solon.youcan.common.validate.UpdateLabel;

/**
 * 用户在线
 *
 * @author 胡高
 */
public class UserOnline implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 5375529122893893417L;

    /**
     * 用户会话id
     */
    @NotNull(groups = UpdateLabel.class, message = "必须输入ID")
    private String id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 用户昵称
     */
    @Length(min = 2, max = 20, message = "用户昵称必须 2 到 20 字符")
    private String nickname;

    /**
     * 部门名称
     */
    @Length(min = 2, max = 20, message = "部门名必须 2 到 20 字符")
    private String deptName;

    /**
     * 登录设备
     */
    @Length(min = 2, max = 20, message = "登录设备必须 2 到 20 字符")
    private String device;

    /**
     * 登录IP地址
     */
    private String ip;

    /**
     * 登录地点
     */
    private String location;

    /**
     * 浏览器类型
     */
    private String browser;

    /**
     * 操作系统
     */
    private String os;

    /**
     * ON：在线，OFF：离线
     */
    private String status = OnlineStatus.ON.name();

    /**
     * session创建时间
     */
    private Date startDatetime;

    /**
     * session最后访问时间
     */
    private Date activityDatetime;

    /**
     * 超时时间(秒)
     */
    private Integer expireSecond;

    public Date getActivityDatetime() {
        return this.activityDatetime;
    }

    public String getBrowser() {
        return this.browser;
    }

    public String getDeptName() {
        return this.deptName;
    }

    public String getDevice() {
        return this.device;
    }

    public Integer getExpireSecond() {
        return this.expireSecond;
    }

    public String getId() {
        return this.id;
    }

    public String getIp() {
        return this.ip;
    }

    public String getLocation() {
        return this.location;
    }

    public String getNickname() {
        return this.nickname;
    }

    public String getOs() {
        return this.os;
    }

    public Date getStartDatetime() {
        return this.startDatetime;
    }

    public String getStatus() {
        return this.status;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setActivityDatetime(Date activityDatetime) {
        this.activityDatetime = activityDatetime;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public void setExpireSecond(Integer expireSecond) {
        this.expireSecond = expireSecond;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public void setStartDatetime(Date startDatetime) {
        this.startDatetime = startDatetime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
