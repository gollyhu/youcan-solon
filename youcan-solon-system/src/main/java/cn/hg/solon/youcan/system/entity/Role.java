package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;
import java.util.Date;

import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.Max;
import org.noear.solon.validation.annotation.Min;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;

import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.common.validate.UpdateLabel;

/**
 * 系统角色
 *
 * @author 胡高
 */
public class Role implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3018411126027384403L;

    /**
     * ID
     */
    @NotNull(groups = UpdateLabel.class, message = "必须输入ID")
    private Integer id;

    /**
     * 角色名称
     */
    @NotEmpty(message = "必须输入角色名称")
    @Length(min = 2, max = 20, message = "角色名称必须 2 到 20 字符")
    private String name;

    /**
     * 权限字符串
     */
    @NotEmpty(message = "必须输入权限字符")
    @Length(min = 2, max = 30, message = "权限字符必须 2 到 30 字符")
    private String code;


    @NotNull
    @Min(value = 0, message = "排序值必须为 0 到 32767 之间")
    @Max(value = Short.MAX_VALUE, message = "排序值必须为 0 到 32767 之间")
    private Integer sort;

    /**
     * 数据范围（ALL：全部数据权限 CUSTOM：自定数据权限 DEPARTMENT：本部门数据权限 BELOW：本部门及以下数据权限）
     */
    //@NotEmpty(message = "必须输入数据范围")
    @Length(min = 2, max = 20, message = "数据范围必须 2 到 20 字符")
    private String dataScope = "ALL";

    /**
     * 帐号状态（ON 正常，OFF 停用）
     */
    private String status = BeanStatus.ON.name();

    /**
     * 删除标志（true 是，false 否）
     */
    private Boolean isDel = Boolean.FALSE;

    /**
     * 创建者
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 更新者
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    /**
     * 备注
     */
    @Length(min = 0, max = 500, message = "备注必须 500 字符以内")
    private String remark;

    public String getCode() {
        return this.code;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public Integer getCreator() {
        return this.creator;
    }

    public String getDataScope() {
        return this.dataScope;
    }

    public Boolean getIsDel() {
        return this.isDel;
    }

    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    public Integer getEditor() {
        return this.editor;
    }

    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getRemark() {
        return this.remark;
    }

    public Integer getSort() {
        return this.sort;
    }

    public String getStatus() {
        return this.status;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public void setDataScope(String dataScope) {
        this.dataScope = dataScope;
    }

    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
