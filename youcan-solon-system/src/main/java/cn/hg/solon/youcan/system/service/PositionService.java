package cn.hg.solon.youcan.system.service;

import cn.hg.solon.youcan.system.entity.Position;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;

import java.util.List;
import java.util.Map;

/**
 * 系统岗位服务
 * 
 * @author 胡高
 */
public interface PositionService {

    /**
     * 检测唯一性
     * 
     * @param bean 对象
     * @return true：唯一，false：不唯一
     */
    boolean checkUnique(Position bean);

    /**
     * 通过ID删除对象
     * 
     * @param idList ID列表
     * @return true：成功，false：失败
     */
    boolean delete(List<Integer> idList);

    /**
     * 通过ID获取对象
     * 
     * @param id ID值
     * @return 对象
     */
    Position get(Integer id);

    /**
     * 插入对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean insert(Position bean);

    /**
     * 通过参数Map获取对象分页结果集
     *
     * @param page 分页对象
     * @param paraMap 参数Map
     * @return 分页数据结果集
     */
    PageResult<? extends Position> pageBy(Page page, Map<String, Object> paraMap);

    /**
     * 更新对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean update(Position bean);
}
