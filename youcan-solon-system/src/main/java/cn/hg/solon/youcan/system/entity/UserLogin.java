package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;
import java.util.Date;

import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.NotNull;

import cn.hg.solon.youcan.common.enums.BusinessStatus;
import cn.hg.solon.youcan.common.validate.UpdateLabel;

/**
 * 用户登录
 *
 * @author 胡高
 */
public class UserLogin implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7689677607150087442L;

    /**
     * ID
     */
    @NotNull(groups = UpdateLabel.class, message = "必须输入ID")
    private Integer id;

    /**
     * 账号
     */
    @Length(min = 2, max = 20, message = "登录账号必须 2 到 20 字符")
    private String account;

    /**
     * 昵称
     */
    @Length(min = 2, max = 20, message = "用户昵称必须 2 到 20 字符")
    private String nickname;

    /**
     * 登录IP
     */
    private String ip;

    /**
     * 登录地点
     */
    private String location;

    /**
     * 浏览器类型
     */
    private String browser;

    /**
     * 操作系统
     */
    private String os;

    /**
     * 任务状态：SUCCESS 成功，FAIL 失败
     */
    private String status = BusinessStatus.SUCCESS.name();

    /**
     * 提示消息
     */
    private String msg;

    /**
     * 访问时间
     */
    private Date loginDatetime;

    public String getAccount() {
        return this.account;
    }

    public String getBrowser() {
        return this.browser;
    }

    public Integer getId() {
        return this.id;
    }

    public String getIp() {
        return this.ip;
    }

    public String getLocation() {
        return this.location;
    }

    public Date getLoginDatetime() {
        return this.loginDatetime;
    }

    public String getMsg() {
        return this.msg;
    }

    public String getNickname() {
        return this.nickname;
    }

    public String getOs() {
        return this.os;
    }

    public String getStatus() {
        return this.status;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setLoginDatetime(Date loginDatetime) {
        this.loginDatetime = loginDatetime;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
