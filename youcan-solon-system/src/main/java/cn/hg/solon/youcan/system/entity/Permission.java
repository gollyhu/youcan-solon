package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;
import java.util.Date;

import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.Max;
import org.noear.solon.validation.annotation.Min;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;

import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.common.validate.UpdateLabel;

/**
 * 系统权限
 *
 * @author 胡高
 */
public class Permission implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3873803246308505469L;

    /**
     * ID
     */
    @NotNull(groups = UpdateLabel.class, message = "必须输入ID")
    private Integer id;

    /**
     * 权限名称
     */
    @NotEmpty(message = "必须输入权限名称")
    @Length(min = 2, max = 50, message = "权限名称必须 2 到 50 字符")
    private String name;

    /**
     * 父菜单ID
     */
    private Integer parentId;

    /**
     * 祖级ID列表
     */
    private String ancestors;

    /**
     * 显示顺序
     */
    @NotNull
    @Min(value = 0, message = "排序值必须为 0 到 32767 之间")
    @Max(value = Short.MAX_VALUE, message = "排序值必须为 0 到 32767 之间")
    private Integer sort;

    /**
     * 请求地址
     */
    private String url;

    /**
     * 打开方式（_iframe 正常打开，_blank 新建窗口）
     */
    private String target;

    /**
     * 类型（NODE 节点，MENU 菜单，FUNCTION 功能按钮）
     */
    @NotEmpty(message = "必须输入类型")
    @Length(min = 2, max = 10, message = "类型必须 2 到 10 字符")
    private String type;

    /**
     * 状态（ON 正常，OFF 停用）
     */
    private String status = BeanStatus.ON.name();

    /**
     * 删除标志（true 是，false 否）
     */
    private Boolean isDel = Boolean.FALSE;

    /**
     * 请求方法
     */
    private String method;

    /**
     * 权限值
     */
    private String value;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 页面Class
     */
    private String cssClass;

    /**
     * 创建人
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 编辑人
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    public String getAncestors() {
        return this.ancestors;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public Integer getCreator() {
        return this.creator;
    }

    public String getCssClass() {
        return this.cssClass;
    }

    public Boolean getIsDel() {
        return this.isDel;
    }

    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    public Integer getEditor() {
        return this.editor;
    }

    public String getIcon() {
        return this.icon;
    }

    public Integer getId() {
        return this.id;
    }

    public String getMethod() {
        return this.method;
    }

    public String getName() {
        return this.name;
    }

    public Integer getParentId() {
        return this.parentId;
    }

    public Integer getSort() {
        return this.sort;
    }

    public String getStatus() {
        return this.status;
    }

    public String getTarget() {
        return this.target;
    }

    public String getType() {
        return this.type;
    }

    public String getUrl() {
        return this.url;
    }

    public String getValue() {
        return this.value;
    }

    public void setAncestors(String ancestors) {
        this.ancestors = ancestors;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
