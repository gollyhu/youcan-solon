package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;

/**
 * 角色部门映射
 *
 * @author 胡高
 */
public class RoleDeptMapping implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -616131333096526671L;

    /**
     * ID
     */
    private Integer id;

    /**
     * 角色ID，关联sys_role表的ID
     */
    private Integer roleId;

    /**
     * 部门ID，关联sys_dept表的ID
     */
    private Integer deptId;

    public Integer getDeptId() {
        return this.deptId;
    }

    public Integer getId() {
        return this.id;
    }

    public Integer getRoleId() {
        return this.roleId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

}
