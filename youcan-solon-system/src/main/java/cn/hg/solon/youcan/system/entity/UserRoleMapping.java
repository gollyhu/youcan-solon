package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;

/**
 * 用户角色映射
 *
 * @author 胡高
 */
public class UserRoleMapping implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 30204785822397667L;

    /**
     * ID
     */
    private Integer id;

    /**
     * 用户ID，关联sys_user表的ID
     */
    private Integer userId;

    /**
     * 角色ID，关联sys_role表的ID
     */
    private Integer roleId;

    public Integer getId() {
        return this.id;
    }

    public Integer getRoleId() {
        return this.roleId;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
