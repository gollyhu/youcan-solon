package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;
import java.util.Date;

import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.Max;
import org.noear.solon.validation.annotation.Min;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;

import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.common.validate.UpdateLabel;

/**
 * 字典子项
 *
 * @author 胡高
 */
public class DictItem implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -2762946702495546324L;

    /**
     * 字典编码
     */
    @NotNull(groups = UpdateLabel.class, message = "必须输入ID")
    private Integer id;

    /**
     * 显示排序
     */
    @NotNull
    @Min(value = 0, message = "排序值必须为 0 到 32767 之间")
    @Max(value = Short.MAX_VALUE, message = "排序值必须为 0 到 32767 之间")
    private Integer sort;

    /**
     * 字典标签
     */
    @NotEmpty(message = "必须输入字典标签")
    @Length(min = 2, max = 20, message = "字典标签必须 2 到 20 字符")
    private String label;

    /**
     * 字典键值
     */
    @NotEmpty(message = "必须输入字典键值")
    @Length(min = 2, max = 100, message = "字典键值必须 2 到 100 字符")
    private String value;

    /**
     * 字典类型
     */
    @NotEmpty(message = "必须输入字典类型")
    @Length(min = 2, max = 30, message = "字典类型必须 2 到 30 字符")
    private String type;

    /**
     * 备注
     */
    @Length(min = 0, max = 500, message = "备注必须 500 字符以内")
    private String remark;

    /**
     * 样式属性（其他样式扩展）
     */
    @Length(min = 0, max = 100, message = "样式属性必须 0 到 100 字符")
    private String cssClass;

    /**
     * 表格回显样式
     */
    @Length(min = 0, max = 100, message = "回显样式必须 0 到 100 字符")
    private String listClass;

    /**
     * 是否默认（0 否，1 是）
     */
    private Boolean isDefault = Boolean.FALSE;

    /**
     * 状态（ON 正常, OFF 停用,）
     */
    private String status = BeanStatus.ON.name();

    /**
     * 删除标志（true 是，false 否）
     */
    private Boolean isDel = Boolean.FALSE;

    /**
     * 创建人
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 编辑人
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public Integer getCreator() {
        return this.creator;
    }

    public String getCssClass() {
        return this.cssClass;
    }

    public Boolean getIsDel() {
        return this.isDel;
    }

    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    public Integer getEditor() {
        return this.editor;
    }

    public Integer getId() {
        return this.id;
    }

    public Boolean getIsDefault() {
        return this.isDefault;
    }

    public String getLabel() {
        return this.label;
    }

    public String getListClass() {
        return this.listClass;
    }

    public String getRemark() {
        return this.remark;
    }

    public Integer getSort() {
        return this.sort;
    }

    public String getStatus() {
        return this.status;
    }

    public String getType() {
        return this.type;
    }

    public String getValue() {
        return this.value;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setListClass(String listClass) {
        this.listClass = listClass;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
