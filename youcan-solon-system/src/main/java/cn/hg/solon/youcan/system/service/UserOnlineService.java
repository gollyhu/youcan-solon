package cn.hg.solon.youcan.system.service;

import cn.hg.solon.youcan.system.entity.UserOnline;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 在线用户服务
 * 
 * @author 胡高
 */
public interface UserOnlineService {

    /**
     * 通过token获取对象
     * 
     * @param token token值
     * @return 对象
     */
    UserOnline get(String token);

    /**
     * 插入对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean insert(UserOnline bean);

    /**
     * 通过token强制下线用户
     * 
     * @param token token值
     * @return 对象
     */
    boolean offline(String token);

    /**
     * 获取在线用户列表
     * 
     * @return 在线用户列表
     */
    List<? extends UserOnline> onlineList();

    /**
     * 通过参数Map获取对象分页结果集
     *
     * @param page 分页对象
     * @param paraMap 参数Map
     * @return 分页数据结果集
     */
    PageResult<? extends UserOnline> pageBy(Page page, Map<String, Object> paraMap);

    /**
     * 更新最后访问时间
     * 
     * @param token token值
     * @param datetime 时间值
     */
    void syncActivityTime(String token, Date datetime);

    /**
     * 更新对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean update(UserOnline bean);
}
