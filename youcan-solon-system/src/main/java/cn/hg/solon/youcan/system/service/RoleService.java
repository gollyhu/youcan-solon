package cn.hg.solon.youcan.system.service;

import cn.hg.solon.youcan.system.entity.Role;
import cn.hg.solon.youcan.system.entity.User;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;

import java.util.List;
import java.util.Map;

/**
 * 系统角色服务
 * 
 * @author 胡高
 */
public interface RoleService {
    /**
     * 分配数据权限
     * 
     * @param bean 角色对象
     * @param dataScope 数据范围值
     * @param deptIdList 部门ID列表
     */
    void assignDataScope(Role bean, String dataScope, List<Integer> deptIdList);

    /**
     * 分配权限
     * 
     * @param role 角色对象
     * @param permissionIdList 权限ID列表
     */
    void assignPermission(Role role, List<Integer> permissionIdList);

    /**
     * 检测唯一性
     * 
     * @param bean 对象
     * @return true：唯一，false：不唯一
     */
    boolean checkUnique(Role bean);

    /**
     * 通过ID删除对象
     * 
     * @param idList ID列表
     * @return true：成功，false：失败
     */
    boolean delete(List<Integer> idList);

    /**
     * 通过ID获取对象
     * 
     * @param id ID值
     * @return 对象
     */
    Role get(Integer id);

    /**
     * 通过代码获取对象
     * 
     * @param code 代码值
     * @return 对象
     */
    Role getByCode(String code);

    /**
     * 插入对象
     * 
     * @param bean 对象
     * @param deptIdList 部门ID列表
     * @param permissionIdList 权限ID列表
     * @return true：成功，false：失败
     */
    boolean insert(Role bean, List<Integer> deptIdList, List<Integer> permissionIdList);

    /**
     * 通过参数Map获取对象列表
     * 
     * @param paraMap 参数Map
     * @return 对象列表
     */
    List<? extends Role> listBy(Map<String, Object> paraMap);

    /**
     * 通过状态获取对象列表
     * 
     * @param status 状态值
     * @return 对象列表
     */
    List<? extends Role> listByStatus(String status);

    /**
     * 通过用户获取对象列表
     * 
     * @param bean 用户对象
     * @return 对象列表
     */
    List<? extends Role> listByUser(User bean);

    /**
     * 通过参数Map获取对象分页结果集
     *
     * @param page 分页对象
     * @param paraMap 参数Map
     * @return 分页数据结果集
     */
    PageResult<? extends Role> pageBy(Page page, Map<String, Object> paraMap);

    /**
     * 更新对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean update(Role bean);
}
