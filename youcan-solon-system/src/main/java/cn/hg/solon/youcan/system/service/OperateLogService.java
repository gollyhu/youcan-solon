package cn.hg.solon.youcan.system.service;

import cn.hg.solon.youcan.system.entity.OperateLog;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;

import java.util.List;
import java.util.Map;

/**
 * 操作日志服务
 * 
 * @author 胡高
 */
public interface OperateLogService {

    /**
     * 清空对象
     * 
     * @return true：成功，false：失败
     */
    boolean clear();

    /**
     * 通过ID删除对象
     * 
     * @param idList ID列表
     * @return true：成功，false：失败
     */
    boolean delete(List<Integer> idList);

    /**
     * 通过ID获取对象
     * 
     * @param id ID值
     * @return 对象
     */
    OperateLog get(Integer id);

    /**
     * 插入对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean insert(OperateLog bean);

    /**
     * 通过参数Map获取对象分页结果集
     *
     * @param page 分页对象
     * @param paraMap 参数Map
     * @return 分页数据结果集
     */
    PageResult<? extends OperateLog> pageBy(Page page, Map<String, Object> paraMap);
}
