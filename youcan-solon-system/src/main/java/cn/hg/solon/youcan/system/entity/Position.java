package cn.hg.solon.youcan.system.entity;

import java.io.Serializable;
import java.util.Date;

import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.Max;
import org.noear.solon.validation.annotation.Min;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;

import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.common.validate.UpdateLabel;

/**
 * 公司岗位
 *
 * @author 胡高
 */
public class Position implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8734829937866344315L;

    /**
     * ID
     */
    @NotNull(groups = UpdateLabel.class, message = "必须输入ID")
    private Integer id;

    /**
     * 岗位编码
     */
    @NotEmpty(message = "必须输入岗位编码")
    @Length(min = 2, max = 64, message = "岗位编码必须 2 到 64 字符")
    private String code;

    /**
     * 岗位名称
     */
    @NotEmpty(message = "必须输入岗位名称")
    @Length(min = 2, max = 50, message = "岗位名称必须 2 到 50 字符")
    private String name;

    /**
     * 显示排序
     */
    @NotNull
    @Min(value = 0, message = "排序值必须为 0 到 32767 之间")
    @Max(value = Short.MAX_VALUE, message = "排序值必须为 0 到 32767 之间")
    private Integer sort;

    /**
     * 状态（ON 正常, OFF 停用）
     */
    private String status = BeanStatus.ON.name();

    /**
     * 删除标志（true 是，false 否）
     */
    private Boolean isDel = Boolean.FALSE;

    /**
     * 创建人
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 编辑人
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    public String getCode() {
        return this.code;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public Integer getCreator() {
        return this.creator;
    }

    public Boolean getIsDel() {
        return this.isDel;
    }

    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    public Integer getEditor() {
        return this.editor;
    }

    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Integer getSort() {
        return this.sort;
    }

    public String getStatus() {
        return this.status;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
