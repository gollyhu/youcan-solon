package cn.hg.solon.youcan.system.service;

import java.util.List;
import java.util.Map;

import cn.hg.solon.youcan.system.entity.Dept;
import cn.hg.solon.youcan.system.entity.Role;
import org.dromara.hutool.core.tree.MapTree;

/**
 * 行政部门服务
 * 
 * @author 胡高
 */
public interface DeptService {

    /**
     * 检测唯一性
     * 
     * @param bean 对象
     * @return true：唯一，false：不唯一
     */
    boolean checkUnique(Dept bean);

    /**
     * 通过ID删除
     * 
     * @param id ID
     * @return true：成功，false：失败
     */
    boolean delete(Integer id);

    /**
     * 通过ID获取对象
     * 
     * @param id ID值
     * @return 对象
     */
    Dept get(Integer id);

    /**
     * 插入对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean insert(Dept bean);

    /**
     * 通过参数Map获取对象列表
     * 
     * @param paraMap 参数Map
     * @return 对象列表
     */
    List<? extends Dept> listBy(Map<String, Object> paraMap);

    /**
     * 通过角色获取对象列表
     * 
     * @param bean 角色对象
     * @return 对象列表
     */
    List<? extends Dept> listByRole(Role bean);

    /**
     * 通过状态对象列表
     * 
     * @param status 状态值
     * @return 对象列表
     */
    List<? extends Dept> listByStatus(String status);

    /**
     * 通过参数Map获取对象树
     *
     * @param paraMap 参数Map
     * @return 对象树，参照Hutool的TreeUtil
     */
    List<MapTree<Integer>> treeBy(Map<String, Object> paraMap);

    /**
     * 更新对象
     * 
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean update(Dept bean);
}
