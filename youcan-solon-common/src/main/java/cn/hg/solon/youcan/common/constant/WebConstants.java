package cn.hg.solon.youcan.common.constant;

/**
 * @author 胡高
 */
public interface WebConstants {

    /**
     * 排序方向（正向）
     */
    public static final String ORDER_DIRECTION_ASC = "ASC";

    /**
     * 404错误页面地址
     */
    public static final String ERROR_PAGE_404 = "/_error/404.html";

    /**
     * 500错误页面地址
     */
    public static final String ERROR_PAGE_500 = "/_error/500.html";

    /**
     * sa-token获取当前用户ID的getter的Bean对象名称
     */
    public static final String USER_ID_GETTER = "USER_GETTER";
}
