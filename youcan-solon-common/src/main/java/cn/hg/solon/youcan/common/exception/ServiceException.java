package cn.hg.solon.youcan.common.exception;

import org.dromara.hutool.http.meta.HttpStatus;
import org.noear.solon.core.handle.Result;

/**
 * @author 胡高
 */
public class ServiceException extends RuntimeException {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 4560718886767512047L;

    /**
     * 状态码
     *
     * 200:成功
     * 400:未知失败
     * 409:明确的失败
     * 500:系统错误
     */
    private int code = HttpStatus.HTTP_CONFLICT;

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(int code, String message) {
        super(message);
        this.code = code;
    }

    public ServiceException(Throwable cause) {
        super(cause);
        this.code = Result.FAILURE_CODE;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
