package cn.hg.solon.youcan.common.constant;

/**
 * @author 胡高
 */
public interface CacheConstants {

    /**
     * 缓存时间：1小时
     */
    public static final int CACHE_SECONDS_ONE_HOUR = 3600;

    /**
     * 缓存时间：1分钟
     */
    public static final int CACHE_SECONDS_ONE_MINUTE = 60;

    /**
     * 缓存时间：1天
     */
    public static final int CACHE_SECONDS_ONE_DAY = 3600 * 24;

    /**
     * 参数管理 cache name
     */
    public static final String SYS_CONFIG_TAG = "SYS_CONFIG";

    /**
     * 系统机构 cache name
     */
    public static final String SYS_DEPT_TAG = "SYS_DEPT";

    /**
     * 字典管理 cache name
     */
    public static final String SYS_DICT_TAG = "SYS_DICT";

    /**
     * 定时任务 cache name
     */
    public static final String SYS_JOB_TAG = "SYS_JOB";

    /**
     * 通知公告 cache name
     */
    public static final String SYS_NOTICE_TAG = "SYS_NOTICE";

    /**
     * 系统权限 cache name
     */
    public static final String SYS_PERMISSION_TAG = "SYS_PERMISSION";

    /**
     * 系统岗位 cache name
     */
    public static final String SYS_POSITION_TAG = "SYS_POSITION";

    /**
     * 系统角色 cache name
     */
    public static final String SYS_ROLE_TAG = "SYS_ROLE";

    /**
     * 系统用户 user tag
     */
    public static final String SYS_USER_TAG = "SYS_USER";

}
