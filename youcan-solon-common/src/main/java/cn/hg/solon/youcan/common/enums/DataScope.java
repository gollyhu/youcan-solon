package cn.hg.solon.youcan.common.enums;

/**
 * @author 胡高
 */
public enum DataScope {
    ALL, //全部数据权限
    CUSTOM, //自定数据权限
    DEPARTMENT, //本部门数据权限
    BELOW //本部门及以下数据权限
}
