package cn.hg.solon.youcan.common.vo;

public interface IPaginationObject extends IQueryObject {

    int getPageNumber();

    int getPageSize();

    String getSortField();

    String getSortType();
}
