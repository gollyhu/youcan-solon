package cn.hg.solon.youcan.common.constant;

import java.util.Date;

import org.dromara.hutool.core.date.DateUtil;

/**
 * @author 胡高
 */
public interface OnlineConstants {

    /**
     * 上次同步数据库的时间戳
     */
    public static final String LAST_SYNC_DB_TIMESTAMP = "LAST_SYNC_DB_TIMESTAMP";

    /**
     * 同步数据到数据库的时间间隔
     */
    public static final long SYNC_PERIOD = CacheConstants.CACHE_SECONDS_ONE_MINUTE * 1000;


    public static final String EXPIRATION_DATETIME = "EXPIRATION_DATETIME";

    public static final Date MAX_DATE = DateUtil.parse("2099-12-31");
}
