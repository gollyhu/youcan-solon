package cn.hg.solon.youcan.common.vo;

import java.io.Serializable;

import org.dromara.hutool.json.JSONUtil;

/**
 * 基础VO对象
 * 
 * @author 胡高
 */
public abstract class BaseVo implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3606531022049546283L;

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return JSONUtil.toJsonStr(this);
    }

}
