package cn.hg.solon.youcan.common.enums;

/**
 * @author 胡高
 */
public enum BeanStatus {
    /**
     * 启用
     */
    ON,

    /**
     * 禁用
     */
    OFF,
}
