package cn.hg.solon.youcan.common.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 胡高
 */
public abstract class BaseController {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

}
