package cn.hg.solon.youcan.common.vo;

import java.util.Map;

public interface IQueryObject {

    String getWord();

    Map<String, Object> toMap();
}
