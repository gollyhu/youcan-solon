package cn.hg.solon.youcan.common.constant;

import org.dromara.hutool.http.meta.HttpStatus;

/**
 * @author 胡高
 */
public interface AppConstants {

    /**
     * 失败消息：fail
     */
    static final String RETURN_MESSAGE_VALUE_FAILED = "fail";

    /**
     * 成功消息：success
     */
    static final String RETURN_MESSAGE_VALUE_SUCCESS = "success";

    /**
     * 默认失败参数值：500
     */
    static final int RETURN_CODE_VALUE_FAIL = HttpStatus.HTTP_INTERNAL_ERROR;

    /**
     * 默认成功参数值：200
     */
    static final int RETURN_CODE_VALUE_SUCCESS = HttpStatus.HTTP_OK;
}
