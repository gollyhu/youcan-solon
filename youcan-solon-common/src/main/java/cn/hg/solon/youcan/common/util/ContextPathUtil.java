package cn.hg.solon.youcan.common.util;

import org.dromara.hutool.core.text.StrUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.http.HttpUtil;
import org.noear.solon.Solon;

/**
 * context-path 工具类
 * @author 胡高
 */
public class ContextPathUtil {

    private static String ctx = null;

    
    /**
     * 获取 context-path
     * @return context-path
     */
    public static String getContextPath() {
        if (ObjUtil.isNotNull(ctx)) {
            return ctx;
        }

        ctx = Solon.cfg().serverContextPath();
        ctx = StrValidator.isBlank(ctx) ? "" : ctx;
        
        // 去掉context-path最后的反斜杠
        if (StrUtil.endWith(ctx, '/')) {
        	ctx = StrUtil.sub(ctx, 0, StrUtil.length(ctx) - 1);
        }

        return ctx;
    }

    public static String prefixContextPath(String url) {

        return (HttpUtil.isHttp(url) || HttpUtil.isHttps(url)) ? url : getContextPath() + url;
    }
}
