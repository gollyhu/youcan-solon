package cn.hg.solon.youcan.common.enums;

/**
 * 用户会话
 * 
 * @author ruoyi
 */
public enum OnlineStatus {
    /** 用户状态 */
    ON, OFF;
}
