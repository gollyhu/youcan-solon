package cn.hg.solon.youcan.job.entity;

import java.util.Date;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

import cn.hg.solon.youcan.common.enums.BusinessStatus;
import cn.hg.solon.youcan.flex.listener.EntityInsertListener;
import cn.hg.solon.youcan.flex.listener.EntityUpdateListener;


@Table(value = "sys_job_log", onInsert = EntityInsertListener.class, onUpdate = EntityUpdateListener.class)
public class SysJobLog extends JobLog {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -5871017911329507024L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Auto)
    private Integer id;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 任务组名
     */
    private String group;

    /**
     * 调用目标字符串
     */
    private String target;

    /**
     * 日志信息
     */
    private String message;

    /**
     * 任务状态：SUCCESS 成功，FAIL 失败
     */
    private String status = BusinessStatus.SUCCESS.name();

    /**
     * 异常信息
     */
    private String exception;

    /**
     * 执行时间（ms）
     */
    private Long cost;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 开始时间
     */
    private Date startedDatetime;

    /**
     * 结束时间
     */
    private Date endedDatetime;

    @Override
    public Long getCost() {
        return this.cost;
    }

    @Override
    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    @Override
    public Date getEndedDatetime() {
        return this.endedDatetime;
    }

    @Override
    public String getException() {
        return this.exception;
    }

    @Override
    public String getGroup() {
        return this.group;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Date getStartedDatetime() {
        return this.startedDatetime;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public String getTarget() {
        return this.target;
    }

    @Override
    public void setCost(Long cost) {
        this.cost = cost;
    }

    @Override
    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    @Override
    public void setEndedDatetime(Date endedDatetime) {
        this.endedDatetime = endedDatetime;
    }

    @Override
    public void setException(String exception) {
        this.exception = exception;
    }

    @Override
    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setStartedDatetime(Date startedDatetime) {
        this.startedDatetime = startedDatetime;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void setTarget(String target) {
        this.target = target;
    }

}
