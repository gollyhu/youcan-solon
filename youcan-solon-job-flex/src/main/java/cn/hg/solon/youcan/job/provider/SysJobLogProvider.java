package cn.hg.solon.youcan.job.provider;

import cn.hg.solon.youcan.flex.util.QueryWrapperUtil;
import cn.hg.solon.youcan.job.entity.JobLog;
import cn.hg.solon.youcan.job.entity.SysJobLog;
import cn.hg.solon.youcan.job.mapper.SysJobLogMapper;
import cn.hg.solon.youcan.job.service.JobLogService;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static cn.hg.solon.youcan.job.entity.table.SysJobLogTableDef.SYS_JOB_LOG;

@Component
public class SysJobLogProvider extends ServiceImpl<SysJobLogMapper, SysJobLog> implements JobLogService {

    @Tran
    @Override
    public boolean clear() {
        return this.getMapper().deleteByCondition(SYS_JOB_LOG.ID.ne(0)) > 0;
    }

    @Tran
    @Override
    public boolean delete(List<Integer> idList) {
        return this.getMapper().deleteBatchByIds(idList) > 0;
    }

    @Override
    public JobLog get(Integer id) {
        return this.getMapper().selectOneById(id);
    }

    @Tran
    @Override
    public boolean insert(JobLog bean) {
        SysJobLog cloneBean = BeanUtil.copyProperties(bean, SysJobLog.class);

        return this.getMapper().insert(cloneBean) > 0;
    }

    @Override
    public PageResult<? extends JobLog> pageBy(org.dromara.hutool.db.Page page, Map<String, Object> paraMap) {
        String word = (String) paraMap.get("word");
        String group = (String) paraMap.get("group");
        Date startDate = (Date) paraMap.get("startDate");
        Date endDate = (Date) paraMap.get("endDate");
        String status = (String) paraMap.get("status");

        QueryWrapper query = QueryWrapper.create()
                .where(SYS_JOB_LOG.GROUP.eq(group).when(StrValidator.isNotBlank(group))
                        .and(SYS_JOB_LOG.STARTED_DATETIME.ge(startDate).when(ObjUtil.isNotNull(startDate)))
                        .and(SYS_JOB_LOG.ENDED_DATETIME.le(endDate).when(ObjUtil.isNotNull(endDate)))
                        .and(SYS_JOB_LOG.STATUS.eq(status).when(StrValidator.isNotBlank(status)))
                        .and(SYS_JOB_LOG.NAME.like(word).when(StrValidator.isNotBlank(word))
                                .or(SYS_JOB_LOG.TARGET.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_JOB_LOG.MESSAGE.like(word).when(StrValidator.isNotBlank(word)))));

        Page<SysJobLog> pageList = this.getMapper().paginate(Page.of(page.getPageNumber(), page.getPageSize()),
                QueryWrapperUtil.applyOrderBy(query, page));

        PageResult<JobLog> result = new PageResult<>();
        result.addAll(pageList.getRecords());
        result.setTotal(ConvertUtil.toInt(pageList.getTotalRow()));

        return result;
    }

}
