package cn.hg.solon.youcan.job.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.mybatisflex.core.BaseMapper;

import cn.hg.solon.youcan.job.entity.SysJobLog;

@Mapper
public interface SysJobLogMapper extends BaseMapper<SysJobLog> {

}
