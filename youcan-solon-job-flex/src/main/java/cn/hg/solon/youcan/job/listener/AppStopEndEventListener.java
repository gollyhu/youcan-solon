package cn.hg.solon.youcan.job.listener;

import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.event.AppPrestopEndEvent;
import org.noear.solon.core.event.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.hg.solon.youcan.job.service.JobService;

/**
 * @author 胡高
 */
@Component
public class AppStopEndEventListener implements EventListener<AppPrestopEndEvent> {
    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    JobService jobService;

    @Override
    public void onEvent(AppPrestopEndEvent event) throws Throwable {
        this.log.info("销毁定时任务。。。");
        this.jobService.destroy();
    }
}
