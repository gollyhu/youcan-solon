package cn.hg.solon.youcan.job.provider;

import static cn.hg.solon.youcan.job.entity.table.SysJobTableDef.SYS_JOB;

import java.util.List;
import java.util.Map;

import cn.hg.solon.youcan.flex.util.QueryWrapperUtil;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.CharSequenceUtil;
import org.dromara.hutool.core.text.StrUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.cron.CronUtil;
import org.dromara.hutool.cron.pattern.CronPattern;
import org.dromara.hutool.cron.task.Task;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.solon.service.impl.ServiceImpl;

import cn.hg.solon.youcan.common.constant.AppConstants;
import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.common.exception.ServiceException;
import cn.hg.solon.youcan.job.constant.JobStatus;
import cn.hg.solon.youcan.job.entity.Job;
import cn.hg.solon.youcan.job.entity.SysJob;
import cn.hg.solon.youcan.job.mapper.SysJobMapper;
import cn.hg.solon.youcan.job.service.JobService;
import cn.hg.solon.youcan.job.utils.AbstractCronJob;
import cn.hg.solon.youcan.job.utils.CronDisallowConcurrentExecution;
import cn.hg.solon.youcan.job.utils.CronJobExecution;
import cn.hg.solon.youcan.job.utils.CronJobUtil;

@Component
public class SysJobProvider extends ServiceImpl<SysJobMapper, SysJob> implements JobService {
    protected Logger log = LoggerFactory.getLogger(this.getClass());

    private boolean checkCron(Job bean) {
        try {
            CronPattern.of(bean.getCron());
            return true;
        } catch (Throwable e) {
            return false;
        }
    }

    @Override
    public boolean checkUnique(Job bean) {
        // 查找已存在记录
        QueryWrapper query = QueryWrapper.create()
                .where(SYS_JOB.GROUP.eq(bean.getGroup())
                        .and(SYS_JOB.NAME.eq(bean.getName()))
                        .and(SYS_JOB.ID.ne(bean.getId()))
                );

        return ObjUtil.isNull(this.getOne(query));
    }

    private AbstractCronJob createTask(Job bean) {
        /*
         * 克隆Job
         */
        Job tempJob = BeanUtil.copyProperties(bean, Job.class);

        if (tempJob.getIsConcurrent()) {
            return new CronJobExecution(tempJob);
        }

        return new CronDisallowConcurrentExecution(tempJob);
    }

    @Override
    public boolean delete(List<Integer> idList) {
        /*
         * 删除前停止任务
         */
        for (Integer id : idList) {
            Job bean = this.get(id);
            this.pause(bean);
        }

        return this.getMapper().deleteBatchByIds(idList) > 0;
    }

    @Override
    public boolean destroy() {
        this.stopCron();
        return true;
    }

    @Override
    public Job get(Integer id) {
        return this.getMapper().selectOneById(id);
    }

    @Override
    public void init() throws ServiceException {
        /*
         * 如果Cron服务没有启动，则启动之
         */
        List<SysJob> jobList = this.list();
        for (Job job : jobList) {
            if (CharSequenceUtil.equals(job.getStatus(), JobStatus.OFF.name())) {
                this.pause(job);
            } else if (CharSequenceUtil.equals(job.getStatus(), JobStatus.ON.name())) {
                this.resume(job);
            }
        }
    }

    @Tran
    @Override
    public boolean insert(Job bean) {
        if (!this.checkUnique(bean)) {
            throw new ServiceException("任务名称已经存在，请更换其它值！");
        }

        if (!this.checkCron(bean)) {
            throw new ServiceException("Cron表达式校验不通过，请更换其它值！");
        }

        SysJob cloneBean = BeanUtil.copyProperties(bean, SysJob.class);

        return this.getMapper().insert(cloneBean) > 0;
    }

    @Override
    public PageResult<? extends Job> pageBy(org.dromara.hutool.db.Page page, Map<String, Object> paraMap) {
        String group = (String) paraMap.get("group");
        String word = (String) paraMap.get("word");

        QueryWrapper query = QueryWrapper.create()
                .where(SYS_JOB.GROUP.eq(group).when(StrValidator.isNotBlank(group))
                        .and(SYS_JOB.NAME.like(word).when(StrValidator.isNotBlank(word))
                                .or(SYS_JOB.TARGET.like(word).when(StrValidator.isNotBlank(word)))
                                .or(SYS_JOB.CRON.like(word).when(StrValidator.isNotBlank(word)))
                        )
                );

        Page<SysJob> pageList = this.getMapper().paginate(Page.of(page.getPageNumber(), page.getPageSize()),
                QueryWrapperUtil.applyOrderBy(query, page));

        PageResult<Job> result = new PageResult<>();
        result.addAll(pageList.getRecords());
        result.setTotal(ConvertUtil.toInt(pageList.getTotalRow()));

        return result;
    }

    @Tran
    @Override
    public boolean pause(Job bean) {
        /*
         * 生成任务Key
         */
        String key = CronJobUtil.genTaskKey(bean);

        /*
         * 无任务
         */
        if (ObjUtil.isNull(CronUtil.getScheduler().getTask(key))) {
            return true;
        }

        /*
         * 有任务，则移除之
         */
        CronUtil.getScheduler().deschedule(key);

        Job exist = this.get(bean.getId());
        if (!CharSequenceUtil.equals(exist.getStatus(), JobStatus.OFF.name())) {
            exist.setStatus(JobStatus.OFF.name());
            this.update(exist);
        }

        this.log.debug("任务 [{}] 停止成功。。。", exist.getName());

        return true;
    }

    @Tran
    @Override
    public boolean resume(Job bean) {
        /*
         * 生成任务Key
         */
        String key = CronJobUtil.genTaskKey(bean);

        this.startCron();

        /*
         * 无任务，则创建之
         */
        if (ObjUtil.isNull(CronUtil.getScheduler().getTask(key))) {
            /*
             * 创建Task
             */
            Task task = this.createTask(bean);

            CronUtil.getScheduler().schedule(key, bean.getCron(), task);

            Job exist = this.get(bean.getId());
            if (!CharSequenceUtil.equals(exist.getStatus(), BeanStatus.ON.name())) {
                exist.setStatus(BeanStatus.ON.name());
                this.update(exist);
            }
            this.log.debug("任务 [{}] 启动成功。。。", bean.getName());
        } else {
            /*
             * 有任务，则更新之
             */
            CronUtil.getScheduler().updatePattern(key, CronPattern.of(bean.getCron()));

            this.log.debug("任务 [{}] 更新成功。。。", bean.getName());
        }

        return true;
    }

    private void startCron() {
        if (!CronUtil.getScheduler().isStarted()) {
            CronUtil.start();
        }
    }

    private void stopCron() {
        if (CronUtil.getScheduler().isStarted()) {
            CronUtil.stop();
        }
    }

    @Tran
    @Override
    public boolean test(Job bean) {
        // 执行测试时，必须写调度日志
        bean.setIsLog(true);

        /*
         * 创建Task
         */
        Task task = this.createTask(bean);

        /*
         * 执行任务（单次）
         */
        try {
            task.execute();
            return true;
        } catch (Throwable e) {
            return false;
        }

    }

    @Override
    public boolean update(Job bean) {
        if (!this.checkUnique(bean)) {
            throw new ServiceException("任务名称已经存在，请更换其它值！");
        }

        if (!this.checkCron(bean)) {
            throw new ServiceException("Cron表达式校验不通过，请更换其它值！");
        }

        if (StrUtil.equals(bean.getStatus(), BeanStatus.OFF.name())) {
            // 停止定时任务
            this.pause(bean);
        }

        SysJob cloneBean = BeanUtil.copyProperties(bean, SysJob.class);

        return this.getMapper().update(cloneBean) > 0;
    }
}
