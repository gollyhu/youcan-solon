package cn.hg.solon.youcan.job.entity;

import java.util.Date;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.flex.listener.EntityInsertListener;
import cn.hg.solon.youcan.flex.listener.EntityUpdateListener;


@Table(value = "sys_job", onInsert = EntityInsertListener.class, onUpdate = EntityUpdateListener.class)
public class SysJob extends Job {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1207935338993322124L;

    /**
     * ID
     */
    @Id(keyType = KeyType.Auto)
    private Integer id;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 任务组名
     */
    private String group;

    /**
     * 调用目标字符串
     */
    private String target;

    /**
     * cron执行表达式
     */
    private String cron;

    /**
     * 计划执行错误策略（IMMEDIATELY 立即执行，ONCE 执行一次，ABORT 放弃执行）
     */
    private String policy = "ABORT";

    /**
     * 是否并发执行
     */
    private Boolean isConcurrent = Boolean.FALSE;

    /**
     * 是否记录调度日志
     */
    private Boolean isLog = Boolean.TRUE;

    /**
     * 任务状态（ON 启用, OFF 停用）
     */
    private String status = BeanStatus.OFF.name();

    /**
     * 创建者
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 更新者
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    /**
     * 备注
     */
    private String remark;

    @Override
    public Boolean getIsConcurrent() {
        return this.isConcurrent;
    }

    @Override
    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    @Override
    public Integer getCreator() {
        return this.creator;
    }

    @Override
    public String getCron() {
        return this.cron;
    }

    @Override
    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    @Override
    public Integer getEditor() {
        return this.editor;
    }

    @Override
    public String getGroup() {
        return this.group;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getPolicy() {
        return this.policy;
    }

    @Override
    public String getRemark() {
        return this.remark;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public String getTarget() {
        return this.target;
    }

    @Override
    public void setIsConcurrent(Boolean isConcurrent) {
        this.isConcurrent = isConcurrent;
    }

    @Override
    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    @Override
    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    @Override
    public void setCron(String cron) {
        this.cron = cron;
    }

    @Override
    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    @Override
    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    @Override
    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setPolicy(String policy) {
        this.policy = policy;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public Boolean getIsLog() {
        return isLog;
    }

    @Override
    public void setIsLog(Boolean log) {
        isLog = log;
    }
}
