package cn.hg.solon.youcan.job.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.mybatisflex.core.BaseMapper;

import cn.hg.solon.youcan.job.entity.SysJob;

@Mapper
public interface SysJobMapper extends BaseMapper<SysJob> {

}
