package cn.hg.solon.youcan.system.entity;

import java.util.Date;

import cn.hg.solon.youcan.system.entity.proxy.EqConfigProxy;
import cn.hg.solon.youcan.system.entity.proxy.EqUserLoginProxy;
import com.easy.query.core.annotation.Column;
import com.easy.query.core.annotation.EntityProxy;
import com.easy.query.core.annotation.Table;
import com.easy.query.core.proxy.ProxyEntityAvailable;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table("sys_user_login")
@EntityProxy
public class EqUserLogin extends UserLogin implements ProxyEntityAvailable<EqUserLogin , EqUserLoginProxy> {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8400337179667489400L;

    /**
     * ID
     */
    @Column(primaryKey = true, generatedKey = true)
    private Integer id;

    /**
     * 账号
     */
    private String account;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 登录IP
     */
    private String ip;

    /**
     * 登录地点
     */
    private String location;

    /**
     * 浏览器类型
     */
    private String browser;

    /**
     * 操作系统
     */
    private String os;

    /**
     * 任务状态：SUCCESS 成功，FAIL 失败
     */
    private String status;

    /**
     * 提示消息
     */
    private String msg;

    /**
     * 访问时间
     */
    private Date loginDatetime;

    @Override
    public String getAccount() {
        return this.account;
    }

    @Override
    public String getBrowser() {
        return this.browser;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getIp() {
        return this.ip;
    }

    @Override
    public String getLocation() {
        return this.location;
    }

    @Override
    public Date getLoginDatetime() {
        return this.loginDatetime;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }

    @Override
    public String getNickname() {
        return this.nickname;
    }

    @Override
    public String getOs() {
        return this.os;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public void setBrowser(String browser) {
        this.browser = browser;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public void setLoginDatetime(Date loginDatetime) {
        this.loginDatetime = loginDatetime;
    }

    @Override
    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void setOs(String os) {
        this.os = os;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

}
