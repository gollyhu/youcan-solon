package cn.hg.solon.youcan.system.provider;

import cn.hg.solon.youcan.easyquery.util.EntityQueryableUtil;
import cn.hg.solon.youcan.system.entity.EqOperateLog;
import cn.hg.solon.youcan.system.entity.OperateLog;
import cn.hg.solon.youcan.system.entity.proxy.EqOperateLogProxy;
import cn.hg.solon.youcan.system.service.OperateLogService;
import com.easy.query.api.proxy.client.EasyEntityQuery;
import com.easy.query.api.proxy.entity.select.EntityQueryable;
import com.easy.query.core.api.pagination.EasyPageResult;
import com.easy.query.solon.annotation.Db;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author 胡高
 */
@Component
public class EqOperateLogProvider implements OperateLogService {

    @Db("db1")
    private EasyEntityQuery easyEntityQuery;

    private EntityQueryable<EqOperateLogProxy, EqOperateLog> buildQuery(Page page, Map<String, Object> map) {
        String type = (String) map.get("type");
        String method = (String) map.get("method");
        String status = (String) map.get("status");
        String word = (String) map.get("word");
        Date startDate = (Date) map.get("startDate");
        Date endDate = (Date) map.get("endDate");

        EntityQueryable<EqOperateLogProxy, EqOperateLog> entityQueryable = this.easyEntityQuery
                // FROM sys_operate_log AS t
                .queryable(EqOperateLog.class)
                // WHERE t.`type` = ${type} AND t.`method` = ${method} AND t.`status` = ${status}
                //      AND ${startDate} <= t.`action_datetime` AND t.`action_datetime` <= ${endDate}
                .where(t -> {
                            t.type().eq(StrValidator.isNotBlank(type), type);
                            t.method().eq(StrValidator.isNotBlank(method), method);
                            t.status().eq(StrValidator.isNotBlank(status), status);
                            t.actionDatetime().rangeClosed(ObjUtil.isNotNull(startDate), startDate, ObjUtil.isNotNull(endDate), endDate);
                            //  AND (t.`title` LIKE '%${word}%' OR t.`user_name` LIKE '%${word}%' OR t.`dept_name` LIKE '%${word}%'
                            //       OR t.`url` LIKE '%${word}%' OR t.`ip` LIKE '%${word}%' OR t.`location` LIKE '%${word}%'
                            //       OR t.`param` LIKE '%${word}%' OR t.`result` LIKE '%${word}%' OR t.`message` LIKE '%${word}%')
                            t.or(StrValidator.isNotBlank(word), () -> {
                                        t.title().like(word);
                                        t.userName().like(word);
                                        t.deptName().like(word);
                                        t.url().like(word);
                                        t.ip().like(word);
                                        t.location().like(word);
                                        t.param().like(word);
                                        t.result().like(word);
                                        t.message().like(word);
                                    }
                            );
                        }
                );

        // ORDER BY ${sortField}
        return EntityQueryableUtil.applyOrderBy(entityQueryable, page);
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.OperateLogService#clear()
     */
    @Tran
    @Override
    public boolean clear() {
        return this.easyEntityQuery.deletable(EqOperateLog.class)
                .where(t -> t.id().ge(0))
                .allowDeleteStatement(true)
                .executeRows() > 0L;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.OperateLogService#delete(java.util.List)
     */
    @Tran
    @Override
    public boolean delete(List<Integer> idList) {
        return this.easyEntityQuery.deletable(EqOperateLog.class)
                .where(t -> t.id().in(idList))
                .allowDeleteStatement(true)
                .executeRows() > 0L;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.OperateLogService#get(java.lang.Integer)
     */
    @Override
    public EqOperateLog get(Integer id) {
        return this.easyEntityQuery.queryable(EqOperateLog.class).whereById(id).firstOrNull();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.OperateLogService#insert(cn.hg.solon.youcan.system.entity.OperateLog)
     */
    @Tran
    @Override
    public boolean insert(OperateLog bean) {
        EqOperateLog cloneBean = BeanUtil.copyProperties(bean, EqOperateLog.class);

        return this.easyEntityQuery.insertable(cloneBean).executeRows() > 0L;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.OperateLogService#pageBy(int, int, java.util.Map)
     */
    @Override
    public PageResult<EqOperateLog> pageBy(Page page, Map<String, Object> paraMap) {
        EasyPageResult<EqOperateLog> pageList = this.buildQuery(page, paraMap).toPageResult(page.getPageNumber(), page.getPageSize());

        PageResult<EqOperateLog> result = new PageResult<>();
        result.addAll(pageList.getData());
        result.setTotal(ConvertUtil.toInt(pageList.getTotal()));

        return result;
    }

}
