package cn.hg.solon.youcan.easyquery.config;

import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;

import com.easy.query.core.configuration.QueryConfiguration;
import com.easy.query.solon.annotation.Db;

import cn.hg.solon.youcan.easyquery.interceptor.EqEntityInterceptor;

/**
 * easy-query相关配置
 * @author 胡高
 */
@Configuration
public class EqConfig {

    /**
     * 配置QueryConfiguration
     * 
     * @param configuration QueryConfiguration实例
     */
    @Bean
    public void eqQueryConfiguration(@Db("db1") QueryConfiguration configuration) {
        // 配置对象拦截器
        configuration.applyInterceptor(new EqEntityInterceptor());
    }

}
