package cn.hg.solon.youcan.system.entity;

import cn.hg.solon.youcan.system.entity.proxy.EqDictItemProxy;
import com.easy.query.core.annotation.*;
import com.easy.query.core.basic.extension.logicdel.LogicDeleteStrategyEnum;
import com.easy.query.core.proxy.ProxyEntityAvailable;

import java.util.Date;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table("sys_dict_item")
@EntityProxy
public class EqDictItem extends DictItem implements ProxyEntityAvailable<EqDictItem , EqDictItemProxy> {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -5461288783871456390L;

    /**
     * 字典编码
     */
    @Column(primaryKey = true, generatedKey = true)
    private Integer id;

    /**
     * 显示排序
     */
    private Integer sort;

    /**
     * 字典标签
     */
    private String label;

    /**
     * 字典键值
     */
    private String value;

    /**
     * 字典类型
     */
    private String type;

    /**
     * 备注
     */
    private String remark;

    /**
     * 样式属性（其他样式扩展）
     */
    private String cssClass;

    /**
     * 表格回显样式
     */
    private String listClass;

    /**
     * 是否默认（0 否，1 是）
     */
    private Boolean isDefault;

    /**
     * 状态（ON 正常, OFF 停用,）
     */
    private String status;

    /**
     * 删除标志（true 是，false 否）
     */
    @LogicDelete(strategy = LogicDeleteStrategyEnum.BOOLEAN)
    private Boolean isDel;

    /**
     * 创建人
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 编辑人
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    @Override
    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    @Override
    public Integer getCreator() {
        return this.creator;
    }

    @Override
    public String getCssClass() {
        return this.cssClass;
    }

    @Override
    public Boolean getIsDel() {
        return this.isDel;
    }

    @Override
    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    @Override
    public Integer getEditor() {
        return this.editor;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public Boolean getIsDefault() {
        return this.isDefault;
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public String getListClass() {
        return this.listClass;
    }

    @Override
    public String getRemark() {
        return this.remark;
    }

    @Override
    public Integer getSort() {
        return this.sort;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    @Override
    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    @Override
    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    @Override
    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    @Override
    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    @Override
    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public void setListClass(String listClass) {
        this.listClass = listClass;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

}
