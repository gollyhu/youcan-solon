package cn.hg.solon.youcan.system.provider;

import cn.hg.solon.youcan.easyquery.util.EntityQueryableUtil;
import cn.hg.solon.youcan.system.entity.EqNotice;
import cn.hg.solon.youcan.system.entity.Notice;
import cn.hg.solon.youcan.system.entity.proxy.EqNoticeProxy;
import cn.hg.solon.youcan.system.service.NoticeService;
import com.easy.query.api.proxy.client.EasyEntityQuery;
import com.easy.query.api.proxy.entity.select.EntityQueryable;
import com.easy.query.core.api.pagination.EasyPageResult;
import com.easy.query.solon.annotation.Db;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.bean.copier.CopyOptions;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.List;
import java.util.Map;

/**
 * @author 胡高
 */
@Component
public class EqNoticeProvider implements NoticeService {

    @Db("db1")
    private EasyEntityQuery easyEntityQuery;

    private EntityQueryable<EqNoticeProxy, EqNotice> buildQuery(Page page, Map<String, Object> paraMap) {
        String word = (String) paraMap.get("word");
        String type = (String) paraMap.get("type");
        String status = (String) paraMap.get("status");

        EntityQueryable<EqNoticeProxy, EqNotice> entityQueryable = this.easyEntityQuery
                // FROM sys_notice AS t
                .queryable(EqNotice.class)
                // WHERE t.`type` = ${type} AND t.`status` = ${status}
                .where(t -> {
                    t.type().eq(StrValidator.isNotBlank(type), type);
                    t.status().eq(StrValidator.isNotBlank(status), status);
                    // AND (t.`title` LIKE '%${word}%' OR t.`content` LIKE '%${word}%' OR t.`remark` LIKE '%${word}%')
                    t.or(StrValidator.isNotBlank(word), () -> {
                        t.title().like(word);
                        t.content().like(word);
                        t.remark().like(word);
                    });
                });
        // ORDER BY ${sortField} DESC
        return EntityQueryableUtil.applyOrderBy(entityQueryable, page);
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.NoticeService#delete(java.util.List)
     */
    @Tran
    @Override
    public boolean delete(List<Integer> idList) {
        return this.easyEntityQuery.deletable(EqNotice.class).where(t -> t.id().in(idList)).executeRows() > 0L;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.NoticeService#get(java.lang.Integer)
     */
    @Override
    public EqNotice get(Integer id) {
        return this.easyEntityQuery.queryable(EqNotice.class).whereById(id).firstOrNull();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.NoticeService#insert(cn.hg.solon.youcan.system.entity.Notice)
     */
    @Tran
    @Override
    public boolean insert(Notice bean) {
        EqNotice cloneBean = BeanUtil.copyProperties(bean, EqNotice.class);

        return this.easyEntityQuery.insertable(cloneBean).executeRows() > 0L;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.NoticeService#pageBy(int, int, java.util.Map)
     */
    @Override
    public PageResult<EqNotice> pageBy(Page page, Map<String, Object> paraMap) {
        EasyPageResult<EqNotice> pageList = this.buildQuery(page, paraMap).toPageResult(page.getPageNumber(), page.getPageSize());

        PageResult<EqNotice> result = new PageResult<>();
        result.addAll(pageList.getData());
        result.setTotal(ConvertUtil.toInt(pageList.getTotal()));

        return result;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.NoticeService#update(cn.hg.solon.youcan.system.entity.Notice)
     */
    @Tran
    @Override
    public boolean update(Notice bean) {
        EqNotice cloneBean = this.get(bean.getId());

        BeanUtil.copyProperties(bean, cloneBean, CopyOptions.of().setIgnoreNullValue(true));

        return this.easyEntityQuery.updatable(cloneBean).executeRows() > 0L;
    }

}
