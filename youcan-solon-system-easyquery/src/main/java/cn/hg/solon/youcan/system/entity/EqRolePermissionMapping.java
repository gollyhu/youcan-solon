package cn.hg.solon.youcan.system.entity;

import cn.hg.solon.youcan.system.entity.proxy.EqConfigProxy;
import cn.hg.solon.youcan.system.entity.proxy.EqRolePermissionMappingProxy;
import com.easy.query.core.annotation.Column;
import com.easy.query.core.annotation.EntityProxy;
import com.easy.query.core.annotation.Table;
import com.easy.query.core.proxy.ProxyEntityAvailable;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table(value = "sys_role_permission_mapping")
@EntityProxy
public class EqRolePermissionMapping extends RolePermissionMapping implements ProxyEntityAvailable<EqRolePermissionMapping , EqRolePermissionMappingProxy> {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -5841518908290364068L;

    /**
     * ID
     */
    @Column(primaryKey = true, generatedKey = true)
    private Integer id;

    /**
     * 角色ID，关联sys_role表的ID
     */
    private Integer roleId;

    /**
     * 权限ID，关联sys_permission表的ID
     */
    private Integer permissionId;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public Integer getPermissionId() {
        return this.permissionId;
    }

    @Override
    public Integer getRoleId() {
        return this.roleId;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    @Override
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

}
