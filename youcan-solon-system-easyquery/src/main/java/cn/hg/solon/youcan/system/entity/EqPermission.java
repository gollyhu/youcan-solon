package cn.hg.solon.youcan.system.entity;

import java.util.Date;

import cn.hg.solon.youcan.system.entity.proxy.EqConfigProxy;
import cn.hg.solon.youcan.system.entity.proxy.EqPermissionProxy;
import com.easy.query.core.annotation.Column;
import com.easy.query.core.annotation.EntityProxy;
import com.easy.query.core.annotation.LogicDelete;
import com.easy.query.core.annotation.Table;
import com.easy.query.core.basic.extension.logicdel.LogicDeleteStrategyEnum;
import com.easy.query.core.proxy.ProxyEntityAvailable;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table("sys_permission")
@EntityProxy
public class EqPermission extends Permission implements ProxyEntityAvailable<EqPermission, EqPermissionProxy> {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3955062045293034601L;

    /**
     * ID
     */
    @Column(primaryKey = true, generatedKey = true)
    private Integer id;

    /**
     * 权限名称
     */
    private String name;

    /**
     * 父菜单ID
     */
    private Integer parentId;

    /**
     * 祖级ID列表
     */
    private String ancestors;

    /**
     * 显示顺序
     */
    private Integer sort;

    /**
     * 请求地址
     */
    private String url;

    /**
     * 打开方式（_iframe 正常打开，_blank 新建窗口）
     */
    private String target;

    /**
     * 类型（NODE 节点，MENU 菜单，FUNCTION 功能按钮）
     */
    private String type;

    /**
     * 状态（ON 正常，OFF 停用）
     */
    private String status;

    /**
     * 删除标志（true 是，false 否）
     */
    @LogicDelete(strategy = LogicDeleteStrategyEnum.BOOLEAN)
    private Boolean isDel;

    /**
     * 请求方法
     */
    private String method;

    /**
     * 权限值
     */
    private String value;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 页面Class
     */
    private String cssClass;

    /**
     * 创建人
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 编辑人
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    @Override
    public String getAncestors() {
        return this.ancestors;
    }

    @Override
    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    @Override
    public Integer getCreator() {
        return this.creator;
    }

    @Override
    public String getCssClass() {
        return this.cssClass;
    }

    @Override
    public Boolean getIsDel() {
        return this.isDel;
    }

    @Override
    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    @Override
    public Integer getEditor() {
        return this.editor;
    }

    @Override
    public String getIcon() {
        return this.icon;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getMethod() {
        return this.method;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Integer getParentId() {
        return this.parentId;
    }

    @Override
    public Integer getSort() {
        return this.sort;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public String getTarget() {
        return this.target;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public String getUrl() {
        return this.url;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public void setAncestors(String ancestors) {
        this.ancestors = ancestors;
    }

    @Override
    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    @Override
    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    @Override
    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    @Override
    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    @Override
    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    @Override
    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    @Override
    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Override
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

}
