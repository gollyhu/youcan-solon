package cn.hg.solon.youcan.system.entity;

import java.util.Date;

import cn.hg.solon.youcan.system.entity.proxy.EqConfigProxy;
import com.easy.query.core.annotation.*;
import com.easy.query.core.basic.extension.logicdel.LogicDeleteStrategyEnum;
import com.easy.query.core.proxy.ProxyEntityAvailable;

/**
 * @author 胡高
 * @date 2023/08/03
 */
@Table("sys_config")
@EntityProxy
public class EqConfig extends Config implements ProxyEntityAvailable<EqConfig , EqConfigProxy> {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7679225220719335300L;

    /**
     * ID
     */
    @Column(primaryKey = true, generatedKey = true)
    private Integer id;

    /**
     * 参数名称
     */
    private String name;

    /**
     * 参数代码
     */
    private String code;

    /**
     * 参数值
     */
    private String value;

    /**
     * 参数类型（SITE 站点，COMPANY 公司，CONFIG 参数。。。类型也可以自己定义）
     */
    private String type;

    /**
     * 系统内置（true 是，false 否），内置参数不允许删除
     */
    private Boolean isSys;

    /**
     * 删除标志（true 是，false 否）
     */
    @LogicDelete(strategy = LogicDeleteStrategyEnum.BOOLEAN)
    private Boolean isDel;

    /**
     * 创建者
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 更新者
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    /**
     * 备注
     */
    private String remark;

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    @Override
    public Integer getCreator() {
        return this.creator;
    }

    @Override
    public Boolean getIsDel() {
        return this.isDel;
    }

    @Override
    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    @Override
    public Integer getEditor() {
        return this.editor;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public Boolean getIsSys() {
        return this.isSys;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRemark() {
        return this.remark;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    @Override
    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    @Override
    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    @Override
    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    @Override
    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setIsSys(Boolean isSys) {
        this.isSys = isSys;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

}
