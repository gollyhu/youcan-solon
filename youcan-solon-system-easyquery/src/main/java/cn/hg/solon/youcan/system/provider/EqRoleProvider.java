package cn.hg.solon.youcan.system.provider;

import cn.hg.solon.youcan.common.exception.ServiceException;
import cn.hg.solon.youcan.easyquery.util.EntityQueryableUtil;
import cn.hg.solon.youcan.system.entity.*;
import cn.hg.solon.youcan.system.entity.proxy.EqRoleProxy;
import cn.hg.solon.youcan.system.service.RoleService;
import com.easy.query.api.proxy.client.EasyEntityQuery;
import com.easy.query.api.proxy.entity.select.EntityQueryable;
import com.easy.query.core.api.pagination.EasyPageResult;
import com.easy.query.solon.annotation.Db;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.bean.copier.CopyOptions;
import org.dromara.hutool.core.collection.CollUtil;
import org.dromara.hutool.core.collection.ListUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author 胡高
 */
@Component
public class EqRoleProvider implements RoleService {

    @Db
    private EasyEntityQuery easyEntityQuery;

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.RoleService#assignDataScope(cn.hg.solon.youcan.system.entity.Role, java.lang.String, java.util.List)
     */
    @Tran
    @Override
    public void assignDataScope(Role role, String dataScope, List<Integer> deptIds) {
        /*
         * 删除所有关联部门
         * DELETE FROM sys_role_dept_mapping WHERE role_id = ${role.id}
         */
        this.easyEntityQuery.deletable(EqRoleDeptMapping.class)
                .where(t -> t.roleId().eq(role.getId()))
                .allowDeleteStatement(true)
                .executeRows();

        /*
         * 重新建立角色部门映射
         */
        List<EqRoleDeptMapping> beanList = new ArrayList<>();
        for (int id : deptIds) {
            EqRoleDeptMapping bean = new EqRoleDeptMapping();
            bean.setRoleId(role.getId());
            bean.setDeptId(id);

            beanList.add(bean);
        }

        if (CollUtil.isNotEmpty(beanList)) {
            this.easyEntityQuery.insertable(beanList).executeRows();
        }

        EqRole cloneBean = new EqRole();
        BeanUtil.copyProperties(role, cloneBean, CopyOptions.of().setIgnoreNullValue(true));

        cloneBean.setDataScope(dataScope);

        this.easyEntityQuery.updatable(cloneBean).executeRows();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.RoleService#assignPermission(cn.hg.solon.youcan.system.entity.Role, java.util.List)
     */
    @Tran
    @Override
    public void assignPermission(Role role, List<Integer> permissionIds) {
        /*
         * 删除所有关联权限
         * DELETE FROM sys_role_permission_mapping WHERE role_id = ${role.id}
         */
        this.easyEntityQuery.deletable(EqRolePermissionMapping.class)
                .where(t -> t.roleId().eq(role.getId()))
                .allowDeleteStatement(true)
                .executeRows();

        /*
         * 重新建立角色权限映射
         */
        List<EqRolePermissionMapping> beanList = new ArrayList<>();
        for (int id : permissionIds) {
            EqRolePermissionMapping bean = new EqRolePermissionMapping();
            bean.setRoleId(role.getId());
            bean.setPermissionId(id);

            beanList.add(bean);
        }

        if (CollUtil.isNotEmpty(beanList)) {
            this.easyEntityQuery.insertable(beanList).executeRows();
        }
    }

    private EntityQueryable<EqRoleProxy, EqRole> buildQuery(Page page, Map<String, Object> paraMap) {
        String word = (String) paraMap.get("word");
        String status = (String) paraMap.get("status");

        EntityQueryable<EqRoleProxy, EqRole> entityQueryable = this.easyEntityQuery
                // FROM sys_role AS t
                .queryable(EqRole.class)
                // WHERE t.`status` = ${status}
                //      AND (t.`name` LIKE '%${word}%' OR t.`code` LIKE '%${word}%' OR t.`value` LIKE '%${word}%')
                .where(t -> {
                    t.status().eq(StrValidator.isNotBlank(status), status);
                    t.or(StrValidator.isNotBlank(word), () -> {
                        t.name().like(word);
                        t.code().like(word);
                    });
                });

        // ORDER BY ${sortField}
        return EntityQueryableUtil.applyOrderBy(entityQueryable, page);
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.RoleService#checkUnique(cn.hg.solon.youcan.system.entity.Role)
     */
    @Override
    public boolean checkUnique(Role bean) {
        return !this.easyEntityQuery
                // FROM sys_role AS t
                .queryable(EqRole.class)
                // WHERE t.`code` = ${bean.code} AND t.`id` <> ${bean.id}
                .where(t -> {
                    t.code().eq(bean.getCode());
                    t.id().ne(ObjUtil.isNotNull(bean.getId()), bean.getId());
                })
                .any();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.RoleService#delete(java.util.List)
     */
    @Tran
    @Override
    public boolean delete(List<Integer> idList) {
        if (CollUtil.contains(idList, Integer.valueOf(1))) {
            throw new ServiceException("初始的 超级管理员角色 不允许删除！");
        }

        /*
         * 检查是否已分配于用户
         */
        for (Integer id : idList) {
            if (this.easyEntityQuery
                    // FROM sys_role AS t
                    .queryable(EqRole.class)
                    // LEFT JOIN sys_user_role_mapping AS t1 ON t1.`role_id` = t.`id`
                    .leftJoin(EqUserRoleMapping.class, (t, t1) -> t1.roleId().eq(t.id()))
                    // LEFT JOIN sys_user AS t2 ON t2.`id` = t1.`user_id`
                    .leftJoin(EqUser.class, (t, t1, t2) -> t2.id().eq(t1.userId()))
                    // WHERE t2.`role_id` = ${id}
                    .where((t, t1, t2) -> t1.roleId().eq(id))
                    .any()) {
                throw new ServiceException("已分配于用户，不允许删除！");
            }
        }

        return this.easyEntityQuery.deletable(EqRole.class).whereByIds(idList).executeRows() > 0L;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.RoleService#get(java.lang.Integer)
     */
    @Override
    public EqRole get(Integer id) {
        return this.easyEntityQuery.queryable(EqRole.class).whereById(id).firstOrNull();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.RoleService#getByCode(java.lang.String)
     */
    @Override
    public EqRole getByCode(String code) {
        return this.easyEntityQuery
                // FROM sys_role AS t
                .queryable(EqRole.class)
                // WHERE t.`code` = ${code}
                .where(t -> t.code().eq(code))
                .firstOrNull();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.RoleService#insert(cn.hg.solon.youcan.system.entity.Role, java.util.List, java.util.List)
     */
    @Tran
    @Override
    public boolean insert(Role bean, List<Integer> deptIds, List<Integer> permissionIds) {
        if (!this.checkUnique(bean)) {
            throw new ServiceException("权限代码已经存在，请更换其它值！");
        }

        EqRole cloneBean = BeanUtil.copyProperties(bean, EqRole.class);

        // 注意：插入时使用的True，代表插入同时获取ID值填充到cloneBean
        boolean ret = this.easyEntityQuery.insertable(cloneBean).executeRows(true) > 0L;

        // 指派数据权限
        this.assignDataScope(cloneBean, bean.getDataScope(), deptIds);

        // 指派系统权限
        this.assignPermission(cloneBean, permissionIds);

        return ret;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.RoleService#listBy(java.util.Map)
     */
    @Override
    public List<EqRole> listBy(Map<String, Object> paraMap) {
        return this.easyEntityQuery.queryable(EqRole.class).toList();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.RoleService#listByStatus(java.lang.String)
     */
    @Override
    public List<EqRole> listByStatus(String status) {
        return this.easyEntityQuery
                // FROM sys_role AS t
                .queryable(EqRole.class)
                // WHERE t.`status` = ${status}
                .where(t -> t.status().eq(StrValidator.isNotBlank(status), status))
                // ORDER BY t.`sort` ASC
                .orderBy(t -> t.sort())
                .toList();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.RoleService#listByUser(cn.hg.solon.youcan.system.entity.User)
     */
    @Override
    public List<EqRole> listByUser(User user) {
        if (ObjUtil.isNull(user)) {
            return ListUtil.empty();
        }

        return this.easyEntityQuery
                // FROM sys_role AS t
                .queryable(EqRole.class)
                // LEFT JOIN sys_user_role AS t1 ON t1.`role_id` = t.`id`
                .leftJoin(EqUserRoleMapping.class, (t, t1) -> t1.roleId().eq(t.id()))
                // WHERE t1.`user_id` = ${user.id}
                .where((t, t1) -> t1.userId().eq(user.getId()))
                // ORDER BY r.sort
                .orderBy((t1, t2) -> t1.sort())
                .toList();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.RoleService#pageBy(int, int, java.util.Map)
     */
    @Override
    public PageResult<EqRole> pageBy(Page page, Map<String, Object> paraMap) {
        EasyPageResult<EqRole> pageList = this.buildQuery(page, paraMap).toPageResult(page.getPageNumber(), page.getPageSize());

        PageResult<EqRole> result = new PageResult<>();
        result.addAll(pageList.getData());
        result.setTotal(ConvertUtil.toInt(pageList.getTotal()));

        return result;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.RoleService#update(cn.hg.solon.youcan.system.entity.Role)
     */
    @Tran
    @Override
    public boolean update(Role bean) {
        if (!this.checkUnique(bean)) {
            throw new ServiceException("权限代码已经存在，请更换其它值！");
        }

        EqRole cloneBean = this.get(bean.getId());

        BeanUtil.copyProperties(bean, cloneBean, CopyOptions.of().setIgnoreNullValue(true));

        return this.easyEntityQuery.updatable(cloneBean).executeRows() > 0L;
    }

}
