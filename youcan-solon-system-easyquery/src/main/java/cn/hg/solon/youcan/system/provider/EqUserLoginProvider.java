package cn.hg.solon.youcan.system.provider;

import cn.hg.solon.youcan.easyquery.util.EntityQueryableUtil;
import cn.hg.solon.youcan.system.entity.EqUserLogin;
import cn.hg.solon.youcan.system.entity.UserLogin;
import cn.hg.solon.youcan.system.entity.proxy.EqUserLoginProxy;
import cn.hg.solon.youcan.system.service.UserLoginService;
import com.easy.query.api.proxy.client.EasyEntityQuery;
import com.easy.query.api.proxy.entity.select.EntityQueryable;
import com.easy.query.core.api.pagination.EasyPageResult;
import com.easy.query.solon.annotation.Db;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author 胡高
 */
@Component
public class EqUserLoginProvider implements UserLoginService {

    @Db("db1")
    private EasyEntityQuery easyEntityQuery;

    private EntityQueryable<EqUserLoginProxy, EqUserLogin> buildQuery(Page page, Map<String, Object> map) {
        String word = (String) map.get("word");
        String status = (String) map.get("status");
        Date startDate = (Date) map.get("startDate");
        Date endDate = (Date) map.get("endDate");

        EntityQueryable<EqUserLoginProxy, EqUserLogin> entityQueryable = this.easyEntityQuery
                // FROM sys_user_login AS t
                .queryable(EqUserLogin.class)
                // WHERE AND t.`status` = ${status} AND ${startDate} <= t.`action_datetime` AND t.`action_datetime` <= ${endDate}
                .where(t -> {
                    t.status().eq(StrValidator.isNotBlank(status), status);
                    t.loginDatetime().rangeClosed(ObjUtil.isNotNull(startDate), startDate, ObjUtil.isNotNull(endDate), endDate);
                    //  AND (t.`title` LIKE '%${word}%' OR t.`user_name` LIKE '%${word}%' OR t.`dept_name` LIKE '%${word}%'
                    //       OR t.`url` LIKE '%${word}%' OR t.`ip` LIKE '%${word}%' OR t.`location` LIKE '%${word}%'
                    //       OR t.`param` LIKE '%${word}%' OR t.`result` LIKE '%${word}%' OR t.`message` LIKE '%${word}%')
                    t.or(StrValidator.isNotBlank(word), () -> {
                        t.account().like(word);
                        t.nickname().like(word);
                        t.ip().like(word);
                        t.location().like(word);
                        t.browser().like(word);
                        t.os().like(word);
                        t.msg().like(word);
                    });
                });
        // ORDER BY ${sortField}
        return EntityQueryableUtil.applyOrderBy(entityQueryable, page);
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.UserLoginService#clear()
     */
    @Tran
    @Override
    public boolean clear() {
        return this.easyEntityQuery.deletable(EqUserLogin.class)
                .where(t -> t.id().ge(0))
                .allowDeleteStatement(true)
                .executeRows() > 0L;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.UserLoginService#delete(java.util.List)
     */
    @Tran
    @Override
    public boolean delete(List<Integer> idList) {
        return this.easyEntityQuery.deletable(EqUserLogin.class)
                .where(t -> t.id().in(idList))
                .allowDeleteStatement(true)
                .executeRows() > 0L;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.UserLoginService#get(java.lang.Integer)
     */
    @Override
    public EqUserLogin get(Integer id) {
        return this.easyEntityQuery.queryable(EqUserLogin.class)
                .whereById(id)
                .firstOrNull();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.UserLoginService#insert(cn.hg.solon.youcan.system.entity.UserLogin)
     */
    @Tran
    @Override
    public boolean insert(UserLogin bean) {
        EqUserLogin cloneBean = BeanUtil.copyProperties(bean, EqUserLogin.class);

        return this.easyEntityQuery.insertable(cloneBean).executeRows() > 0L;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.UserLoginService#pageBy(int, int, java.util.Map)
     */
    @Override
    public PageResult<EqUserLogin> pageBy(Page page, Map<String, Object> paraMap) {
        EasyPageResult<EqUserLogin> pageList = this.buildQuery(page, paraMap).toPageResult(page.getPageNumber(), page.getPageSize());

        PageResult<EqUserLogin> result = new PageResult<>();
        result.addAll(pageList.getData());
        result.setTotal(ConvertUtil.toInt(pageList.getTotal()));

        return result;
    }

}
