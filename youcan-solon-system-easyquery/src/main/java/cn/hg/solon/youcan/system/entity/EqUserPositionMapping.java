package cn.hg.solon.youcan.system.entity;

import cn.hg.solon.youcan.system.entity.proxy.EqConfigProxy;
import cn.hg.solon.youcan.system.entity.proxy.EqUserPositionMappingProxy;
import com.easy.query.core.annotation.Column;
import com.easy.query.core.annotation.EntityProxy;
import com.easy.query.core.annotation.Table;
import com.easy.query.core.proxy.ProxyEntityAvailable;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table(value = "sys_user_position_mapping")
@EntityProxy
public class EqUserPositionMapping extends UserPositionMapping implements ProxyEntityAvailable<EqUserPositionMapping , EqUserPositionMappingProxy> {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 5005875189355266005L;

    /**
     * ID
     */
    @Column(primaryKey = true, generatedKey = true)
    private Integer id;

    /**
     * 用户ID，关联sys_user表的ID
     */
    private Integer userId;

    /**
     * 岗位ID，关联sys_position表的ID
     */
    private Integer positionId;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public Integer getPositionId() {
        return this.positionId;
    }

    @Override
    public Integer getUserId() {
        return this.userId;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    @Override
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
