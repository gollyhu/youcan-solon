package cn.hg.solon.youcan.easyquery.core.sql;

import java.util.Map;

import org.dromara.hutool.core.map.MapUtil;

import com.easy.query.core.api.dynamic.sort.ObjectSort;
import com.easy.query.core.api.dynamic.sort.ObjectSortBuilder;

/**
 * @author 胡高
 */
public class PropertySort implements ObjectSort {

    private final Map<String, Boolean> sort;

    /**
     * @param sort 排序Map，Map中的key为属性名，value为是否小到大排序
     */
    public PropertySort(Map<String, Boolean> sort) {
        this.sort = sort;
    }

    /**
     * @param propertyName 属性名
     * @param asc 是否小到大排序
     */
    public PropertySort(String propertyName, Boolean asc) {
        this.sort = MapUtil.of(propertyName, asc);
    }

    /* (non-Javadoc)
     * @see com.easy.query.core.api.dynamic.sort.ObjectSort#configure(com.easy.query.core.api.dynamic.sort.ObjectSortBuilder)
     */
    @Override
    public void configure(ObjectSortBuilder builder) {
        for (Map.Entry<String, Boolean> s : this.sort.entrySet()) {
            builder.orderBy(s.getKey(), s.getValue());
        }
    }

}
