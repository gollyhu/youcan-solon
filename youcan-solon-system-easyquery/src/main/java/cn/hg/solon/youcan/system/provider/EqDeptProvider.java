package cn.hg.solon.youcan.system.provider;

import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.common.enums.DataScope;
import cn.hg.solon.youcan.common.exception.ServiceException;
import cn.hg.solon.youcan.easyquery.util.EntityQueryableUtil;
import cn.hg.solon.youcan.system.entity.*;
import cn.hg.solon.youcan.system.entity.proxy.EqDeptProxy;
import cn.hg.solon.youcan.system.service.DeptService;
import com.easy.query.api.proxy.client.EasyEntityQuery;
import com.easy.query.api.proxy.entity.select.EntityQueryable;
import com.easy.query.core.basic.api.select.Query;
import com.easy.query.solon.annotation.Db;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.bean.copier.CopyOptions;
import org.dromara.hutool.core.collection.ListUtil;
import org.dromara.hutool.core.math.NumberUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.tree.MapTree;
import org.dromara.hutool.core.tree.TreeNodeConfig;
import org.dromara.hutool.core.tree.TreeUtil;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.sql.Direction;
import org.dromara.hutool.db.sql.Order;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.List;
import java.util.Map;

/**
 * @author 胡高
 */
@Component
public class EqDeptProvider implements DeptService {

    @Db("db1")
    private EasyEntityQuery easyEntityQuery;

    private EntityQueryable<EqDeptProxy, EqDept> buildQuery(Map<String, Object> paraMap) {
        String word = (String) paraMap.get("word");

        // GROUP_CONCAT子查询（单列查询使用Query<T>做类型）
        Query<String> groupConcatQuery = this.easyEntityQuery
                // FROM sys_dept AS t1
                .queryable(EqDept.class)
                // WHERE (t1.`name` LIKE '%${word}$' OR t1.`leader` LIKE '%${word}$' OR t1.`phone` LIKE '%${word}$'
                //      OR t1.`email` LIKE '%${word}$')
                .where(t1 -> t1.or(StrValidator.isNotBlank(word), () -> {
                            t1.name().like(word);
                            t1.leader().like(word);
                            t1.phone().like(word);
                            t1.email().like(word);
                        })
                )
                // SELECT GROUP_CONCAT(t1.`ancestors`)
                //.selectColumn(t1 -> t1.expression().sqlSegment())
                .selectColumn(t1 -> t1.expression().sqlSegment("GROUP_CONCAT({0})",
                                c -> c.expression(t1.ancestors())
                        ).asAnyType(String.class)
                );

        EntityQueryable<EqDeptProxy, EqDept> entityQueryable = this.easyEntityQuery
                // FROM sys_dept AS t
                .queryable(EqDept.class)
                // WHERE (t.`name` LIKE '%${word}$' OR t.`leader` LIKE '%${word}$' OR t.`phone` LIKE '%${word}$'
                //      OR t.`email` LIKE '%${word}$'
                .where(t -> t.or(StrValidator.isNotBlank(word), () -> {
                            t.name().like(word);
                            t.leader().like(word);
                            t.phone().like(word);
                            t.email().like(word);
                            // OR FIND_IN_SET(t.`id`, ({GROUP_CONCAT子查询}))
                            t.expression().sql("FIND_IN_SET({0}, {1})", c -> {
                                c.expression(t.id());
                                c.expression(groupConcatQuery);
                            });
                        })
                );
        // ORDER BY `sort`
        return EntityQueryableUtil.applyOrderBy(entityQueryable, new Order("sort", Direction.ASC));
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.DeptService#checkUnique(cn.hg.solon.youcan.system.entity.Dept)
     */
    @Override
    public boolean checkUnique(Dept bean) {
        return !this.easyEntityQuery
                // FROM sys_dept AS t
                .queryable(EqDept.class)
                // WHERE t.`parent_id` = ${bean.parentId}
                .where(t -> {
                    t.parentId().eq(bean.getParentId());
                    // t.`name` = ${bean.name}
                    t.name().eq(bean.getName());
                    // t.`id` <> ${bean.id}
                    t.id().ne(ObjUtil.isNotNull(bean.getId()), bean.getId());
                })
                .any();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.DeptService#delete(java.lang.Integer)
     */
    @Tran
    @Override
    public boolean delete(Integer id) {
        /*
         * 检查是否子项
         */
        if (this.easyEntityQuery.queryable(EqDept.class).where(t -> t.parentId().eq(id)).any()) {
            throw new ServiceException("存在子项，不允许删除！");
        }

        /*
         * 检查是否已分配于角色
         */
        if (this.easyEntityQuery
                // FROM sys_dept AS t
                .queryable(EqDept.class)
                // LEFT JOIN sys_role_dept_mapping AS t1 ON t1.`dept_id` = t.`id`
                .leftJoin(EqRoleDeptMapping.class, (t, t1) -> t1.id().eq(t.id()))
                // LEFT JOIN sys_role AS t2 ON t2.`id` = t1.`role_id`
                .leftJoin(EqRole.class, (t, t1, t2) -> t2.id().eq(t1.id()))
                // WHERE t1.`dept_id` = ${id} AND t2.`data_scope` = 'CUSTOM'
                .where((t, t1, t2) -> {
                    t1.deptId().eq(id);
                    t2.dataScope().eq(DataScope.CUSTOM.name());
                })
                .any()) {
            throw new ServiceException("已分配于角色，不允许删除！");
        }

        /*
         * 检查部门下是否存在用户
         */
        if (this.easyEntityQuery
                // FROM sys_user AS t
                .queryable(EqUser.class)
                // WHERE t.`dept_id` = ${id}
                .where(t -> t.deptId().eq(id))
                .any()) {
            throw new ServiceException("部门下存在用户，不允许删除！");
        }

        return this.easyEntityQuery.deletable(EqDept.class).whereById(id).executeRows() > 0L;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.DeptService#get(java.lang.Integer)
     */
    @Override
    public EqDept get(Integer id) {
        return this.easyEntityQuery.queryable(EqDept.class).where(t -> t.id().eq(id)).firstOrNull();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.DeptService#insert(cn.hg.solon.youcan.system.entity.Dept)
     */
    @Tran
    @Override
    public boolean insert(Dept bean) {
        /*
         * 无父对象ID时，默认为0
         */
        if (ObjUtil.isNull(bean.getParentId())) {
            bean.setParentId(0);
        }

        if (!this.checkUnique(bean)) {
            throw new ServiceException("部门名称已经存在，请更换其它值！");
        }

        Dept partner = this.get(bean.getParentId());
        bean.setAncestors(ObjUtil.isNull(partner) ? "0" : partner.getAncestors() + "," + bean.getParentId());

        EqDept cloneBean = BeanUtil.copyProperties(bean, EqDept.class);

        return this.easyEntityQuery.insertable(cloneBean).executeRows() > 0L;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.DeptService#listBy(java.util.Map)
     */
    @Override
    public List<EqDept> listBy(Map<String, Object> paraMap) {
        return this.buildQuery(paraMap).toList();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.DeptService#listByRole(cn.hg.solon.youcan.system.entity.Role)
     */
    @Override
    public List<EqDept> listByRole(Role role) {
        if (ObjUtil.isNull(role)) {
            return ListUtil.empty();
        }

        return this.easyEntityQuery
                // FROM sys_dept AS t
                .queryable(EqDept.class)
                // LEFT JOIN sys_role_dept_mapping AS t1 ON t1.`dept_id` = t.`id`
                .leftJoin(EqRoleDeptMapping.class, (t, t1) -> t1.deptId().eq(t.id()))
                // WHERE t.`status` = 'ON' AND t1.`role_id` = ${role.id}
                .where((t, t1) -> {
                    t.status().eq(BeanStatus.ON.name());
                    t1.roleId().eq(role.getId());
                })
                // ORDER BY t.`sort` ASC
                .orderBy(t -> t.sort())
                .distinct()
                .toList();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.DeptService#listByStatus(java.lang.String)
     */
    @Override
    public List<EqDept> listByStatus(String status) {
        return this.easyEntityQuery
                // FROM sys_dept AS t
                .queryable(EqDept.class)
                // WHERE t.`status` = ${status}
                .where(t -> t.status().eq(StrValidator.isNotBlank(status), status))
                // ORDER BY t.`sort` ASC
                .orderBy(c -> c.sort())
                .toList();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.DeptService#update(cn.hg.solon.youcan.system.entity.Dept)
     */
    @Tran
    @Override
    public boolean update(Dept bean) {
        /*
         * 无父对象ID时，默认为0
         */
        if (ObjUtil.isNull(bean.getParentId())) {
            bean.setParentId(0);
        }
        Integer newParentId = bean.getParentId();

        if (NumberUtil.equals(newParentId, bean.getId())) {
            throw new ServiceException("上级不能为自己！");
        }

        if (!this.checkUnique(bean)) {
            throw new ServiceException("部门名称已经存在，请更换其它值！");
        }

        // 查询上级部门
        Dept parent = this.get(bean.getParentId());
        bean.setAncestors(ObjUtil.isNull(parent) ? "0" : parent.getAncestors() + "," + bean.getParentId());

        EqDept cloneBean = this.get(bean.getId());
        Integer currentParentId = cloneBean.getParentId();

        BeanUtil.copyProperties(bean, cloneBean, CopyOptions.of().setIgnoreNullValue(true));

        boolean ret = this.easyEntityQuery.updatable(cloneBean).executeRows() > 0L;

        // 如果上级部门有变化，则更新所有下级部门的ancestors字段
        if (!ObjUtil.equals(newParentId, currentParentId)) {
            this.updateChildrenAncestors(cloneBean);
        }

        return ret;
    }

    @Override
    public List<MapTree<Integer>> treeBy(Map<String, Object> paraMap) {
        List<EqDept> list = this.listBy(paraMap);

        TreeNodeConfig treeNodeConfig = new TreeNodeConfig();
        // 自定义属性名
        treeNodeConfig.setIdKey("id");
        treeNodeConfig.setParentIdKey("parentId");
        treeNodeConfig.setNameKey("name");

        List<MapTree<Integer>> trees = TreeUtil.build(list, 0, treeNodeConfig, (treeNode, tree) -> {
            tree.setId((Integer) treeNode.getId());
            tree.setParentId((Integer) treeNode.getParentId());
            tree.setName((String) treeNode.getName());
            //扩展属性
            tree.putExtra("disabled", !ObjUtil.equals(BeanStatus.ON.name(), treeNode.getStatus()));
        });

        return trees;
    }

    private void updateChildrenAncestors(EqDept bean) {
        // 查询所有直接下级
        List<EqDept> childrenList = this.easyEntityQuery
                // FROM sys_dept AS t
                .queryable(EqDept.class)
                // WHERE t.parent_id = {bean.id}
                .where(t -> t.parentId().eq(bean.getId()))
                .toList();

        // 更新下级的ancestors字段
        for (EqDept item : childrenList) {
            item.setAncestors(bean.getAncestors() + "," + item.getParentId());
            this.easyEntityQuery.updatable(item).executeRows();

            // 递归更新再下一级
            this.updateChildrenAncestors(item);
        }
    }
}
