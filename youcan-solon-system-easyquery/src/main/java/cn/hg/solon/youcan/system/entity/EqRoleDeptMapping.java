package cn.hg.solon.youcan.system.entity;

import cn.hg.solon.youcan.system.entity.proxy.EqConfigProxy;
import cn.hg.solon.youcan.system.entity.proxy.EqRoleDeptMappingProxy;
import com.easy.query.core.annotation.Column;
import com.easy.query.core.annotation.EntityProxy;
import com.easy.query.core.annotation.Table;
import com.easy.query.core.proxy.ProxyEntityAvailable;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table("sys_role_dept_mapping")
@EntityProxy
public class EqRoleDeptMapping extends RoleDeptMapping implements ProxyEntityAvailable<EqRoleDeptMapping , EqRoleDeptMappingProxy> {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -5627406524145620321L;

    /**
     * ID
     */
    @Column(primaryKey = true, generatedKey = true)
    private Integer id;

    /**
     * 角色ID，关联sys_role表的ID
     */
    private Integer roleId;

    /**
     * 部门ID，关联sys_dept表的ID
     */
    private Integer deptId;

    @Override
    public Integer getDeptId() {
        return this.deptId;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public Integer getRoleId() {
        return this.roleId;
    }

    @Override
    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

}
