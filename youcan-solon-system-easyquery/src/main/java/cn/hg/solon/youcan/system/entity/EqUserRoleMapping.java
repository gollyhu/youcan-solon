package cn.hg.solon.youcan.system.entity;

import cn.hg.solon.youcan.system.entity.proxy.EqConfigProxy;
import cn.hg.solon.youcan.system.entity.proxy.EqUserRoleMappingProxy;
import com.easy.query.core.annotation.Column;
import com.easy.query.core.annotation.EntityProxy;
import com.easy.query.core.annotation.Table;
import com.easy.query.core.proxy.ProxyEntityAvailable;

/**
 *  实体类。
 *
 * @author 胡高
 * @since 2023-06-20
 */
@Table("sys_user_role_mapping")
@EntityProxy
public class EqUserRoleMapping extends UserRoleMapping implements ProxyEntityAvailable<EqUserRoleMapping , EqUserRoleMappingProxy> {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7122912059474989060L;

    /**
     * ID
     */
    @Column(primaryKey = true, generatedKey = true)
    private Integer id;

    /**
     * 用户ID，关联sys_user表的ID
     */
    private Integer userId;

    /**
     * 角色ID，关联sys_role表的ID
     */
    private Integer roleId;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public Integer getRoleId() {
        return this.roleId;
    }

    @Override
    public Integer getUserId() {
        return this.userId;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Override
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
