package cn.hg.solon.youcan.system.provider;

import cn.hg.solon.youcan.common.exception.ServiceException;
import cn.hg.solon.youcan.easyquery.util.EntityQueryableUtil;
import cn.hg.solon.youcan.system.entity.EqPosition;
import cn.hg.solon.youcan.system.entity.Position;
import cn.hg.solon.youcan.system.entity.proxy.EqPositionProxy;
import cn.hg.solon.youcan.system.service.PositionService;
import com.easy.query.api.proxy.client.EasyEntityQuery;
import com.easy.query.api.proxy.entity.select.EntityQueryable;
import com.easy.query.core.api.pagination.EasyPageResult;
import com.easy.query.solon.annotation.Db;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.bean.copier.CopyOptions;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Component;
import org.noear.solon.data.annotation.Tran;

import java.util.List;
import java.util.Map;

/**
 * @author 胡高
 */
@Component
public class EqPositionProvider implements PositionService {

    @Db("db1")
    private EasyEntityQuery easyEntityQuery;

    private EntityQueryable<EqPositionProxy, EqPosition> buildQuery(Page page, Map<String, Object> paraMap) {
        String word = (String) paraMap.get("word");
        String status = (String) paraMap.get("status");

        EntityQueryable<EqPositionProxy, EqPosition> entityQueryable = this.easyEntityQuery
                // FROM sys_notice AS t
                .queryable(EqPosition.class)
                // WHERE t.`status` = ${status} AND (t.`name` LIKE '%${word}%' OR t.`code` LIKE '%${word}%'')
                .where(t -> {
                    t.status().eq(StrValidator.isNotBlank(status), status);
                    t.or(StrValidator.isNotBlank(word), () -> {
                        t.name().like(word);
                        t.code().like(word);
                    });
                });

        // ORDER BY ${sortField}
        return EntityQueryableUtil.applyOrderBy(entityQueryable, page);
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.PositionService#checkUnique(cn.hg.solon.youcan.system.entity.Position)
     */
    @Override
    public boolean checkUnique(Position bean) {
        return !this.easyEntityQuery
                // FROM sys_position AS t
                .queryable(EqPosition.class)
                // WHERE t.`code` = ${bean.code}
                .where(t -> {
                    t.code().eq(bean.getCode());
                    // AND t.`id` <> ${bean.id}
                    t.id().ne(ObjUtil.isNotNull(bean.getId()), bean.getId());
                })
                .any();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.PositionService#delete(java.util.List)
     */
    @Tran
    @Override
    public boolean delete(List<Integer> idList) {
        return this.easyEntityQuery.deletable(EqPosition.class).where(t -> t.id().in(idList)).executeRows() > 0L;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.PositionService#get(java.lang.Integer)
     */
    @Override
    public EqPosition get(Integer id) {
        return this.easyEntityQuery.queryable(EqPosition.class).whereById(id).firstOrNull();
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.PositionService#insert(cn.hg.solon.youcan.system.entity.Position)
     */
    @Tran
    @Override
    public boolean insert(Position bean) {
        if (!this.checkUnique(bean)) {
            throw new ServiceException("岗位编码已经存在，请更换其它值！");
        }

        EqPosition cloneBean = BeanUtil.copyProperties(bean, EqPosition.class);

        return this.easyEntityQuery.insertable(cloneBean).executeRows() > 0L;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.PositionService#pageBy(int, int, java.util.Map)
     */
    @Override
    public PageResult<EqPosition> pageBy(Page page, Map<String, Object> paraMap) {
        EasyPageResult<EqPosition> pageList = this.buildQuery(page, paraMap).toPageResult(page.getPageNumber(), page.getPageSize());

        PageResult<EqPosition> result = new PageResult<>();
        result.addAll(pageList.getData());
        result.setTotal(ConvertUtil.toInt(pageList.getTotal()));

        return result;
    }

    /* (non-Javadoc)
     * @see cn.hg.solon.youcan.system.service.PositionService#update(cn.hg.solon.youcan.system.entity.Position)
     */
    @Tran
    @Override
    public boolean update(Position bean) {
        if (!this.checkUnique(bean)) {
            throw new ServiceException("岗位编码已经存在，请更换其它值！");
        }

        EqPosition cloneBean = this.get(bean.getId());

        BeanUtil.copyProperties(bean, cloneBean, CopyOptions.of().setIgnoreNullValue(true));

        return this.easyEntityQuery.updatable(cloneBean).executeRows() > 0L;
    }

}
