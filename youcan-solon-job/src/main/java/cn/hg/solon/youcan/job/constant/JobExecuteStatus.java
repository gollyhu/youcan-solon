package cn.hg.solon.youcan.job.constant;

/**
 * @author 胡高
 */
public enum JobExecuteStatus {
    /*
     * 成功
     */
    SUCCESS,

    /*
     * 失败
     */
    FAIL
}
