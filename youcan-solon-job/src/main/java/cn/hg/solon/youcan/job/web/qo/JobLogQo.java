package cn.hg.solon.youcan.job.web.qo;

import cn.hg.solon.youcan.framework.web.qo.QueryObject;

import java.util.Date;

/**
 * @author 胡高
 */
public class JobLogQo extends QueryObject {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1450305055543496243L;

    private String group;

    private Date startDate;

    private Date endDate;

    private String status;

    public Date getEndDate() {
        return this.endDate;
    }

    public String getGroup() {
        return this.group;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public String getStatus() {
        return this.status;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
