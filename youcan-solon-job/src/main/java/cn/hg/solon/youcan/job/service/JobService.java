package cn.hg.solon.youcan.job.service;

import cn.hg.solon.youcan.common.exception.ServiceException;
import cn.hg.solon.youcan.job.entity.Job;
import org.dromara.hutool.db.Page;
import org.dromara.hutool.db.PageResult;

import java.util.List;
import java.util.Map;

/**
 * 定时任务服务
 *
 * @author 胡高
 */
public interface JobService {

    /**
     * 检测唯一性
     *
     * @param bean 对象
     * @return true：唯一，false：不唯一
     */
    boolean checkUnique(Job bean);

    /**
     * 通过ID删除对象
     *
     * @param idList ID列表
     * @return true：成功，false：失败
     */
    boolean delete(List<Integer> idList);

    /**
     * 停止任务服务
     *
     * @return true：成功，false：失败
     */
    boolean destroy();

    /**
     * 通过ID获取对象
     *
     * @param id ID值
     * @return 对象
     */
    Job get(Integer id);

    /**
     * 初始化定时任务
     * <p>启动所有状态为启用的任务</p>
     *
     * @throws ServiceException 业务异常
     */
    void init();

    /**
     * 插入对象
     *
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean insert(Job bean);

    /**
     * 通过参数Map获取对象分页结果集
     *
     * @param page 分页对象
     * @param paraMap 参数Map
     * @return 分页数据结果集
     */
    PageResult<? extends Job> pageBy(Page page, Map<String, Object> paraMap);

    /**
     * 暂停任务
     *
     * @param bean 任务对象
     * @return true：成功，false：失败
     */
    boolean pause(Job bean);

    /**
     * 回复任务
     *
     * @param bean 任务对象
     * @return true：成功，false：失败
     */
    boolean resume(Job bean);

    /**
     * 测试任务
     *
     * @param bean 任务对象
     * @return true：成功，false：失败
     */
    boolean test(Job bean);

    /**
     * 更新对象
     *
     * @param bean 对象
     * @return true：成功，false：失败
     */
    boolean update(Job bean);

}