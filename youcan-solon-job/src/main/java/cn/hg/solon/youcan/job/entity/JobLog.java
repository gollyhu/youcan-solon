package cn.hg.solon.youcan.job.entity;

import java.util.Date;

import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;

import cn.hg.solon.youcan.common.enums.BusinessStatus;
import cn.hg.solon.youcan.common.validate.UpdateLabel;
import cn.hg.solon.youcan.common.vo.BaseVo;

/**
 * 定时任务日志
 * 
 * @author 胡高
 */
public class JobLog extends BaseVo {

    private static final long serialVersionUID = -3536035099347429093L;

    /**
     * ID
     */
    @NotNull(groups = UpdateLabel.class, message = "必须输入ID")
    private Integer id;

    /**
     * 任务名称
     */
    @NotEmpty(message = "必须输入任务名称")
    @Length(min = 2, max = 64, message = "任务名称必须 2 到 64 字符")
    private String name;

    /**
     * 任务组名
     */
    @NotEmpty(message = "必须输入任务组名")
    @Length(min = 2, max = 20, message = "任务组名必须 2 到 20 字符")
    private String group;

    /**
     * 调用目标字符串
     */
    @NotEmpty(message = "必须输入目标字符")
    @Length(min = 1, max = 1024, message = "目标字符必须 1 到 1024 字符")
    private String target;

    /**
     * 日志信息
     */
    private String message;

    /**
     * 任务状态：SUCCESS 成功，FAIL 失败
     */
    private String status = BusinessStatus.SUCCESS.name();

    /**
     * 异常信息
     */
    private String exception;

    /**
     * 执行时间（ms）
     */
    private Long cost;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 开始时间
     */
    private Date startedDatetime;

    /**
     * 结束时间
     */
    private Date endedDatetime;

    public Long getCost() {
        return this.cost;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public Date getEndedDatetime() {
        return this.endedDatetime;
    }

    public String getException() {
        return this.exception;
    }

    public String getGroup() {
        return this.group;
    }

    public Integer getId() {
        return this.id;
    }

    public String getMessage() {
        return this.message;
    }

    public String getName() {
        return this.name;
    }

    public Date getStartedDatetime() {
        return this.startedDatetime;
    }

    public String getStatus() {
        return this.status;
    }

    public String getTarget() {
        return this.target;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public void setEndedDatetime(Date endedDatetime) {
        this.endedDatetime = endedDatetime;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStartedDatetime(Date startedDatetime) {
        this.startedDatetime = startedDatetime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTarget(String target) {
        this.target = target;
    }

}
