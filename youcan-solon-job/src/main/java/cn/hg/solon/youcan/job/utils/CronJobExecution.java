package cn.hg.solon.youcan.job.utils;

import cn.hg.solon.youcan.job.entity.Job;

/**
 * 定时任务处理（允许并发执行）
 * 
 * @author ruoyi
 *
 */
public class CronJobExecution extends AbstractCronJob {
    public CronJobExecution(Job job) {
        super(job);
    }

    @Override
    protected Object doExecute() throws Throwable {
        return JobInvokeUtil.invokeMethod(this.getJob());
    }

}
