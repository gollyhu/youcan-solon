package cn.hg.solon.youcan.job.web.qo;

import cn.hg.solon.youcan.framework.web.qo.QueryObject;

/**
 * @author 胡高
 */
public class JobQo extends QueryObject {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1450305055543496243L;

    private String group;

    public String getGroup() {
        return this.group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

}
