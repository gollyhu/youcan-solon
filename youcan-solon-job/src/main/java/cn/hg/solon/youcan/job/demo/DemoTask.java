package cn.hg.solon.youcan.job.demo;

import org.dromara.hutool.core.text.CharSequenceUtil;
import org.noear.solon.annotation.Component;

/**
 * 定时任务调度测试
 * 
 * @author ruoyi
 */
@Component(name = "demoTask")
public class DemoTask {
    public void exception() {
        System.out.println("执行异常方法");
        throw new RuntimeException("此异常由cn.hg.jbsa.job.demo.DemoTask.exception()方法抛出");
    }

    public void multipleParams(String s, Boolean b, Long l, Double d, Integer i) {
        System.out.println(CharSequenceUtil.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void noParams() {
        System.out.println("执行无参方法");
    }

    public void params(String params) {
        System.out.println("执行有参方法：" + params);
    }
}
