package cn.hg.solon.youcan.job.entity;

import java.util.Date;

import org.noear.solon.validation.annotation.Length;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;

import cn.hg.solon.youcan.common.enums.BeanStatus;
import cn.hg.solon.youcan.common.validate.UpdateLabel;
import cn.hg.solon.youcan.common.vo.BaseVo;


/**
 * 定时任务
 * 
 * @author 胡高
 */
public class Job extends BaseVo {

    private static final long serialVersionUID = 6034411829263810854L;

    /**
     * ID
     */
    @NotNull(groups = UpdateLabel.class, message = "必须输入ID")
    private Integer id;

    /**
     * 任务名称
     */
    @NotEmpty(message = "必须输入任务名称")
    @Length(min = 2, max = 64, message = "任务名称必须 2 到 64 字符")
    private String name;

    /**
     * 任务组名
     */
    @NotEmpty(message = "必须输入任务组名")
    @Length(min = 2, max = 20, message = "任务组名必须 2 到 20 字符")
    private String group;

    /**
     * 调用目标字符串
     */
    @NotEmpty(message = "必须输入目标字符")
    @Length(min = 1, max = 1024, message = "目标字符必须 1 到 1024 字符")
    private String target;

    /**
     * cron执行表达式
     */
    @NotEmpty(message = "必须输入执行表达式")
    @Length(min = 1, max = 255, message = "执行表达式必须 1 到 255 字符")
    private String cron;

    /**
     * 计划执行错误策略（IMMEDIATELY 立即执行，ONCE 执行一次，ABORT 放弃执行）
     */
    @NotEmpty(message = "必须输入任务组名")
    @Length(min = 2, max = 20, message = "任务组名必须 2 到 20 字符")
    private String policy = "ABORT";

    /**
     * 是否并发执行
     */
    private Boolean isConcurrent = Boolean.FALSE;

    /**
     * 是否记录调度日志
     */
    private Boolean isLog = Boolean.TRUE;

    /**
     * 任务状态（ON 启用, OFF 停用）
     */
    private String status = BeanStatus.OFF.name();

    /**
     * 创建者
     */
    private Integer creator;

    /**
     * 创建时间
     */
    private Date createdDatetime;

    /**
     * 更新者
     */
    private Integer editor;

    /**
     * 更新时间
     */
    private Date editedDatetime;

    /**
     * 备注
     */
    @Length(min = 0, max = 500, message = "备注必须 500 字符以内")
    private String remark;

    public Boolean getIsConcurrent() {
        return this.isConcurrent;
    }

    public Date getCreatedDatetime() {
        return this.createdDatetime;
    }

    public Integer getCreator() {
        return this.creator;
    }

    public String getCron() {
        return this.cron;
    }

    public Date getEditedDatetime() {
        return this.editedDatetime;
    }

    public Integer getEditor() {
        return this.editor;
    }

    public String getGroup() {
        return this.group;
    }

    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getPolicy() {
        return this.policy;
    }

    public String getRemark() {
        return this.remark;
    }

    public String getStatus() {
        return this.status;
    }

    public String getTarget() {
        return this.target;
    }

    public void setIsConcurrent(Boolean isConcurrent) {
        this.isConcurrent = isConcurrent;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public void setEditedDatetime(Date editedDatetime) {
        this.editedDatetime = editedDatetime;
    }

    public void setEditor(Integer editor) {
        this.editor = editor;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Boolean getIsLog() {
        return isLog;
    }

    public void setIsLog(Boolean log) {
        isLog = log;
    }
}
