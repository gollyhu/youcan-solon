package cn.hg.solon.youcan.job.constant;

/**
 * @author 胡高
 */
public enum JobStatus {
    /**
     * 启用
     */
    ON,

    /**
     * 停用
     */
    OFF
}
