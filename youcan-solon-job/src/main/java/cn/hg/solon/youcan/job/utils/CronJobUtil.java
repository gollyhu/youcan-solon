package cn.hg.solon.youcan.job.utils;

import org.dromara.hutool.core.convert.ConvertUtil;

import cn.hg.solon.youcan.job.entity.Job;


/**
 * @author 胡高
 */
public class CronJobUtil {

    public static String genTaskKey(Job job) {
        return ConvertUtil.toStr(job.getId());
    }

}
