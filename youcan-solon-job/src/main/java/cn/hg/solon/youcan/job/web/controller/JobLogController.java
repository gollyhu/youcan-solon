package cn.hg.solon.youcan.job.web.controller;


import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hg.solon.youcan.common.enums.BusinessType;
import cn.hg.solon.youcan.framework.web.admin.annotation.Logging;
import cn.hg.solon.youcan.framework.web.pear.LayuiPage;
import cn.hg.solon.youcan.framework.web.qo.PaginationQueryObject;
import cn.hg.solon.youcan.job.entity.JobLog;
import cn.hg.solon.youcan.job.service.JobLogService;
import cn.hg.solon.youcan.job.web.qo.JobLogQo;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.date.DateUtil;
import org.dromara.hutool.core.text.split.SplitUtil;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;
import org.noear.solon.core.handle.ModelAndView;
import org.noear.solon.core.handle.Result;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

import java.util.Date;

/**
 * @author 胡高
 */
@Controller
@Mapping("/admin/monitor/jobLog")
public class JobLogController extends BaseJobController {

    private static final String SERVICE_NAME = "调度日志";

    private static final String VIEW_PATH = "/admin/monitor/jobLog/";

    @Inject
    JobLogService jobLogService;


    @Mapping(path = "/clear", method = MethodType.DELETE)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.CLEAR)
    @SaCheckPermission(value = {"monitor:jobLog:del"})
    public Result<?> clear(Context ctx, JobLogQo query) {
        this.log.info("清空[{}]记录：query={}", SERVICE_NAME, query);

        return this.jobLogService.clear() ? Result.succeed() : Result.failure();
    }

    @Mapping(path = "/delete", method = MethodType.DELETE)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.DELETE)
    @SaCheckPermission(value = {"monitor:jobLog:del"})
    public Result<?> delete(Context ctx, @NotBlank String ids) {
        this.log.info("删除[{}]记录：ids={}", SERVICE_NAME, ids);

        return this.jobLogService.delete(ConvertUtil.toList(Integer.class, SplitUtil.split(ids, ",")))
                ? Result.succeed()
                : Result.failure();
    }


    /**
     * 跳转到记录展示页面
     */
    @Mapping(path = "/detail", method = MethodType.GET)
    @SaCheckPermission(value = {"monitor:jobLog:query"})
    public ModelAndView detail(Context ctx, @NotNull Integer id) {
        this.log.info("跳转到[{}]展示页面：id={}", SERVICE_NAME, id);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "detail.html");

        mav.put("bean", this.jobLogService.get(id));

        return mav;
    }

    @Mapping(path = "", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView index(Context ctx) {
        this.log.info("跳转到[{}]列表页面", SERVICE_NAME);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "index.html");

        // 如果打开列表页面时有参数传递过来，则继续
        mav.putAll(ctx.paramMap().toValueMap());

        return mav;
    }

    private void prepare(JobLogQo query) {
        Date start = query.getStartDate();
        Date end = query.getEndDate();

        query.setStartDate(ObjUtil.isNull(start) ? null : DateUtil.beginOfDay(start));
        query.setEndDate(ObjUtil.isNull(end) ? null : DateUtil.endOfDay(end, false));
    }

    /**
     * 分页查询功能
     */
    @Mapping(path = "/query", method = MethodType.POST)
    @SaCheckPermission(value = {"monitor:jobLog:query"})
    public LayuiPage<? extends JobLog> query(Context ctx, @NotNull JobLogQo query, PaginationQueryObject page) {

        this.log.info("执行[{}]分页查询：query={}", SERVICE_NAME, query);

        this.prepare(query);

        /*
         * 服务调用
         */
        PageResult<? extends JobLog> result = this.jobLogService.pageBy(page, query.toMap());

        /*
         * 返回值处理
         */
        return new LayuiPage<>(result, result.getTotal());
    }

}
