package cn.hg.solon.youcan.web.system.controller;

import cn.hg.solon.youcan.framework.web.qo.PaginationQueryObject;
import cn.hg.solon.youcan.web.system.qo.DictItemQo;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.collection.ListUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.text.split.SplitUtil;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;
import org.noear.solon.core.handle.ModelAndView;
import org.noear.solon.core.handle.Result;
import org.noear.solon.data.annotation.Cache;
import org.noear.solon.data.annotation.CacheRemove;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;
import org.noear.solon.validation.annotation.Validated;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hg.solon.youcan.common.constant.CacheConstants;
import cn.hg.solon.youcan.common.enums.BusinessType;
import cn.hg.solon.youcan.common.validate.UpdateLabel;
import cn.hg.solon.youcan.framework.web.pear.LayuiPage;
import cn.hg.solon.youcan.framework.web.admin.annotation.Logging;
import cn.hg.solon.youcan.system.entity.DictItem;
import cn.hg.solon.youcan.system.service.DictItemService;
import cn.hg.solon.youcan.web.BaseAdminController;

/**
 * @author 胡高
 */
@Controller
@Mapping("/admin/system/dictItem")
public class DictItemController extends BaseAdminController {

    private static final String SERVICE_NAME = "字典子项";

    private static final String VIEW_PATH = "/admin/system/dictItem/";

    @Inject
    private DictItemService dictItemService;

    @Mapping(path = "add", method = MethodType.POST)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.CREATE)
    @SaCheckPermission(value = {"system:dict:add"})
    @CacheRemove(tags = CacheConstants.SYS_DICT_TAG)
    public Result<?> add(Context ctx, @NotNull @Validated DictItem bean) {
        this.log.info("新增[{}]记录：bean={}", SERVICE_NAME, bean);

        // 新建记录到数据库
        return this.dictItemService.insert(bean) ? Result.succeed() : Result.failure();
    }

    /**
     * 跳转到新增页面
     */
    @Mapping(path = "add", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView add(Context ctx, @NotBlank String type) {
        this.log.info("跳转到新增[{}]页面", SERVICE_NAME);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "add.html");

        DictItem bean = new DictItem();
        bean.setType(type);

        mav.put("bean", bean);

        return mav;
    }

    @Mapping(path = "delete", method = MethodType.DELETE)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.DELETE)
    @SaCheckPermission(value = {"system:dict:del"})
    @CacheRemove(tags = CacheConstants.SYS_DICT_TAG)
    public Result<?> delete(Context ctx, @NotNull String ids) {
        this.log.info("删除[{}]记录：ids={}", SERVICE_NAME, ids);

        return this.dictItemService.delete(StrValidator.isBlank(ids) ? ListUtil.empty() : ConvertUtil.toList(Integer.class, SplitUtil.split(ids, ","))) ? Result.succeed() : Result.failure();
    }

    /**
     * 跳转到记录展示页面
     */
    @Mapping(path = "detail", method = MethodType.GET)
    @SaCheckPermission(value = {"system:dict:query"})
    public ModelAndView detail(Context ctx, int id) {
        this.log.info("跳转到[{}]展示页面：id={}", SERVICE_NAME, id);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "detail.html");

        mav.put("bean", this.dictItemService.get(id));

        return mav;
    }

    @Mapping(path = "edit", method = MethodType.PUT)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.UPDATE)
    @SaCheckPermission(value = {"system:dict:edit"})
    @CacheRemove(tags = CacheConstants.SYS_DICT_TAG)
    public Result<?> edit(Context ctx, @NotNull @Validated(UpdateLabel.class) DictItem bean) {
        this.log.info("更新[{}]记录：bean={}", SERVICE_NAME, bean);

        // 更新到数据库
        return this.dictItemService.update(bean) ? Result.succeed() : Result.failure();
    }

    /**
     * 跳转到编辑页面
     */
    @Mapping(path = "edit", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView edit(Context ctx, int id) {
        this.log.info("跳转到编辑[{}]页面, id={}", SERVICE_NAME, id);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "edit.html");

        mav.put("bean", this.dictItemService.get(id));

        return mav;
    }

    @Mapping(path = "", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView index(Context ctx, @NotBlank String type) {
        this.log.info("跳转到[{}]列表页面", SERVICE_NAME);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "index.html");

        mav.put("type", type);

        return mav;
    }

    /**
     * 分页查询功能
     */
    @Mapping(path = "query", method = MethodType.POST)
    @SaCheckPermission(value = {"system:dict:query"})
    @Cache(key = "item-query:${query}-${page}", tags = CacheConstants.SYS_DICT_TAG, seconds = CacheConstants.CACHE_SECONDS_ONE_HOUR)
    public LayuiPage<? extends DictItem> query(Context ctx, DictItemQo query, PaginationQueryObject page) {
        this.log.info("执行[{}]分页查询：query={}", SERVICE_NAME, query);

        /*
         * 服务调用
         */
        PageResult<? extends DictItem> result = this.dictItemService.pageBy(page, BeanUtil.beanToMap(query));

        /*
         * 返回值处理
         */
        return new LayuiPage<>(result, result.getTotal());
    }

}
