package cn.hg.solon.youcan.web.common;

import cn.hg.solon.youcan.common.util.ContextPathUtil;
import org.dromara.hutool.core.date.DateUtil;
import org.dromara.hutool.core.net.NetUtil;
import org.dromara.hutool.core.thread.ThreadUtil;
import org.noear.solon.Solon;
import org.noear.solon.SolonProps;
import org.noear.solon.annotation.Component;
import org.noear.solon.core.event.AppLoadEndEvent;
import org.noear.solon.core.event.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class SystemStartupPrint implements EventListener<AppLoadEndEvent> {
    protected Logger log = LoggerFactory.getLogger(SystemStartupPrint.class);

    @Override
    public void onEvent(AppLoadEndEvent event) throws Throwable {

        log.info("启动完毕...");

        ThreadUtil.execute(() -> {
            final SolonProps cfg = Solon.cfg();
            final String appName = cfg.appName();
            final String env = cfg.env();
            final String contextPath = ContextPathUtil.getContextPath();
            final String port = cfg.get("server.port");

            List<String> ipList = getIps();

            final StringBuilder stringBuilder = new StringBuilder(
                    "\n------------- " + appName + " (" + env + ") 启动成功 --by " + DateUtil.now() + " -------------\n");
            stringBuilder.append("\t主页访问: --\n");
            stringBuilder.append("\t\t- 访问: http://")
                    .append("localhost")
                    .append(":")
                    .append(port);
            if (contextPath != null && !contextPath.isEmpty()) {
                stringBuilder.append(contextPath);
            }
            stringBuilder.append("\n");
            for (String ip : ipList) {
                stringBuilder.append("\t\t- 访问: http://")
                        .append(ip)
                        .append(":")
                        .append(port);
                if (contextPath != null && !contextPath.isEmpty()) {
                    stringBuilder.append(contextPath);
                }
                stringBuilder.append("\n");
            }
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            System.out.println(stringBuilder);
        });
    }

    private static List<String> getIps() {
        List<String> ipList = new ArrayList<>();
        for (String localIpv4 : NetUtil.localIpv4s()) {
            ipList.add(localIpv4);
        }

        return ipList;
    }

}
