package cn.hg.solon.youcan;

import cn.hg.solon.youcan.common.constant.WebConstants;
import cn.hg.solon.youcan.common.util.RequestUtil;
import cn.hg.solon.youcan.framework.cache.interceptor.CacheTagsInterceptor;
import cn.hg.solon.youcan.framework.cache.interceptor.CacheTagsPutInterceptor;
import cn.hg.solon.youcan.framework.cache.interceptor.CacheTagsRemoveInterceptor;
import com.jfinal.template.Engine;
import org.dromara.hutool.http.meta.HttpStatus;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.SolonMain;
import org.noear.solon.core.event.AppPluginLoadEndEvent;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;
import org.noear.solon.core.handle.ModelAndView;
import org.noear.solon.core.handle.Result;
import org.noear.solon.data.annotation.Cache;
import org.noear.solon.data.annotation.CachePut;
import org.noear.solon.data.annotation.CacheRemove;

/**
 * @author 胡高
 */
@Controller
@SolonMain
public class YoucanSolonApp {
    /**
     * App引导方法
     */
    public static void main(String[] args) {
        Solon.start(YoucanSolonApp.class, args, app -> {
            //::在 Solon.start 后实例化，否则格式可能不统一（因为配置未加完好）
            //Logger log = LoggerFactory.getLogger(YoucanSolonApp.class);

            // 订阅Enjoy模板引擎初始化事件
            app.onEvent(Engine.class, eng -> {
                if (Solon.cfg().isDebugMode() || Solon.cfg().isFilesMode()) {
                    eng.setDevMode(true); // 启用开发模式，此行放前面
                }
            });

            //            /*
            //             * 处理未处理过的其它异常
            //             */
            //            app.onError(e -> {
            //                //::转到日志器里
            //                log.error(e.getMessage(), e);
            //            });

            /*
             * 处理未处理过的404异常
             */
            app.onStatus(HttpStatus.HTTP_NOT_FOUND, c -> {
                if (c.getRendered()) {
                    return;
                }
                if (RequestUtil.isAjaxRequest(c)) {
                    c.renderAndReturn(Result.failure(HttpStatus.HTTP_NOT_FOUND));
                } else {
                    c.render(WebConstants.ERROR_PAGE_404, null);
                }
            });

            /*
             * 处理未处理过的500异常
             */
            app.onStatus(HttpStatus.HTTP_INTERNAL_ERROR, c -> {
                if (c.getRendered()) {
                    return;
                }
                if (RequestUtil.isAjaxRequest(c)) {
                    c.renderAndReturn(Result.failure(HttpStatus.HTTP_INTERNAL_ERROR));
                } else {
                    c.render(WebConstants.ERROR_PAGE_500, null);
                }
            });

            app.onEvent(AppPluginLoadEndEvent.class, e -> {
                app.context().beanInterceptorAdd(CachePut.class, new CacheTagsPutInterceptor(), 110);
                app.context().beanInterceptorAdd(CacheRemove.class, new CacheTagsRemoveInterceptor(), 110);
                app.context().beanInterceptorAdd(Cache.class, new CacheTagsInterceptor(), 111);
            });
        });
    }

    @Mapping("/")
    public ModelAndView index() {
        return new ModelAndView("/index.html");
    }

    @Mapping(path = "/pear", method = MethodType.GET)
    public void index(Context ctx) {
        ctx.redirect("/pear/index.html");
    }

}
