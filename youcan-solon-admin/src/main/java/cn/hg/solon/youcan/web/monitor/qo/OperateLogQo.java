package cn.hg.solon.youcan.web.monitor.qo;

import cn.hg.solon.youcan.framework.web.qo.QueryObject;

import java.util.Date;

/**
 * @author 胡高
 */
public class OperateLogQo extends QueryObject {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1450305055543496243L;

    private String type;

    private String method;

    private String status;

    private Date startDate;

    private Date endDate;

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return this.endDate;
    }

    public String getMethod() {
        return this.method;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return this.startDate;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return this.status;
    }

    public String getType() {
        return this.type;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    public void setType(String type) {
        this.type = type;
    }

}
