package cn.hg.solon.youcan.web.monitor.qo;

import cn.hg.solon.youcan.framework.web.qo.QueryObject;

/**
 * @author 胡高
 */
public class UserLoginQo extends QueryObject {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1450305055543496243L;

    private String status;

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
