package cn.hg.solon.youcan.web.system.qo;

import cn.hg.solon.youcan.framework.web.qo.QueryObject;

/**
 * @author 胡高
 */
public class PermissionQo extends QueryObject {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8348369460935172425L;

    private String type;

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
