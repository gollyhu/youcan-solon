package cn.hg.solon.youcan.web.system.qo;

import cn.hg.solon.youcan.framework.web.qo.QueryObject;

/**
 * @author 胡高
 */
public class NoticeQo extends QueryObject {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -2017154046697805051L;

    private String status;

    private String type;

    public String getStatus() {
        return this.status;
    }

    public String getType() {
        return this.type;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setType(String type) {
        this.type = type;
    }

}
