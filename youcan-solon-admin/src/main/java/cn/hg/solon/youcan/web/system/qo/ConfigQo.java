package cn.hg.solon.youcan.web.system.qo;

import cn.hg.solon.youcan.framework.web.qo.QueryObject;

/**
 * @author 胡高
 */
public class ConfigQo extends QueryObject {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1450305055543496243L;

    private String type;

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
