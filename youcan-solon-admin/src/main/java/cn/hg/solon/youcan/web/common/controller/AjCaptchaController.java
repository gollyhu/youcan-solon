package cn.hg.solon.youcan.web.common.controller;

import org.dromara.hutool.core.util.RandomUtil;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;

import com.anji.captcha.model.common.ResponseModel;
import com.anji.captcha.model.vo.CaptchaVO;
import com.anji.captcha.service.CaptchaService;

/**
 * @author 胡高
 */
@Controller
@Mapping("/aj-captcha")
public class AjCaptchaController {

    @Inject
    CaptchaService captchaService;

    @Mapping(path = "/captcha/check", method = MethodType.POST)
    public ResponseModel checkCaptcha(Context ctx, CaptchaVO captchaVO) {
        return this.captchaService.check(captchaVO);
    }

    @Mapping(path = "/captcha/get", method = MethodType.POST)
    public ResponseModel getCaptcha(Context ctx, CaptchaVO captchaVO) {
        captchaVO.setCaptchaId(RandomUtil.randomString(5));
        return this.captchaService.get(captchaVO);
    }

}
