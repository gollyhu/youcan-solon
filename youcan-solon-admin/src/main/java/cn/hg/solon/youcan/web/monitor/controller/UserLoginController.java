package cn.hg.solon.youcan.web.monitor.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hg.solon.youcan.common.enums.BusinessType;
import cn.hg.solon.youcan.framework.web.admin.annotation.Logging;
import cn.hg.solon.youcan.framework.web.pear.LayuiPage;
import cn.hg.solon.youcan.framework.web.qo.PaginationQueryObject;
import cn.hg.solon.youcan.system.entity.UserLogin;
import cn.hg.solon.youcan.system.service.UserLoginService;
import cn.hg.solon.youcan.web.BaseAdminController;
import cn.hg.solon.youcan.web.monitor.qo.OperateLogQo;
import cn.hg.solon.youcan.web.monitor.qo.UserLoginQo;
import org.dromara.hutool.core.collection.ListUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.text.split.SplitUtil;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;
import org.noear.solon.core.handle.ModelAndView;
import org.noear.solon.core.handle.Result;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

/**
 * @author 胡高
 */
@Controller
@Mapping("/admin/monitor/login")
public class UserLoginController extends BaseAdminController {

    private static final String SERVICE_NAME = "登录日志";

    private static String VIEW_PATH = "/admin/monitor/login/";

    @Inject
    private UserLoginService userLoginService;

    @Mapping(path = "clear", method = MethodType.DELETE)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.CLEAR)
    @SaCheckPermission(value = {"monitor:operate:clear"})
    public Result<?> clear(Context ctx, OperateLogQo query) {
        this.log.info("清除所有[{}]记录", SERVICE_NAME);

        this.userLoginService.clear();

        return Result.succeed();
    }

    @Mapping(path = "delete", method = MethodType.DELETE)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.DELETE)
    @SaCheckPermission(value = {"monitor:login:del"})
    public Result<?> delete(Context ctx, @NotBlank String ids) {
        this.log.info("删除[{}]记录：ids={}", SERVICE_NAME, ids);

        return this.userLoginService.delete(
                StrValidator.isBlank(ids) ? ListUtil.empty() : ConvertUtil.toList(Integer.class, SplitUtil.split(ids, ","))
        ) ? Result.succeed() : Result.failure();
    }

    /**
     * 跳转到记录展示页面
     */
    @Mapping(path = "detail", method = MethodType.GET)
    @SaCheckPermission(value = {"monitor:login:query"})
    public ModelAndView detail(Context ctx, @NotNull Integer id) {
        this.log.info("跳转到[{}]展示页面：id={}", SERVICE_NAME, id);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "detail.html");

        mav.put("bean", this.userLoginService.get(id));

        return mav;
    }

    @Mapping(path = "", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView index(Context ctx) {
        ModelAndView mav = new ModelAndView(VIEW_PATH + "index.html");

        return mav;
    }

    /**
     * 分页查询功能
     */
    @Mapping(path = "query", method = MethodType.POST)
    @SaCheckPermission(value = {"monitor:login:query"})
    public LayuiPage<? extends UserLogin> query(Context ctx, @NotNull UserLoginQo query, PaginationQueryObject page) {
        this.log.info("执行[{}]分页查询：query={}", SERVICE_NAME, query);

        /*
         * 服务调用
         */
        PageResult<? extends UserLogin> result = this.userLoginService.pageBy(page, query.toMap());

        /*
         * 返回值处理
         */
        return new LayuiPage<>(result, result.getTotal());
    }

}
