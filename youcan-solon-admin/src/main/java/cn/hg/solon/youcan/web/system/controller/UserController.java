package cn.hg.solon.youcan.web.system.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hg.solon.youcan.common.constant.CacheConstants;
import cn.hg.solon.youcan.common.enums.BusinessType;
import cn.hg.solon.youcan.common.validate.UpdateLabel;
import cn.hg.solon.youcan.framework.satoken.SaUtil;
import cn.hg.solon.youcan.framework.web.admin.annotation.Logging;
import cn.hg.solon.youcan.framework.web.pear.LayuiPage;
import cn.hg.solon.youcan.framework.web.qo.PaginationQueryObject;
import cn.hg.solon.youcan.system.entity.User;
import cn.hg.solon.youcan.system.service.RoleService;
import cn.hg.solon.youcan.system.service.UserService;
import cn.hg.solon.youcan.web.BaseAdminController;
import cn.hg.solon.youcan.web.system.qo.UserQo;
import org.dromara.hutool.core.collection.ListUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.text.split.SplitUtil;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;
import org.noear.solon.core.handle.ModelAndView;
import org.noear.solon.core.handle.Result;
import org.noear.solon.data.annotation.Cache;
import org.noear.solon.data.annotation.CacheRemove;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
 * @author 胡高
 */
@Controller
@Mapping("/admin/system/user")
public class UserController extends BaseAdminController {

    private static final String SERVICE_NAME = "系统用户";

    private static final String VIEW_PATH = "/admin/system/user/";

    @Inject
    private UserService userService;

    @Inject
    private RoleService roleService;

    /**
     * 跳转到新增页面
     */
    @Mapping(path = "add", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView add(Context ctx) {
        this.log.info("跳转到新增[{}]页面", SERVICE_NAME);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "add.html");

        this.preEdit(mav, null);

        return mav;
    }

    @Mapping(path = "add", method = MethodType.POST)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.CREATE)
    @SaCheckPermission(value = {"system:user:add"})
    @CacheRemove(tags = CacheConstants.SYS_USER_TAG + "," + CacheConstants.SYS_ROLE_TAG)
    public Result<?> add(Context ctx, @NotNull @Validated User bean, String roleIds) {
        this.log.info("新增[{}]记录：bean={}", SERVICE_NAME, bean);

        boolean ret = this.userService.insert(bean, StrValidator.isBlank(roleIds)
                ? ListUtil.empty()
                : ConvertUtil.toList(Integer.class, SplitUtil.split(roleIds, ",")));

        return ret ? Result.succeed() : Result.failure();
    }

    @Mapping(path = "assignRole", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView assignRole(Context ctx, @NotNull Integer id) {
        this.log.info("跳转到[{}]分配角色页面：id={}", SERVICE_NAME, id);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "assignRole.html");

        this.preEdit(mav, id);

        return mav;
    }

    @Mapping(path = "assignRole", method = MethodType.PUT)
    @Logging(title = "分配角色", businessType = BusinessType.GRANT)
    @SaCheckPermission(value = {"system:user:assignRole"})
    @CacheRemove(tags = CacheConstants.SYS_USER_TAG + "," + CacheConstants.SYS_ROLE_TAG)
    public Result<?> assignRole(Context ctx, @NotNull Integer id, String roleIds) {
        this.log.info("执行[{}]分配角色操作：id={}, roleIds={}", SERVICE_NAME, id, roleIds);

        this.userService.assignRole(this.userService.get(id), StrValidator.isBlank(roleIds)
                ? ListUtil.empty()
                : ConvertUtil.toList(Integer.class, SplitUtil.split(roleIds, ","))
        );

        return Result.succeed();
    }

    @Mapping(path = "delete", method = MethodType.DELETE)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.DELETE)
    @SaCheckPermission(value = {"system:user:del"})
    @CacheRemove(tags = CacheConstants.SYS_USER_TAG + "," + CacheConstants.SYS_ROLE_TAG)
    public Result<?> delete(Context ctx, @NotBlank String ids) {
        this.log.info("删除[{}]记录：ids={}", SERVICE_NAME, ids);

        List<Integer> idList =
                StrValidator.isBlank(ids) ? ListUtil.empty() : ConvertUtil.toList(Integer.class, SplitUtil.split(ids, ","));

        return this.userService.delete(idList) ? Result.succeed() : Result.failure();
    }

    /**
     * 跳转到记录展示页面
     */
    @Mapping(path = "detail", method = MethodType.GET)
    @SaCheckPermission(value = {"system:user:query"})
    public ModelAndView detail(Context ctx, int id) {
        this.log.info("跳转到[{}]展示页面：id={}", SERVICE_NAME, id);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "detail.html");

        mav.put("bean", this.userService.get(id));

        return mav;
    }

    /**
     * 跳转到编辑页面
     */
    @Mapping(path = "edit", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView edit(Context ctx, int id) {
        this.log.info("跳转到编辑[{}]页面", SERVICE_NAME);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "edit.html");

        this.preEdit(mav, id);

        return mav;
    }

    @Mapping(path = "edit", method = MethodType.PUT)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.UPDATE)
    @SaCheckPermission(value = {"system:user:edit"})
    @CacheRemove(tags = CacheConstants.SYS_USER_TAG)
    public Result<?> edit(Context ctx, @NotNull @Validated(UpdateLabel.class) User bean, String roleIds) {
        this.log.info("更新[{}]记录：bean={}", SERVICE_NAME, bean);

        // 更新到数据库
        this.userService.update(bean);

        return Result.succeed();
    }

    @Mapping(path = "", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView index(Context ctx) {
        this.log.info("跳转到[{}]列表页面", SERVICE_NAME);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "index.html");

        // 如果打开列表页面时有参数传递过来，则继续
        mav.putAll(ctx.paramMap().toValueMap());

        return mav;
    }

    private void preEdit(ModelAndView mav, Integer id) {
        mav.put("bean", ObjUtil.isNull(id) ? new User() : this.userService.get(id));
    }

    /**
     * 分页查询功能
     */
    @Mapping(path = "query", method = MethodType.POST)
    @SaCheckPermission(value = {"system:user:query"})
    @Cache(key = "query:${query}-${page}", tags = CacheConstants.SYS_USER_TAG, seconds = CacheConstants.CACHE_SECONDS_ONE_HOUR)
    public LayuiPage<? extends User> query(Context ctx, @NotNull UserQo query, PaginationQueryObject page) {
        this.log.info("执行[{}]分页查询：query={}", SERVICE_NAME, query);

        /*
         * 服务调用
         */
        PageResult<? extends User> result = this.userService.pageBy(page, query.toMap());

        /*
         * 返回值处理
         */
        return new LayuiPage<>(result, result.getTotal());
    }

    @Mapping(path = "resetPassword", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView resetPassword(Context ctx, @NotNull Integer id) {
        this.log.info("跳转到[{}]重置密码页面：id={}", SERVICE_NAME, id);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "resetPassword.html");

        mav.put("bean", this.userService.get(id));

        return mav;
    }

    @Mapping(path = "resetPassword", method = MethodType.PUT)
    @Logging(title = "重置密码", businessType = BusinessType.UPDATE)
    @SaCheckPermission(value = {"system:user:edit"})
    public Result<?> resetPassword(Context ctx, @NotNull Integer id, @NotEmpty String password) {
        this.log.info("执行[{}]重置密码操作：id={}", SERVICE_NAME, id);

        this.userService.resetPassword(this.userService.get(id), password, SaUtil.getCurrentUser());

        return Result.succeed();
    }
}
