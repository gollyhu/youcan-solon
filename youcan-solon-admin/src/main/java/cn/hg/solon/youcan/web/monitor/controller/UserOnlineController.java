package cn.hg.solon.youcan.web.monitor.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hg.solon.youcan.common.enums.BusinessType;
import cn.hg.solon.youcan.framework.service.LogoutService;
import cn.hg.solon.youcan.framework.web.admin.annotation.Logging;
import cn.hg.solon.youcan.framework.web.pear.LayuiPage;
import cn.hg.solon.youcan.framework.web.qo.PaginationQueryObject;
import cn.hg.solon.youcan.system.entity.UserOnline;
import cn.hg.solon.youcan.system.service.UserOnlineService;
import cn.hg.solon.youcan.web.BaseAdminController;
import cn.hg.solon.youcan.web.monitor.qo.UserOnlineQo;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.collection.ListUtil;
import org.dromara.hutool.core.text.StrValidator;
import org.dromara.hutool.core.text.split.SplitUtil;
import org.dromara.hutool.db.PageResult;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;
import org.noear.solon.core.handle.ModelAndView;
import org.noear.solon.core.handle.Result;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotNull;

/**
 * @author 胡高
 */
@Controller
@Mapping("/admin/monitor/online")
public class UserOnlineController extends BaseAdminController {

    private static final String SERVICE_NAME = "在线用户";

    private static String VIEW_PATH = "/admin/monitor/online/";

    @Inject
    private UserOnlineService userOnlineService;

    @Inject
    private LogoutService logoutService;

    /**
     * 跳转到记录展示页面
     */
    @Mapping(path = "detail", method = MethodType.GET)
    @SaCheckPermission(value = {"monitor:online:query"})
    public ModelAndView detail(Context ctx, @NotNull String id) {
        this.log.info("跳转到[{}]展示页面：id={}", SERVICE_NAME, id);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "detail.html");

        mav.put("bean", this.userOnlineService.get(id));

        return mav;
    }

    @Mapping(path = "forceLogout", method = MethodType.DELETE)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.FORCE)
    @SaCheckPermission(value = {"monitor:online:forceLogout"})
    public Result<?> forceLogout(Context ctx, @NotBlank String tokens) {
        this.log.info("[{}]强退：tokens={}", SERVICE_NAME, tokens);

        return this.logoutService.kickout(
                StrValidator.isBlank(tokens) ? ListUtil.empty() : SplitUtil.split(tokens, ",")
        ) ? Result.succeed() : Result.failure();
    }

    @Mapping(path = "", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView index(Context ctx) {
        ModelAndView mav = new ModelAndView(VIEW_PATH + "index.html");

        return mav;
    }

    /**
     * 分页查询功能
     */
    @Mapping(path = "query", method = MethodType.POST)
    @SaCheckPermission(value = {"monitor:online:query"})
    public LayuiPage<? extends UserOnline> query(Context ctx, @NotNull UserOnlineQo query, PaginationQueryObject page) {
        this.log.info("执行[{}]分页查询：query={}", SERVICE_NAME, query);

        /*
         * 服务调用
         */
        PageResult<? extends UserOnline> result = this.userOnlineService.pageBy(page, BeanUtil.beanToMap(query));

        /*
         * 返回值处理
         */
        return new LayuiPage<>(result, result.getTotal());
    }

}
