package cn.hg.solon.youcan.api.demo;

import cn.hg.solon.youcan.common.controller.BaseController;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;

@Controller
@Mapping("/api/demo")
public class ApiDemoController extends BaseController {

    @Mapping(path = "")
    public String index(Context ctx) {
        return "ok";
    }
}
