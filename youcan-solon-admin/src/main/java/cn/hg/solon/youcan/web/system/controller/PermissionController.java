package cn.hg.solon.youcan.web.system.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hg.solon.youcan.common.constant.CacheConstants;
import cn.hg.solon.youcan.common.enums.BusinessType;
import cn.hg.solon.youcan.common.validate.UpdateLabel;
import cn.hg.solon.youcan.framework.web.admin.annotation.Logging;
import cn.hg.solon.youcan.framework.web.pear.LayuiTreeTable;
import cn.hg.solon.youcan.system.entity.Permission;
import cn.hg.solon.youcan.system.service.PermissionService;
import cn.hg.solon.youcan.system.service.RoleService;
import cn.hg.solon.youcan.web.BaseAdminController;
import cn.hg.solon.youcan.web.system.qo.PermissionQo;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.collection.CollUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.map.MapUtil;
import org.dromara.hutool.core.tree.MapTree;
import org.dromara.hutool.core.util.ObjUtil;
import org.noear.solon.annotation.Body;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;
import org.noear.solon.core.handle.ModelAndView;
import org.noear.solon.core.handle.Result;
import org.noear.solon.data.annotation.Cache;
import org.noear.solon.data.annotation.CacheRemove;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.NotNull;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
 * @author 胡高
 */
@Controller
@Mapping("/admin/system/permission")
public class PermissionController extends BaseAdminController {

    private static final String SERVICE_NAME = "系统权限";

    private static final String VIEW_PATH = "/admin/system/permission/";

    @Inject
    private PermissionService permissionService;

    @Inject
    private RoleService roleService;

    /**
     * 跳转到新增页面
     */
    @Mapping(path = "add", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView add(Context ctx, Integer parentId) {
        this.log.info("跳转到新增[{}]页面", SERVICE_NAME);

        Permission bean = new Permission();
        bean.setParentId(ObjUtil.isNull(parentId) ? 0 : parentId);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "add.html");
        mav.put("bean", bean);

        return mav;
    }

    @Mapping(path = "add", method = MethodType.POST)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.CREATE)
    @SaCheckPermission(value = {"system:permission:add"})
    @CacheRemove(tags = CacheConstants.SYS_PERMISSION_TAG)
    public Result<?> add(Context ctx, @NotNull @Validated Permission bean) {
        this.log.info("新增[{}]记录：bean={}", SERVICE_NAME, bean);

        // 新建记录到数据库
        return this.permissionService.insert(bean) ? Result.succeed() : Result.failure();
    }

    @Mapping(path = "delete", method = MethodType.DELETE)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.DELETE)
    @SaCheckPermission(value = {"system:permission:del"})
    @CacheRemove(tags = CacheConstants.SYS_PERMISSION_TAG + "," + CacheConstants.SYS_USER_TAG)
    public Result<?> delete(Context ctx, @NotEmpty String ids) {
        this.log.info("删除[{}]记录：ids={}", SERVICE_NAME, ids);

        return this.permissionService.delete(ConvertUtil.toInt(ids)) ? Result.succeed() : Result.failure();
    }

    /**
     * 跳转到记录展示页面
     */
    @Mapping(path = "detail", method = MethodType.GET)
    @SaCheckPermission(value = {"system:permission:query"})
    public ModelAndView detail(Context ctx, @NotNull Integer id) {
        this.log.info("跳转到[{}]展示页面：id={}", SERVICE_NAME, id);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "detail.html");

        mav.put("bean", this.permissionService.get(id));

        return mav;
    }

    /**
     * 跳转到编辑页面
     */
    @Mapping(path = "edit", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView edit(Context ctx, @NotNull Integer id) {
        this.log.info("跳转到编辑[{}]页面", SERVICE_NAME);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "edit.html");
        mav.put("bean", this.permissionService.get(id));

        return mav;
    }

    @Mapping(path = "edit", method = MethodType.PUT)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.UPDATE)
    @SaCheckPermission(value = {"system:permission:edit"})
    @CacheRemove(tags = CacheConstants.SYS_PERMISSION_TAG + "," + CacheConstants.SYS_USER_TAG)
    public Result<?> edit(Context ctx, @NotNull @Validated(UpdateLabel.class) Permission bean) {
        this.log.info("更新[{}]记录：bean={}", SERVICE_NAME, bean);

        // 更新到数据库
        return this.permissionService.update(bean) ? Result.succeed() : Result.failure();
    }

    @Mapping(path = "", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView index(Context ctx) {
        this.log.info("跳转到[{}]列表页面", SERVICE_NAME);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "index.html");

        mav.putAll(ctx.paramMap().toValueMap());

        return mav;
    }

    /**
     * 分页查询功能
     */
    @Mapping(path = "query", method = MethodType.POST)
    @SaCheckPermission(value = {"system:permission:query"})
    @Cache(key = "query:${query}", tags = CacheConstants.SYS_PERMISSION_TAG, seconds = CacheConstants.CACHE_SECONDS_ONE_HOUR)
    public LayuiTreeTable<? extends Permission> query(Context ctx, @Body PermissionQo query) {
        this.log.info("执行[{}]查询：query={}", SERVICE_NAME, query);

        /*
         * 服务调用
         */
        List<? extends Permission> list = this.permissionService.listBy(BeanUtil.beanToMap(query));

        /*
         * 返回值处理
         */
        return new LayuiTreeTable<>(list, CollUtil.size(list),
                "id", "parentId", "name", Integer.valueOf(0));
    }

    @Mapping(path = "queryByRole", method = MethodType.GET)
    @SaCheckLogin
    @Cache(key = "queryByRole:${roleId}", tags = CacheConstants.SYS_ROLE_TAG + "," + CacheConstants.SYS_PERMISSION_TAG, seconds = CacheConstants.CACHE_SECONDS_ONE_HOUR)
    public List<? extends Permission> queryByRole(Context ctx, @NotNull Integer roleId) {
        this.log.info("执行[{}]查询：roleId={}", SERVICE_NAME, roleId);

        return this.permissionService.listByRole(this.roleService.get(roleId));
    }

    @Mapping(path = "tree", method = MethodType.GET)
    @SaCheckLogin
    @Cache(key = "tree", tags = CacheConstants.SYS_PERMISSION_TAG, seconds = CacheConstants.CACHE_SECONDS_ONE_HOUR)
    public List<MapTree<Integer>> tree(Context ctx) {
        return this.permissionService.treeBy(MapUtil.empty());
    }
}
