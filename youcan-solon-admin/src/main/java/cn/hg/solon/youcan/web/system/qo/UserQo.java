package cn.hg.solon.youcan.web.system.qo;

import cn.hg.solon.youcan.framework.web.qo.QueryObject;

/**
 * @author 胡高
 */
public class UserQo extends QueryObject {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8109862388717917773L;

    private String status;

    private Integer deptId;

    public Integer getDeptId() {
        return this.deptId;
    }

    public String getStatus() {
        return this.status;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
