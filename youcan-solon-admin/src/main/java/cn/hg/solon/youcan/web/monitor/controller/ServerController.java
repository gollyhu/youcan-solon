package cn.hg.solon.youcan.web.monitor.controller;

import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;
import org.noear.solon.core.handle.ModelAndView;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.hg.solon.youcan.framework.web.Server;
import cn.hg.solon.youcan.web.BaseAdminController;

/**
 * @author 胡高
 */
@Controller
@Mapping("/admin/monitor/server")
public class ServerController extends BaseAdminController {

    private static String VIEW_PATH = "/admin/monitor/server/";

    @Mapping(path = "", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView index(Context ctx) {

        ModelAndView mav = new ModelAndView(VIEW_PATH + "index.html");

        Server server = new Server();
        try {
            server.copyTo();
            mav.put("server", server);
        } catch (Exception e) {
        }

        return mav;
    }
}
