package cn.hg.solon.youcan.web;


import org.noear.solon.validation.annotation.Valid;

import cn.hg.solon.youcan.common.controller.BaseController;

/**
 * @author 胡高
 */
@Valid
public abstract class BaseAdminController extends BaseController {
}
