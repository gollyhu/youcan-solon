package cn.hg.solon.youcan.web.system.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hg.solon.youcan.common.constant.CacheConstants;
import cn.hg.solon.youcan.common.enums.BusinessType;
import cn.hg.solon.youcan.common.validate.UpdateLabel;
import cn.hg.solon.youcan.framework.web.admin.annotation.Logging;
import cn.hg.solon.youcan.framework.web.pear.LayuiTreeTable;
import cn.hg.solon.youcan.system.entity.Dept;
import cn.hg.solon.youcan.system.service.DeptService;
import cn.hg.solon.youcan.system.service.RoleService;
import cn.hg.solon.youcan.web.BaseAdminController;
import cn.hg.solon.youcan.web.system.qo.DeptQo;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.collection.CollUtil;
import org.dromara.hutool.core.convert.ConvertUtil;
import org.dromara.hutool.core.map.MapUtil;
import org.dromara.hutool.core.tree.MapTree;
import org.dromara.hutool.core.util.ObjUtil;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;
import org.noear.solon.core.handle.ModelAndView;
import org.noear.solon.core.handle.Result;
import org.noear.solon.data.annotation.Cache;
import org.noear.solon.data.annotation.CacheRemove;
import org.noear.solon.validation.annotation.NotNull;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
 * @author 胡高
 */
@Controller
@Mapping("/admin/system/dept")
public class DeptController extends BaseAdminController {

    private static final String SERVICE_NAME = "公司部门";

    private static final String VIEW_PATH = "/admin/system/dept/";

    @Inject
    private DeptService deptService;

    @Inject
    private RoleService roleService;

    @Mapping(path = "add", method = MethodType.POST)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.CREATE)
    @SaCheckPermission(value = {"system:department:add"})
    @CacheRemove(tags = CacheConstants.SYS_DEPT_TAG)
    public Result<?> add(Context ctx, @NotNull @Validated Dept bean) {
        this.log.info("新增[{}]记录：bean={}", SERVICE_NAME, bean);

        // 新建记录到数据库
        return this.deptService.insert(bean) ? Result.succeed() : Result.failure();
    }

    /**
     * 跳转到新增页面
     */
    @Mapping(path = "add", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView add(Context ctx, Integer parentId) {
        this.log.info("跳转到新增[{}]页面", SERVICE_NAME);

        Dept bean = new Dept();
        bean.setParentId(ObjUtil.isNull(parentId) ? 0 : parentId);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "add.html");

        mav.put("bean", bean);

        return mav;
    }

    @Mapping(path = "delete", method = MethodType.DELETE)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.DELETE)
    @SaCheckPermission(value = {"system:department:del"})
    @CacheRemove(tags = CacheConstants.SYS_DEPT_TAG + "," + CacheConstants.SYS_USER_TAG)
    public Result<?> delete(Context ctx, @NotNull String ids) {
        this.log.info("删除[{}]记录：ids={}", SERVICE_NAME, ids);

        return this.deptService.delete(ConvertUtil.toInt(ids)) ? Result.succeed() : Result.failure();
    }

    /**
     * 跳转到记录展示页面
     */
    @Mapping(path = "detail", method = MethodType.GET)
    @SaCheckPermission(value = {"system:department:query"})
    public ModelAndView detail(Context ctx, int id) {
        this.log.info("跳转到[{}]展示页面：id={}", SERVICE_NAME, id);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "detail.html");
        mav.put("bean", this.deptService.get(id));

        return mav;
    }

    @Mapping(path = "edit", method = MethodType.PUT)
    @Logging(title = SERVICE_NAME, businessType = BusinessType.UPDATE)
    @SaCheckPermission(value = {"system:department:edit"})
    @CacheRemove(tags = CacheConstants.SYS_DEPT_TAG + "," + CacheConstants.SYS_USER_TAG)
    public Result<?> edit(Context ctx, @NotNull @Validated(UpdateLabel.class) Dept bean) {
        this.log.info("更新[{}]记录：bean={}", SERVICE_NAME, bean);

        if (ObjUtil.isNull(bean.getParentId())) {
            bean.setParentId(0);
        }

        // 更新到数据库
        return this.deptService.update(bean) ? Result.succeed() : Result.failure();
    }

    /**
     * 跳转到编辑页面
     */
    @Mapping(path = "edit", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView edit(Context ctx, int id) {
        this.log.info("跳转到编辑[{}]页面", SERVICE_NAME);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "edit.html");

        mav.put("bean", this.deptService.get(id));

        return mav;
    }

    @Mapping(path = "", method = MethodType.GET)
    @SaCheckLogin
    public ModelAndView index(Context ctx) {
        this.log.info("跳转到[{}]列表页面", SERVICE_NAME);

        ModelAndView mav = new ModelAndView(VIEW_PATH + "index.html");

        mav.putAll(ctx.paramMap().toValueMap());

        return mav;
    }

    /**
     * 分页查询功能
     */
    @Mapping(path = "query", method = MethodType.POST)
    @SaCheckPermission(value = {"system:department:query"})
    @Cache(key = "query:${query}", tags = CacheConstants.SYS_DEPT_TAG, seconds = CacheConstants.CACHE_SECONDS_ONE_HOUR)
    public LayuiTreeTable<? extends Dept> query(Context ctx, DeptQo query) {
        this.log.info("执行[{}]查询：query={}", SERVICE_NAME, query);

        /*
         * 服务调用
         */
        List<? extends Dept> list = this.deptService.listBy(BeanUtil.beanToMap(query));

        /*
         * 返回值处理
         */
        return new LayuiTreeTable<>(list, CollUtil.size(list),
                "id", "parentId", "name", Integer.valueOf(0));
    }

    @Mapping(path = "queryByRole", method = MethodType.GET)
    @SaCheckLogin
    @Cache(key = "queryByRole:${roleId}", tags = CacheConstants.SYS_DEPT_TAG + "," + CacheConstants.SYS_ROLE_TAG, seconds = CacheConstants.CACHE_SECONDS_ONE_HOUR)
    public List<? extends Dept> queryByRole(Context ctx, @NotNull Integer roleId) {
        this.log.info("执行[{}]查询：roleId={}", SERVICE_NAME, roleId);

        return this.deptService.listByRole(this.roleService.get(roleId));
    }

    @Mapping(path = "tree", method = MethodType.GET)
    @SaCheckLogin
    @Cache(key = "tree", tags = CacheConstants.SYS_DEPT_TAG, seconds = CacheConstants.CACHE_SECONDS_ONE_HOUR)
    public List<MapTree<Integer>> tree(Context ctx) {
        return this.deptService.treeBy(MapUtil.empty());
    }
}
