layui.use(['jquery', 'watermark'], function() {
    var $ = layui.jquery, watermark = layui.watermark;
    
    var markNode;
    
    window.watermark = {
        show: (config = {content: 'Youcan-Solon'}) => {
            window.watermark.hide();
            markNode = new watermark(config);
        },
        hide: () => {
            if (markNode) {
                markNode.destroy();
            }
        }
    };

});