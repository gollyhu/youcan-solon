layui.use(['jquery', 'form', 'element'], function() {
    var $ = layui.jquery, form = layui.form;
    $("input").attr("disabled", true);
    $("select").attr("disabled", true);
    $("textarea").attr("disabled", true);

    $("input").attr("placeholder", "");
    $("select").attr("placeholder", "");
    $("textarea").attr("placeholder", "");

    $(".layui-btn").addClass("layui-hide");
    $(".layui-form-label").removeClass("layui-bg-blue");
    
    form.render();
});
